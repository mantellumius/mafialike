import react from "@vitejs/plugin-react-swc";
import { defineConfig } from "vite";
import path from "node:path";
export default defineConfig({
    plugins: [react()],
    resolve: {
        alias: {
            "@stores": path.resolve(__dirname, "./src/stores"),
            "@assets": path.resolve(__dirname, "./src/assets"),
            "@hooks": path.resolve(__dirname, "./src/hooks"),
            "@pages": path.resolve(__dirname, "./src/pages"),
            "@UI": path.resolve(__dirname, "./src/UI"),
            "@components": path.resolve(__dirname, "./src/components"),
            "@lib": path.resolve(__dirname, "./src/lib"),
        }
    },
    server: {
        host: "0.0.0.0",
        strictPort: false,
        port: 4173,
        watch: {
            usePolling: true
        }
    },
});
