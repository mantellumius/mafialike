import axios, { AxiosResponse } from "axios";
import { makeAutoObservable } from "mobx";
import { $auth } from "../api";
import RouteStore from "./RouteStore";

class AuthStore {
	private readonly route: RouteStore;
	authenticated: boolean;
	fetching: boolean;
	user: User | undefined;
	onLogin: (() => void)[];
	onLogout: (() => void)[];

	constructor(routeStore: RouteStore) {
		this.route = routeStore;
		this.onLogin = [];
		this.onLogout = [];
		this.fetching = true;
		this.authenticated = localStorage.getItem("authenticated") === "true";
		void this.isAuthenticated();
		makeAutoObservable(this);
	}

	async login(login: string, password: string): Promise<AxiosResponse | undefined> {
		try {
			const response = await $auth.post("/auth/login", {
				login,
				password
			});
			if (response.status === 200) {
				await this.isAuthenticated();
				this.route.navigate("/");
				this.onLogin.forEach((callback: () => void) => callback());
				return response;
			}
		} catch (e: Error | unknown) {
			if (axios.isAxiosError(e)) {
				return e.response;
			}
		}
	}

	async register(login: string, email: string, password: string): Promise<AxiosResponse | undefined> {
		try {
			return await $auth.post("/auth/register", {
				login,
				email,
				password
			});
		} catch (e: Error | unknown) {
			if (axios.isAxiosError(e))
				return e.response;
		}
	}

	async logout() {
		const response = await $auth.get("/auth/logout");
		if (response.status === 200) {
			this.authenticated = false;
			this.route.navigate("/");
			this.onLogout.forEach((callback: (...args: unknown[]) => void) => callback());
			localStorage.removeItem("authenticated");
			return true;
		}
		return false;
	}

	async isAuthenticated(): Promise<void> {
		const response = await $auth.get("/auth/is-authenticated");
		if (response.status === 200) {
			const { authenticated, user } = response.data as { authenticated: boolean, user: User };
			this.authenticated = authenticated;
			this.user = user;
		} else {
			this.authenticated = false;
			this.user = undefined;
			this.route.navigate("/login");
		}
		if (!this.authenticated)
			this.onLogout.forEach((callback: () => void) => callback());
		localStorage.setItem("authenticated", JSON.stringify(this.authenticated));
		this.fetching = false;
	}
}

export default AuthStore;
