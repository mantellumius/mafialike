import { action, autorun, makeObservable, observable } from "mobx";

class SnackbarStore {
	messages: SnackbarMessage[] = [];
	currentId = 0;

	constructor() {
		makeObservable(this, {
			messages: observable,
			addMessage: action,
			removeMessage: action
		});
		autorun(() => {
			if (this.messages.length > 0 && this.messages.every(m => m.isRemoved))
				this.clear();
		});
	}

	addMessage(message: SnackbarMessage) {
		message.duration ??= 5000;
		message.id = ++this.currentId;
		message.isRemoved = false;
		message.remove = () => this.removeMessage.call(this, this.currentId);
		this.messages.push(message);
	}

	removeMessage(messageId: number) {
		this.messages = this.messages.map(m => (m.id === messageId ? { ...m, isRemoved: true } : m));
	}

	clear() {
		this.messages = [];
	}
}

export default SnackbarStore;
