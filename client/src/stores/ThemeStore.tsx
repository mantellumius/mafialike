import { makeAutoObservable } from "mobx";

class ThemeStore {
	backgroundImage: string | undefined;

	constructor() {
		makeAutoObservable(this);
	}

	setBackgroundImage(image: string | undefined) {
		this.backgroundImage = image ?? undefined;
	}

	resetBackgroundImage() {
		this.backgroundImage = undefined;
	}
}

export default ThemeStore;