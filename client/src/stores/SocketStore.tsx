import { io, Socket } from "socket.io-client";
import { ClientToServerEvents, ServerToClientEvents } from "../../../shared";
import AuthStore from "./AuthStore";

class SocketStore {
	private socket: Socket<ServerToClientEvents, ClientToServerEvents>;
	eventsToCallback: Record<keyof ServerToClientEvents, ServerToClientEvents[keyof ServerToClientEvents]> = {} as never;

	constructor(authStore: AuthStore) {
		authStore.onLogin.push(() => this.connect());
		authStore.onLogout.push(() => this.disconnect());
		this.connect();
	}
	
	// eslint-disable-next-line @typescript-eslint/no-explicit-any
	public emit<T extends keyof ClientToServerEvents, U extends ClientToServerEvents[T]>(event: T, ...args: U extends (...args: infer V) => any ? V : never) {
		this.socket.emit(event, ...args);
	}

	public connect() {
		this.socket = io(import.meta.env.VITE_API_URL as string, { withCredentials: true });
		this.socket.on("connect", () => {
			console.log("Connected to socket");
			this.registerEvents();
		});
	}

	disconnect() {
		this.socket.disconnect();
	}

	registerEvents() {
		this.socket.on("disconnect", (reason) => console.log(`disconnected reason: ${reason}`));
		for (const event in this.eventsToCallback)
			this.socket.on(event as keyof ServerToClientEvents, this.eventsToCallback[event as keyof ServerToClientEvents]);
		console.log("Event listeners registered");
	}

	addEvent<T extends keyof ServerToClientEvents, U extends ServerToClientEvents[T]>(event: T, callback: U) {
		this.eventsToCallback[event] = callback;
	}
}

export default SocketStore;