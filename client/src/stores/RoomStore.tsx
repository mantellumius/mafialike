import { makeAutoObservable } from "mobx";
import { $api } from "../api";
import AuthStore from "./AuthStore";
import SocketStore from "./SocketStore";
import RouteStore from "./RouteStore";

class RoomStore {
	private readonly route: RouteStore;
	private readonly socket: SocketStore;
	private readonly auth: AuthStore;
	id: string | null = null;
	createdRoomId = "";
	owner: UserDto;

	constructor(socketStore: SocketStore, authStore: AuthStore, routeStore: RouteStore) {
		this.route = routeStore;
		this.socket = socketStore;
		this.auth = authStore;
		this.registerEvents();
		makeAutoObservable(this);
	}

	private registerEvents() {
		this.socket.addEvent("JoinFailed", this.onJoinFailed);
	}

	private onJoinFailed = () => {
		this.route.navigate("/create-room");
	};

	get isOwner(): boolean {
		return this.owner?.id === this.auth.user?.id;
	}

	join(id: string) {
		if (!id) return;
		this.socket.emit("Join", id, (owner) => this.owner = owner);
		this.id = id;
	}

	async create() {
		const response = await $api.post("/rooms/");
		if (response.status === 200)
			this.createdRoomId = response.data;
	}
}

export default RoomStore;
