import { makeAutoObservable } from "mobx";
import AuthStore from "./AuthStore";
import SnackbarStore from "./SnackbarStore";
import SocketStore from "./SocketStore";
import ThemeStore from "./ThemeStore";

class GameStore {
	private readonly socket: SocketStore;
	private readonly auth: AuthStore;
	private readonly theme: ThemeStore;
	private readonly snackbar: SnackbarStore;
	players: PlayerDto[];
	missions: MissionDto[] = [];
	currentMissionIndex = -1;
	crownedUserId: number | undefined;
	revealed = true;
	locked = false;
	currentGroupSuggestion: UserDto[];
	stage: PlayerStage;
	previousStage: PlayerStage | undefined;
	groups: GroupDto[];
	selectedGroupId: string | null;
	groupsWindowShown = false;
	votesHistoryWindowShown = false;
	setGameSetWindowShown = false;
	gameSet: WithIdAndAuthorId<GameSet> | null;
	abilityTargetId: number | null = null;
	votesHistories: VotesHistory[];
	votesHistoryIndex = 0;

	constructor(socketStore: SocketStore, authStore: AuthStore, themeStore: ThemeStore, snackbarStore: SnackbarStore) {
		this.socket = socketStore;
		this.auth = authStore;
		this.theme = themeStore;
		this.snackbar = snackbarStore;
		this.players = [];
		this.stage = "Wait";
		this.votesHistories = [];
		this.registerEvents();
		makeAutoObservable(this);
	}

	get me(): PlayerDto | undefined {
		return this.players.find(player => player.user.id === this.auth.user?.id);
	}

	get currentMission(): MissionDto {
		return this.missions[Math.max(0, this.currentMissionIndex)];
	}

	get currentVotesHistory(): VotesHistory | undefined {
		return this.votesHistories[this.votesHistoryIndex];
	}

	get canUseAbility() {
		return !this.me?.ability?.isDisabled && (this.me?.ability?.amount ?? 0) > 0 &&
			((this.me?.ability?.allowedMissions.length ?? 0) === 0 || this.me?.ability?.allowedMissions.includes(this.currentMissionIndex + 1)) &&
			(this.stage === "Ability" || this.me?.ability?.allowedGameStages.includes(this.stage as GameStage));
	}

	get isStarted() {
		return this.stage !== "PreGame";
	}

	set setRevealed(value: boolean) {
		this.revealed = value;
	}

	set setGroupsWindowShown(value: boolean) {
		this.groupsWindowShown = value;
	}

	set setLogsWindowShown(value: boolean) {
		this.votesHistoryWindowShown = value;
	}

	shufflePlayers() {
		this.socket.emit("ShufflePlayers");
	}

	setLocked(value: boolean) {
		this.socket.emit("ChangeLockState", value);
	}

	setAbilityStage(state: boolean) {
		if (state) {
			this.previousStage = this.stage;
			this.stage = "Ability";
		} else {
			this.rollbackToPreviousStage();
		}
	}

	public get clickAction(): ((id: number) => void) | null {
		switch (this.stage) {
			case "Suggestions":
				return (id: number) => {
					if (!this.players.find(p => p.user.id === id)?.settings?.canDoMissions) return;
					if (this.currentGroupSuggestion.find(user => user.id === id))
						this.currentGroupSuggestion = this.currentGroupSuggestion.filter(user => user.id !== id);
					else {
						const user = this.players.find(player => player.user.id === id)?.user;
						if (!user) return;
						this.currentGroupSuggestion.push(user);
					}
				};
			case "Ability":
				return (id: number) => {
					if (this.checkCanBeChosen(this.players.find(p => p.user.id === id)))
						this.abilityTargetId = id;
				};
			default:
				return null;
		}
	}

	public onPlayerClick = (id: number) => {
		if (typeof this.clickAction === "function" && this.isStarted)
			this.clickAction(id);
	};

	private registerEvents() {
		this.socket.addEvent("LockStateChanged", this.onLockStateChanged);
		this.socket.addEvent("GameSetChanged", this.onGameSetChanged);
		this.socket.addEvent("RoomStateSent", this.onRoomStateSent);
		this.socket.addEvent("GameStateSent", this.onGameStateSent);
		this.socket.addEvent("GameStarted", this.onGameStarted);
		this.socket.addEvent("PlayersUpdated", this.onPlayersUpdated);
		this.socket.addEvent("StartFailed", this.onStartFailed);
		this.socket.addEvent("SuggestionsStarted", this.onSuggestionsStarted);
		this.socket.addEvent("VotingStarted", this.onVotingStarted);
		this.socket.addEvent("MissionStarted", this.onMissionStarted);
		this.socket.addEvent("MissionFinished", this.onMissionFinished);
		this.socket.addEvent("NextMission", this.onNextMission);
		this.socket.addEvent("GameOver", this.onGameOver);
		this.socket.addEvent("StageUpdated", this.onStageUpdated);
		this.socket.addEvent("AbilityResultReceived", this.onAbilityResultReceived);
		this.socket.addEvent("UpdateAbilityInfo", this.onUpdateAbilityInfo);
		this.socket.addEvent("VotesHistoryUpdated", this.onVotesHistoryUpdated);
	}


	private onStageUpdated = (stage: PlayerStage) => {
		this.stage = stage;
		this.previousStage = undefined;
	};

	private onGameOver = (players: PlayerDto[]) => {
		this.players = players;
		this.revealed = true;
		console.log("GameOver");
	};

	private onNextMission = (missionIndex: number) => {
		this.currentMissionIndex = missionIndex;
	};

	private onLockStateChanged = (state: boolean) => {
		this.locked = state;
	};

	private onGameSetChanged = (gameSet: WithIdAndAuthorId<GameSet>) => {
		this.gameSet = gameSet;
		this.theme.setBackgroundImage(gameSet.backgroundImage);
	};

	private onMissionFinished = (missions: MissionDto[]) => {
		this.missions = missions;
	};

	private onMissionStarted = (missions: MissionDto[]) => {
		this.missions = missions;
	};

	private onVotingStarted = (groups: GroupDto[]) => {
		this.groups = groups;
		this.groupsWindowShown = true;
	};

	private onSuggestionsStarted = () => {
		this.currentGroupSuggestion = [];
	};

	private onStartFailed = (reason: string) => {
		this.snackbar.addMessage({
			message: {
				type: "Error",
				text: `Не удалось запустить игру\nПричина: ${reason}`
			}
		});
	};

	private onGameStateSent = (players: PlayerDto[], missions: MissionDto[], currentMissionIndex: number, crownedUserId: number) => {
		this.crownedUserId = crownedUserId;
		this.players = players;
		this.missions = missions;
		this.currentMissionIndex = currentMissionIndex;
		this.currentGroupSuggestion = [];
	};

	private onRoomStateSent = (users: UserDto[]) => {
		this.players = users.map(user => ({ user: user }));
	};

	private onPlayersUpdated = (players: PlayerDto[]) => {
		this.players = this.players.map(p => players.find(player => player.user.id === p.user.id) || p);
	};

	private onGameStarted = (players: PlayerDto[], missions: MissionDto[], crownedUserId: number) => {
		this.missions = missions;
		this.players = players;
		this.crownedUserId = crownedUserId;
		this.revealed = true;
		this.setGameSetWindowShown = false;
	};

	private onAbilityResultReceived = (message: Message) => {
		this.snackbar.addMessage({ message, duration: 10000 });
	};

	private onUpdateAbilityInfo = (abilityInfo: AbilityDto) => {
		if (this.me?.ability)
			this.me.ability = abilityInfo;
	};

	private onVotesHistoryUpdated = (missionIndex: number, votesHistory: VotesHistory) => {
		this.votesHistories[missionIndex] = votesHistory;
	};

	private rollbackToPreviousStage() {
		if (this.previousStage) {
			this.stage = this.previousStage;
			this.previousStage = undefined;
		}
	}

	public changeGameSet(id: number) {
		if (id === -1) return;
		localStorage.setItem("lastGameSetId", id.toString());
		this.socket.emit("ChangeGameSet", id);
	}

	public useAbility() {
		if (!this.abilityTargetId) return;
		this.socket.emit("UseSpecialAbility", this.abilityTargetId);
		this.abilityTargetId = null;
		this.rollbackToPreviousStage();
	}

	public cancelAbility() {
		this.socket.emit("CancelUseSpecialAbility");
		this.abilityTargetId = null;
		this.rollbackToPreviousStage();
	}

	public sendFail() {
		this.socket.emit("PerformMissionAction", "Fail");
	}

	public sendSuccess() {
		this.socket.emit("PerformMissionAction", "Success");
	}

	public vote() {
		if (!this.selectedGroupId) return;
		this.socket.emit("Vote", this.selectedGroupId);
		this.groups = [];
		this.selectedGroupId = null;
		this.groupsWindowShown = false;
	}

	public startSuggestions() {
		this.socket.emit("StartSuggestions");
	}

	public suggestGroup() {
		if (this.currentGroupSuggestion.length != this.currentMission.reqMembers) return;
		this.socket.emit("SuggestGroup", this.currentGroupSuggestion);
		this.currentGroupSuggestion = [];
	}

	public startGame() {
		this.socket.emit("StartGame");
	}

	public sitDown() {
		this.socket.emit("SitDown");
	}

	public standUp() {
		this.socket.emit("GetUp");
	}

	public kick(userId: number) {
		this.socket.emit("Kick", userId);
	}

	public setVotesHistoryIndex(missionIndex: number) {
		if (Number.isNaN(missionIndex)) return;
		this.votesHistoryIndex = missionIndex % this.missions.length;
	}

	public checkCanBeChosen(player: PlayerDto | undefined): boolean {
		if (!player || this.clickAction === null) return false;
		if (this.stage === "Suggestions") {
			return !!player.settings?.canDoMissions;
		} else if (this.stage === "Ability" && this.me?.ability) {
			const myAbility = this.me.ability;
			return !myAbility.targetOnlyMissionMembers ||
				myAbility.targetOnlyMissionMembers && this.currentMission.members.includes(player.user.id);
		}
		return false;
	}

	public checkIsInMission(id: number) {
		return (this.stage === "Mission" || this.stage === "MissionActive") && 
		this.currentMission.members.includes(id);
	}

	public checkIsChosen(id: number) {
		return (this.abilityTargetId === id && this.stage === "Ability") ||
			(this.currentGroupSuggestion && this.currentGroupSuggestion.find(user => user.id === id) !== undefined);
	}
}

export default GameStore;
