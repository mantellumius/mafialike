import { AxiosResponse } from "axios";
import { makeAutoObservable } from "mobx";
import * as React from "react";
import { $api } from "../api";
import DeleteGameSet from "@components/CreateSet/Tabs/DeleteGameSet/DeleteGameSet";
import EditSetInfo from "@components/CreateSet/Tabs/EditSetInfo/EditSetInfo";
import GameSettings from "@components/CreateSet/Tabs/GameSettings/GameSettings";
import RoleSettingsTab from "@components/CreateSet/Tabs/UpdateRoles/UpdateRolesTabs/RoleSettingsTab";
import RoleTab from "@components/CreateSet/Tabs/UpdateRoles/UpdateRolesTabs/RoleTab";
import UpdateRoles from "@components/CreateSet/Tabs/UpdateRoles/UpdateRoles";
import SnackbarStore from "./SnackbarStore";
import ThemeStore from "./ThemeStore";
import RouteStore from "./RouteStore";

class GameSetStore {
	private readonly route: RouteStore;
	private readonly theme: ThemeStore;
	private readonly snackbar: SnackbarStore;
	setId: number;
	gameSet: WithIdAndAuthorId<GameSet>;
	missionsSettings: MissionSettings[];
	roles: WithId<RoleDto>[];
	chosenRole: WithId<RoleDto> | undefined;
	chosenRoleSettings: RoleSettings;
	chosenRoleAbilitySettings: SpecialAbilitySettings;
	tabs: Tab[];
	currentTab: Tab;

	constructor(theme: ThemeStore, snackbar: SnackbarStore, routeStore: RouteStore) {
		this.route = routeStore;
		this.theme = theme;
		this.snackbar = snackbar;
		this.tabs = [
			{
				text: "",
				component: <RoleTab getInitialRole={() => ({ name: "", description: "", image: "" })}
				                    submitButtonText={"Создать"}
				                    onSubmit={async (role) => await this.createRoleAndAdd.call(this, role)} />,
				index: 2
			},
			{
				text: "",
				component: <RoleTab getInitialRole={(() => this.chosenRole ?? { name: "", description: "", image: "" })}
				                    submitButtonText={"Сохранить"}
				                    onSubmit={async (role) => await this.updateRole.call(this, role)} />,
				index: 2
			},
			{
				text: "",
				component: <RoleSettingsTab />,
				index: 2
			},
			{
				text: "Настройки набора",
				component: <EditSetInfo />,
				index: 0
			},
			{
				text: "Настройки игры",
				component: <GameSettings />,
				index: 1
			},
			{
				text: "Настройки ролей",
				component: <UpdateRoles />,
				index: 2
			},
			{
				text: "Удалить набор",
				component: <DeleteGameSet />,
				index: 3
			}

		];
		this.currentTab = this.tabs[this.tabs.length - 4];
		makeAutoObservable(this);
	}

	changeSetId(setId: number) {
		if (!setId) return;
		this.setId = setId;
		localStorage.setItem("gameSetId", setId.toString());
	}

	async create(gameSet: GameSet) {
		const response = await $api.post("game-sets", gameSet);
		if (response.status === 200) {
			// eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
			this.changeSetId(response.data.id as number);
			this.route.navigate("/game-sets/edit");
		}
	}

	async update(gameSet: GameSet) {
		const response = await $api.put(`/game-sets/${this.setId}`, gameSet);
		if (response.status === 200) {
			this.gameSet = response.data;
			this.theme.setBackgroundImage(this.gameSet.backgroundImage);
			this.successfullySaved();
		}
	}

	deleteMissionSettings(number: number) {
		this.missionsSettings = this.missionsSettings.filter(m => m.number !== number);
		this.missionsSettings.forEach((m, i) => m.number = i + 1);
		this.missionsSettings = this.missionsSettings.sort((a, b) => a.number - b.number);
	}

	addMissionSettings() {
		this.missionsSettings.push({ number: this.missionsSettings.length + 1, requiredFails: 1, requiredMembers: 1 });
	}

	async saveMissions() {
		const response = await $api.put(`/game-sets/${this.setId}/missions`, this.missionsSettings);
		if (response.status === 200)
			this.successfullySaved();
		else if (response.status === 400)
			this.snackbar.addMessage({
				message: {
					type: "Error",
					// eslint-disable-next-line @typescript-eslint/no-unsafe-member-access,@typescript-eslint/restrict-template-expressions
					text: `Не удалось сохранить.\nПричина: ${response.data.reason ?? ""}`
				}
			});
	}

	async createRoleAndAdd(role: RoleDto) {
		const roleId = await this.createRole(role);
		if (roleId !== -1) {
			const response = await this.addRole(roleId);
			if (response.status === 200) {
				await this.fetch();
				this.snackbar.addMessage({ message: { type: "Success", text: "Роль успешно создана!" } });
				this.chosenRole = this.roles.find(r => r.id === roleId);
				await this.setUpdateRoleSettingsTab();
				this.currentTab = this.tabs[2];
			} else if (response.status === 400) {
				this.snackbar.addMessage({
					message: {
						type: "Error",
						// eslint-disable-next-line @typescript-eslint/no-unsafe-member-access,@typescript-eslint/restrict-template-expressions
						text: `Не удалось добавить роль.\nПричина: ${response.data.reason ?? ""}`
					}
				});
			}
		}
	}

	async createRole(role: RoleDto): Promise<number> {
		const response = (await $api.post("/roles", role));
		// eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
		return response.status === 200 ? response.data.role.id as number : -1;
	}

	async updateRole(role: RoleDto): Promise<void> {
		if (!this.chosenRole) return;
		const response = (await $api.patch(`/roles/${this.chosenRole.id}`, role));
		await this.fetchRoles();
		if (response.status === 200) this.successfullySaved();
	}

	async addRole(roleId: number): Promise<AxiosResponse> {
		return (await $api.post("/game-sets/roles", { setId: this.setId, roleId }));
	}

	async saveRoleSettings() {
		if (!this.chosenRole) return;
		const response = (await $api.put(`/roles/${this.chosenRole.id}/settings`, this.chosenRoleSettings));
		return response.status === 200;
	}

	async saveSpecialAbilitySettings() {
		if (!this.chosenRole) return;
		const response = (await $api.put(`/roles/${this.chosenRole.id}/abilities`, this.chosenRoleAbilitySettings));
		return response.status === 200;
	}

	setCreateRoleTab() {
		this.chosenRole = undefined;
		this.currentTab = this.tabs[0];
	}

	setUpdateRoleTab() {
		this.currentTab = this.tabs[1];
	}

	chooseRole(roleIndex: number) {
		this.chosenRole = this.roles[roleIndex];
		this.currentTab = this.tabs[this.tabs.length - 2];
	}

	async setUpdateRoleSettingsTab() {
		if (!this.chosenRole) return;
		const roleSettings = await $api.get(`/roles/${this.chosenRole.id}/settings`);
		const roleAbilitySettings = await $api.get(`/roles/${this.chosenRole.id}/abilities`);
		if (roleSettings.status === 200)
			this.chosenRoleSettings = roleSettings.data;
		if (roleAbilitySettings.status === 200)
			this.chosenRoleAbilitySettings = roleAbilitySettings.data;
		this.currentTab = this.tabs[2];
	}

	async deleteRole() {
		if (!this.chosenRole) return;
		await $api.delete(`/roles/${this.chosenRole.id}`);
		this.chosenRole = undefined;
		await this.fetchRoles();
	}

	async deleteGameSet() {
		const response = await $api.delete(`/game-sets/${this.setId}`);
		if (response.status === 200)
			this.route.navigate("/game-sets");
	}

	async fetch() {
		const response = await $api.get(`/game-sets/${this.setId}`);
		if (response.status !== 200) return this.route.navigate("/");
		this.gameSet = response.data;
		this.theme.setBackgroundImage(this.gameSet.backgroundImage);
		await this.fetchRoles();
		await this.fetchMissions();
	}

	async fetchRoles() {
		const response = await $api.get(`/game-sets/${this.setId}/roles`);
		if (response.status === 200) this.roles = response.data;
	}

	async fetchMissions() {
		const response = await $api.get(`/game-sets/${this.setId}/missions`);
		if (response.status === 200) this.missionsSettings = response.data;
	}

	successfullySaved() {
		this.snackbar.addMessage({ message: { type: "Success", text: "Изменения успешно сохранены" } });
	}
}

export type Tab = {
	text: string,
	component: React.ReactElement,
	index: number,
}

export default GameSetStore;
