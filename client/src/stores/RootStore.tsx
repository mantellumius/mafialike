import { ReactNode, createContext, useContext } from "react";
import ThemeStore from "./ThemeStore";
import SocketStore from "./SocketStore";
import RoomStore from "./RoomStore";
import GameStore from "./GameStore";
import AuthStore from "./AuthStore";
import GameSetStore from "./GameSetStore";
import SnackbarStore from "./SnackbarStore";
import RouteStore from "./RouteStore";

class RootStore {
	themeStore: ThemeStore;
	socketStore: SocketStore;
	roomStore: RoomStore;
	gameStore: GameStore;
	authStore: AuthStore;
	gameSetStore: GameSetStore;
	snackbarStore: SnackbarStore;
	routeStore: RouteStore;

	constructor() {
		this.routeStore = new RouteStore();
		this.snackbarStore= new SnackbarStore(),
		this.themeStore= new ThemeStore(),
		this.authStore=new AuthStore(this.routeStore),
		this.socketStore= new SocketStore(this.authStore),
		this.gameSetStore= new GameSetStore(this.themeStore, this.snackbarStore, this.routeStore),
		this.roomStore= new RoomStore(this.socketStore, this.authStore, this.routeStore),
		this.gameStore= new GameStore(this.socketStore, this.authStore, this.themeStore, this.snackbarStore);
	}
}

export default RootStore;

const StoreContext = createContext(null as unknown as RootStore);
export const StoreProvider = ({ children, store }:
	{ children: ReactNode | ReactNode[] | undefined, store: RootStore; }) => {
	return (
		<StoreContext.Provider value={store}>
			{children}
		</StoreContext.Provider>
	);
};
export const useRootStore = () => useContext(StoreContext);