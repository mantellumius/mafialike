import { useEffect, useState } from "react";
import { $api } from "../api";
import { AxiosRequestConfig, AxiosResponse } from "axios";

const useApi = <TResponseData>({ url, method, data, headers, ...config }: AxiosRequestConfig & {url: string, method: "get" | "post" | "delete" | "patch" | "put"}) => {
	const [response, setResponse] = useState<TResponseData | null>(null);
	const [error, setError] = useState("");
	const [loading, setloading] = useState(true);
	useEffect(() => {
		$api[method](url, { method: method ?? "get", headers, data, ...config })
			.then((res: AxiosResponse<TResponseData>) => {
				setResponse(res.data);
			})
			.catch((err: string) => {
				setError(err);
			})
			.finally(() => {
				setloading(false);
			});
	// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [method, url, data, headers]);

	return { response, error, loading };
};

export default useApi;