import { useEffect } from "react";
import ThemeStore from "@stores/ThemeStore";

export default function useSetBackgroundImage(themeStore: ThemeStore, image?: string) {
	useEffect(() => {
		if (image)
			themeStore.setBackgroundImage(image);
		else
			themeStore.resetBackgroundImage();
	}, [image, themeStore]);
}