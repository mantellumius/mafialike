type SnackbarMessage = {
	message: Message;
	isRemoved?: boolean;
	duration?: number;
	id?: number;
	remove?: () => void;
}