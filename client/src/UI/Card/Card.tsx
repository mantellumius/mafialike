import * as React from "react";
import classes from "./Card.module.css";

const Card: React.FC<Props> = ({ title, description, image, author, children }) => {
	return (
	  <div className={classes.card}>
		  <div className={classes.image}
		       style={{ backgroundImage: `url(${image})` }}></div>
		  <div className={classes.content}>
			  <div className={classes.textContent}>
				  <div>
					  Автор: <span className={classes.author}>{author}</span>
				  </div>
				  <div className={classes.title}>
					  {title}
				  </div>
				  <div className={classes.description}>
					  {`${description.slice(0, 200)}${description.length > 100 ? "..." : ""}`}
				  </div>
			  </div>
			  <div className={classes.actions}>
				  {children}
			  </div>
		  </div>
	  </div>
	);
};
type Props = {
	title: string;
	description: string;
	image: string;
	author: string;
	children: React.ReactNode;
};
export default Card;