import { FC } from "react";
import classes from "./Loader.module.css";
import classNames from "@lib/classNames";

const Loader: FC = () => {
	return (
		<div className={classNames(classes.root)}>
			<div>
			</div>
			<div>
			</div>
			<div>
			</div>
			<div>
			</div>
		</div>
	);
};

export { Loader };