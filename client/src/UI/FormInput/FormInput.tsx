import * as React from "react";
import classes from "./FormInput.module.css";
import classNames from "@lib/classNames";

const FormInput: React.FC<Props> = ({ label, error, width,className, ...props }) => {
	return (
	  <div className={classes.container} style={{ width: width }}>
		  <fieldset>
			  <input className={classNames(classes.input,{[classes.errorInput]: !!error}, [className])} required autoComplete={label} {...props} />
			  <label className={classes.label}>{label}</label>
		  </fieldset>
		  {error && <div className={classes.error}>{error}</div>}
	  </div>
	);
};

interface Props extends React.InputHTMLAttributes<HTMLInputElement> {
	label?: string,
	className?: string,
	error?: string,
	width?: string,
}

export default FormInput;