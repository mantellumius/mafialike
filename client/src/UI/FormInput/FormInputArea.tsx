import * as React from "react";
import classes from "./FormInput.module.css";

const FormInputArea: React.FC<Props> = ({ label, error, width, ...props }) => {
	return (
	  <div className={classes.container} style={{ width: width }}>
		  <fieldset>
			  <textarea className={classes.input} required autoComplete={label} {...props} />
			  <label className={classes.label}>{label}</label>
		  </fieldset>
		  {error && <div className={classes.error}>{error}</div>}
	  </div>
	);
};

interface Props extends React.TextareaHTMLAttributes<HTMLTextAreaElement> {
	label?: string,
	error?: string,
	width?: string,
}

export default FormInputArea;