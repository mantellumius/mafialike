import { ChevronDownIcon } from "@heroicons/react/24/outline";
import * as React from "react";
import { PropsWithChildren } from "react";
import ToggleButton from "../ToggleButton/ToggleButton";
import Center from "./Center";
import Left from "./Left";
import Right from "./Right";
import classes from "./ToolBar.module.css";
import classNames from "@lib/classNames";

const ToolBarComponent: React.FC<PropsWithChildren> = ({ children }) => {
	const [isToggled, setIsToggled] = React.useState(true);
	return (
	  <div className={classNames(classes.toolBar, {[classes.toggled]: isToggled})}>
		  <ToggleButton className={classes.toggleButton} onClick={() => setIsToggled(!isToggled)}
		                trueIcon={<ChevronDownIcon className={classNames(classes.icon, {},[classes.iconToggled])} />}
		                falseIcon={<ChevronDownIcon className={classes.icon} />} />
		  <div className={classes.toolBarContent}>
			  {(children as React.ReactElement[])?.find(child => child?.type === Left)}
			  {(children as React.ReactElement[])?.find(child => child?.type === Center)}
			  {(children as React.ReactElement[])?.find(child => child?.type === Right)}
		  </div>
	  </div>
	);
};

ToolBarComponent.propTypes = {
	children: function(props: PropsWithChildren, propName: string, componentName: string) {
		const prop = props.children as React.ReactElement[];
		if (prop.length < 3) {
			return new Error(
			  `${propName} in ${componentName} must be at least 3`
			);
		}
		const left = prop.filter(child => child.type === Left);
		if (left.length !== 1)
			return new Error(`${componentName} must contain 1 ToolBar.Left component as children`);
		const center = prop.filter(child => child.type === Center);
		if (center.length !== 1)
			return new Error(`${componentName} must contain 1 ToolBar.Center component as children`);
		const right = prop.filter(child => child.type === Right);
		if (right.length !== 1)
			return new Error(`${componentName} must contain 1 ToolBar.Right component as children`);
		return null;
	}
};

export const ToolBar = Object.assign(ToolBarComponent, { Left, Right, Center });
