import * as React from "react";
import classes from "./ToolBar.module.css";

const Left: React.FC<Props> = ({ children }) => {
	return (
	  <div className={classes.left}>
		  {children}
	  </div>
	);
};
type Props = {
	children: React.ReactNode | React.ReactNode[];
}
export default Left;