import * as React from "react";
import classes from "./ToolBar.module.css";

const Right: React.FC<Props> = ({ children }) => {
	return (
	  <div className={classes.right}>
		  {children}
	  </div>
	);
};
type Props = {
	children: React.ReactNode | React.ReactNode[];
}
export default Right;