import * as React from "react";
import classes from "./ToolBar.module.css";

const Center: React.FC<Props> = ({ children }) => {
	return (
	  <div className={classes.center}>
		  {children}
	  </div>
	);
};
type Props = {
	children: React.ReactNode | React.ReactNode[];
}
export default Center;