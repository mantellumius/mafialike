import * as React from "react";
import classes from "./ButtonIcon.module.css";
import classNames from "@lib/classNames";


const ButtonIcon: React.FC<Props> = ({
	                                     className,
	                                     icon,
	                                     background,
	                                     hoverBackground,
	                                     color,
	                                     width,
	                                     height,
	                                     ...props
}) => {
	return (
	  <button
			style={{
				width: width,
				height: height,
				color: color,
				background: background,
				"--hover-background": hoverBackground ?? (background ? `${background.slice(0, -4)}800)` : undefined)
			} as React.CSSProperties}
			className={classNames(classes.root, {}, ["focus:ring-gray-200",className])}
			{...props}>
		  {<div className={classes.icon}>
			  {icon}
		  </div>}
	  </button>
	);
};

interface Props extends React.ButtonHTMLAttributes<HTMLButtonElement> {
	icon: React.ReactNode,
	className?: string,
	width?: string | number,
	height?: string | number,
	background?: string
	hoverBackground?: string;
	color?: string;
}

export default ButtonIcon;