import * as React from "react";
import classes from "./Header.module.css";

const Header:  React.FC<Props> = ({children}) => {
	return (
	  <div className={classes.header}>
			<h1>{children}</h1>
	  </div>
	);
};
type Props = {
	children: string;
};
export default Header;