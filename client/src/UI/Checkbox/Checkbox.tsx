import * as React from "react";
import classes from "./Checkbox.module.css";

const Checkbox: React.FC<Props> = ({checked, name, onChange,title}) => {
	return (
	  <div className={classes.checkbox}>
			<input type={"checkbox"} checked={checked ?? false} name={name} onChange={onChange} />
		  <label className={classes.label}>{title}</label>
	  </div>
	);
};
type Props = {
	checked?: boolean;
	name?: string;
	onChange: (e: React.ChangeEvent<HTMLInputElement>) => void;
	title: string;
}
export default Checkbox;