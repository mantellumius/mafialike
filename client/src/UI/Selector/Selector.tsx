import * as React from "react";
import classes from "./Selector.module.css";

const Selector: React.FC<Props> = ({ options, onChange, value, label, name }) => {
	return (
	  <div className={classes.container}>
		  <select className={classes.selector} onChange={onChange} value={value} placeholder={"dnfds"} name={name}>
			  {options.map((option, index) => (<option key={index} value={option.value}>{option.label}</option>))}
		  </select>
		  <label className={classes.label}>{label}</label>
	  </div>
	);
};
type Props = {
	options: {value: string, label: string}[];
	onChange: (e: React.ChangeEvent<HTMLSelectElement>) => void;
	value: string;
	label: string;
	name: string;
}
export default Selector;