import * as React from "react";
import { PropsWithChildren } from "react";
import classes from "./Paper.module.css";

const Paper:  React.FC<PropsWithChildren> = ({children}) => {
	return (
	  <div className={classes.paper}>
		  {children}
	  </div>
	);
};

export default Paper;