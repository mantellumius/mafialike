import * as React from "react";
import classes from "./Button.module.css";
import classNames from "@lib/classNames";

const Button: React.FC<Props> = React.memo(({
	text,
	onClick,
	className,
	children,
	background,
	hoverBackground,
	color,
	width,
	height,
	...props
}) => {
	return (
	  <button
			style={{
				width: width,
				height: height,
				color: color,
				background: background,
				"--hover-background": hoverBackground ?? (background ? `${background.slice(0, -4)}800)` : undefined)
			} as React.CSSProperties}
			onClick={onClick}
			className={classNames(classes.root, {}, ["focus:ring-gray-200", className])}
			{...props}>
			{text && <div>{text}</div>}
			{children}
	  </button>
	);
});

interface Props extends React.ButtonHTMLAttributes<HTMLButtonElement> {
	text?: string,
	className?: string,
	width?: string | number,
	height?: string | number,
	background?: string
	hoverBackground?: string;
	color?: string;
}

export default Button;