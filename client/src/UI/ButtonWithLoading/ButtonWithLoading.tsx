import { FC, useState } from "react";
import classes from "./ButtonWithLoading.module.css";
import classNames from "@lib/classNames";
import { Loader } from "@UI/Loader/Loader";

const ButtonWithLoading: FC<Props> = ({ 
	text,
	onClick,
	className,
	children,
	background,
	hoverBackground,
	color,
	width,
	height,
	...props 
}) => {
	const [isLoading, setIsLoading] = useState(false);
	function onClickWrapper(e: React.MouseEvent<HTMLButtonElement, MouseEvent>) {
		e.preventDefault();
		setIsLoading(true);
		onClick?.().finally(() => setIsLoading(false));
	}
	
	return (
		<button
			style={{
				width: width,
				height: height,
				color: color,
				background: background,
				"--hover-background": hoverBackground ?? (background ? `${background.slice(0, -4)}800)` : undefined)
			} as React.CSSProperties}
			disabled={isLoading}
			onClick={onClickWrapper}
			className={classNames(classes.root, {}, ["focus:ring-gray-200", className])}
			{...props}>
			{isLoading ? <Loader /> : <>
				{text && <div>{text}</div>}
				{children}
			</>}
	  </button>
	);
};

interface Props extends React.ButtonHTMLAttributes<HTMLButtonElement> {
	text?: string,
	className?: string,
	onClick?: () => Promise<void>,
	width?: string | number,
	height?: string | number,
	background?: string
	hoverBackground?: string;
	color?: string;
}

export { ButtonWithLoading };