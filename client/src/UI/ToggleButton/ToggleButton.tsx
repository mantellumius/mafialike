import * as React from "react";
import { ReactElement, useEffect, useState } from "react";
import classes from "./ToggleButton.module.css";
import classNames from "@lib/classNames";

const ToggleButton: React.FC<Props> = ({ onClick, value, tooltip, trueIcon, falseIcon, disabled, className }) => {
	const [isToggled, setIsToggled] = useState(false);
	useEffect(() => {
		setIsToggled(!!value);
	}, [value]);

	return (
	  <button onClick={() => {
		  setIsToggled(prev => !prev);
		  onClick(!isToggled);
	  }} className={classNames(classes.toggleButton, {}, [className])} disabled={disabled}>
		  {tooltip && <span className={classes.tooltip}>{tooltip}</span>}
		  {isToggled ?
				<>{trueIcon}</> :
				<>{falseIcon}</>
		  }
	  </button>
	);
};

type Props = {
	value?: boolean;
	tooltip?: string;
	disabled?: boolean;
	onClick: (state: boolean) => void;
	trueIcon: ReactElement;
	falseIcon: ReactElement;
	className?: string;
}
export default ToggleButton;