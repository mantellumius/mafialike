import axios, { AxiosError } from "axios";
import { rootStore } from "../main";

export const URL = import.meta.env.VITE_API_URL as string;
export const API_URL = `${URL}/api`;

export const $auth = axios.create({
	withCredentials: true,
	baseURL: URL
});
export const $api = axios.create({
	validateStatus: function () {
		return true;
	},
	withCredentials: true,
	baseURL: API_URL
});
const rateLimitMessage: (timeout: number) => Message = (timeout: number) => ({
	type: "Error",
	text: `Слишком много запросов.\n Попробуйте через ${timeout ?? 60} секунд(ы)`
});
$auth.interceptors.response.use(
	(response) => response,
	(error: AxiosError) => {
		if (error.response?.status === 429) {
			rootStore.snackbarStore.addMessage({
				message: rateLimitMessage((error.response?.data as { timeout: number; }).timeout)
			});
		}
		return Promise.reject(error);
	}
);
$api.interceptors.response.use(
	(response) => {
		if (response.status === 429) {
			rootStore.snackbarStore.addMessage({
				message: rateLimitMessage((response?.data as { timeout: number; }).timeout)
			});
		}
		return response;
	}
);
