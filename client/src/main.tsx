import * as React from "react";
import * as ReactDOM from "react-dom/client";
import { BrowserRouter } from "react-router-dom";
import App from "./App";
import "./index.css";
import RootStore from "./stores/RootStore";

export const rootStore = new RootStore();
void (function () {
	ReactDOM.createRoot(document.getElementById("root") as HTMLElement).render(
		<React.StrictMode>
			<BrowserRouter>
				<App rootStore={rootStore} />
			</BrowserRouter>
		</React.StrictMode>
	);
})();

