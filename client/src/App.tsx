import * as React from "react";
import { useNavigate } from "react-router-dom";
import Main from "./components/Main/Main";
import MainRouter from "./components/MainRouter";
import NavBar from "./components/NavBar/NavBar";
import Snackbar from "./components/Snackbar/Snackbar";
import RootStore, { StoreProvider } from "./stores/RootStore";

function App({ rootStore }: { rootStore: RootStore; }) {
	rootStore.routeStore.navigate = useNavigate();
	return (
		<StoreProvider store={rootStore}>
			<NavBar />
			<Main>
				<MainRouter />
				<Snackbar />
			</Main>
		</StoreProvider>
	);
}

export default App;
