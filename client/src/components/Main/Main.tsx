import { observer } from "mobx-react-lite";
import * as React from "react";
import { PropsWithChildren } from "react";
import classes from "./Main.module.css";
import { useRootStore } from "@stores/RootStore";

const Main: React.FC<PropsWithChildren> = observer(({ children }) => {
	const {themeStore} = useRootStore();

	return (
	  <main className={classes.main}
	        style={{ backgroundImage: themeStore.backgroundImage ? `url(${themeStore.backgroundImage})` : undefined }}>
		  {children}
	  </main>
	);
});
export default Main;