import {
	AcademicCapIcon,
	ArrowPathRoundedSquareIcon,
	Cog8ToothIcon,
	EyeIcon,
	EyeSlashIcon,
	LockClosedIcon,
	LockOpenIcon,
	TableCellsIcon,
	UserGroupIcon
} from "@heroicons/react/24/outline";
import {
	AcademicCapIcon as AcademicCapIconSolid,
	Cog8ToothIcon as Cog8ToothIconSolid,
	TableCellsIcon as TableCellsIconSolid,
	UserGroupIcon as UserGroupIconSolid
} from "@heroicons/react/24/solid";
import { observer } from "mobx-react-lite";
import * as React from "react";
import Button from "@UI/Button/Button";
import ButtonIcon from "@UI/ButtonIcon/ButtonIcon";
import ToggleButton from "@UI/ToggleButton/ToggleButton";
import { ToolBar } from "@UI/ToolBar/ToolBar";
import { useRootStore } from "@stores/RootStore";

const GameToolBar: React.FC<Props> = observer(() => {
	const {gameStore, roomStore: roomStore} = useRootStore();
	return (
	  <ToolBar>
		  <ToolBar.Left>
			  <ToggleButton onClick={(state) => gameStore.setGameSetWindowShown = state}
			                value={gameStore.setGameSetWindowShown}
			                tooltip={"Выбор набора"}
			                trueIcon={<Cog8ToothIconSolid />} falseIcon={<Cog8ToothIcon />}
			                disabled={!roomStore.isOwner || gameStore.isStarted} />
			  <ToggleButton onClick={(state) => gameStore.setLocked(state)} value={gameStore.locked}
			                tooltip={"Закрыть/Открыть комнату"}
			                trueIcon={<LockClosedIcon />} falseIcon={<LockOpenIcon />}
			                disabled={!roomStore.isOwner || gameStore.isStarted} />
			  <ButtonIcon onClick={() => gameStore.shufflePlayers()}
			              disabled={!roomStore.isOwner || gameStore.isStarted}
			              icon={<ArrowPathRoundedSquareIcon />} />
			  <ToggleButton onClick={(state) => gameStore.setLogsWindowShown = state}
			                value={gameStore.votesHistoryWindowShown}
			                tooltip={"Скрыть/Показать журнал"}
			                trueIcon={<TableCellsIconSolid />} falseIcon={<TableCellsIcon />} />
		  </ToolBar.Left>
		  <ToolBar.Center>
			  <Button text={"Начать игру"} onClick={() => gameStore.startGame()} background={"var(--success-500)"}
			          aria-label={"Collapse tool bar"} disabled={!roomStore.isOwner || gameStore.isStarted} />
		  </ToolBar.Center>
		  <ToolBar.Right>
			  <ToggleButton onClick={(state) => gameStore.setRevealed = state}
			                value={gameStore.revealed} tooltip={"Скрыть/Показать роли"}
			                trueIcon={<EyeIcon />} falseIcon={<EyeSlashIcon />} />
			  <ToggleButton onClick={(state) => gameStore.setGroupsWindowShown = state}
			                value={gameStore.groupsWindowShown}
			                tooltip={"Скрыть/Показать Окно групп"}
			                trueIcon={<UserGroupIconSolid />} falseIcon={<UserGroupIcon />} />
			  <ToggleButton onClick={(state) => gameStore.setAbilityStage(state)} value={gameStore.stage === "Ability"}
			                disabled={!gameStore.canUseAbility}
			                tooltip={"Скрыть/Показать Окно групп"}
			                trueIcon={<AcademicCapIconSolid />} falseIcon={<AcademicCapIcon />} />
		  </ToolBar.Right>
	  </ToolBar>
	);
});
type Props = {
	children?: React.ReactNode;
};
export default GameToolBar;