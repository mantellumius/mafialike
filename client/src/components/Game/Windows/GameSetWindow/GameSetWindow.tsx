import { observer } from "mobx-react-lite";
import * as React from "react";
import { useEffect, useState } from "react";
import Window from "../Window/Window";
import GameSet from "./GameSet";
import classes from "./GameSetWindow.module.css";
import useApi from "@hooks/useApi";
import { useRootStore } from "@stores/RootStore";

const GameSetWindow = observer(() => {
	const {gameStore} = useRootStore();
	const [gameSets, setGameSets] = useState<WithIdAndAuthorId<GameSet>[]>([]);
	const { response } = useApi({
		method: "get",
		url: "/game-sets",
	});
	useEffect(() => setGameSets(response as WithIdAndAuthorId<GameSet>[]), [response]);
	if (!gameStore.setGameSetWindowShown) return <></>;
	return (
		<Window position="top-left"
			header={"Выберите набор ролей"}
			content={
				<div className={classes.gameSetWindow}>
					<div>
						{gameSets && gameSets.map(gameSet => <GameSet gameSet={gameSet}
							isChosen={gameSet.id === gameStore.gameSet?.id}
							onClick={() => gameStore.changeGameSet(gameSet.id)}
							key={gameSet.id} />)}
					</div>
				</div>
			} />
	);
});

export default GameSetWindow;