import * as React from "react";
import classes from "./GameSetWindow.module.css";

const GameSet: React.FC<Props> = ({ gameSet, onClick, isChosen }) => {
	return (
	  <div onClick={onClick} className={classes.gameSetItem}>
		  <div className={classes.name}>{gameSet.name}</div>
		  <div className={classes.description}>{isChosen && `${gameSet.description.slice(0, 100)}${gameSet.description.length > 100 ? "..." : ""}`}</div>
	  </div>
	);
};
type Props = {
	gameSet: WithIdAndAuthorId<GameSet>;
	onClick: () => void;
	isChosen: boolean;
};
export default GameSet;