import * as React from "react";
import Header from "@UI/Header/Header";
import classes from "./Window.module.css";

const Window: React.FC<Props> = ({ position, header, content, onKeyDown}) => {
	return (
	  <div className={classes.windowWrap} data-position={position ?? "top-left"} onKeyDown={(e) => typeof onKeyDown === "function" && onKeyDown(e)} tabIndex={0}>
		  <div className={classes.window}>
			  <div className={classes.windowHeader}>
				  <Header>{header}</Header>
			  </div>
			  <div className={classes.windowContent}>
				  {content}
			  </div>
		  </div>
	  </div>
	);
};
type Props = {
	content: React.ReactNode | React.ReactNode[];
	header: string;
	position?: "top-right" | "top-left" | "bottom-right" | "bottom-left";
	onKeyDown?: (e: React.KeyboardEvent) => void;
}
export default Window;