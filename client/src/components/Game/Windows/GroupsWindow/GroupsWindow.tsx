import { observer } from "mobx-react-lite";
import * as React from "react";
import Window from "../Window/Window";
import Group from "./Group";
import classes from "./GroupsWindow.module.css";
import { useRootStore } from "@stores/RootStore";

const GroupsWindow = observer(() => {
	const {gameStore} = useRootStore();
	if (!gameStore.groupsWindowShown) return <></>;
	return (
	  <Window position="top-right"
	          content={
		          <div className={classes.wrapper}>
			          <div className={classes.groups}>
				          {gameStore.groups && gameStore.groups.map(group => <Group key={group.id} group={group} />)}
			          </div>
			          <div className={classes.legend}>
				          <div>
					          <div className={classes.authorsColor}></div>
					          Предложившие группу
				          </div>
				          <div>
					          <div className={classes.membersColor}></div>
					          Участники
				          </div>
			          </div>
		          </div>
	          }
	          header={"Предложенные группы"} />
	);
});

export default GroupsWindow;