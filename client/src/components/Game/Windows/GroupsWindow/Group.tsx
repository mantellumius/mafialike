import { observer } from "mobx-react-lite";
import * as React from "react";
import classes from "./GroupsWindow.module.css";
import { useRootStore } from "@stores/RootStore";
import classNames from "@lib/classNames";

const Group: React.FC<Props> = observer(({ group }) => {
	const {gameStore} = useRootStore();
	return (
	  <div className={classNames(classes.group, {[classes.selected]: gameStore.selectedGroupId === group.id})}
	       onClick={() => gameStore.selectedGroupId = group.id}>
		  <div className={classes.authors}>
			  {group.authors.map(a => a.nickname).join(", ")}
		  </div>
		  <div className={classes.members}>
			  {group.members.map(m => m.nickname).join(", ")}
		  </div>
		  <hr/>
	  </div>
	);
});
type Props = {
	group: GroupDto
};
export default Group;