import { observer } from "mobx-react-lite";
import * as React from "react";
import Window from "../Window/Window";
import classes from "./VotesHistory.module.css";
import { useRootStore } from "@stores/RootStore";
import classNames from "@lib/classNames";

const VotesHistory = observer(() => {
	const { gameStore } = useRootStore();
	if (!gameStore.votesHistoryWindowShown) return <></>;
	return (
	  <Window position="bottom-left" header={"Таблица голосований"}
	          onKeyDown={e => gameStore.setVotesHistoryIndex(parseInt(e.key) - 1)} content={
		  <div className={classes.votesHistory}>
			  <div className={classes.table}>
				  {gameStore.currentVotesHistory && <>
					  {<div className={classes.players}>
						  <div className="text-center font-bold">Имя</div>
						  {gameStore.currentVotesHistory.players.map((p) =>
									(<div key={p.numberFromCrown}
							      className={(gameStore.currentVotesHistory?.winner ?? []).includes(p.numberFromCrown) ? "bg-[var(--success-400)] rounded" : ""}>
										{p.nickname} <span className="font-bold">{p.numberFromCrown.toString()}</span>
									</div>)
						  )}
					  </div>
					  }
							<div className={classNames(classes.column, {}, ["bg-[var(--primary-500)] rounded"])}>
								<div className="text-center font-bold">Группа</div>
						  {gameStore.currentVotesHistory.history.groups.map((group, index) => (
									<div key={index}
							     className={classNames(classes.group, 
											{
												"rounded font-bold": (gameStore.currentVotesHistory?.winner ?? []).every((w, i) => group[i] === w)
											})}>
										{group?.join(" - ")}
									</div>
						  ))}
							</div>
					  {gameStore.currentVotesHistory.history.votings.map((voting, index) => (
								<div className={classes.column} key={index}>
									<div className="font-bold">{index + 1} голосование</div>
									{voting.map((votes) => (
							  <div className={(gameStore.currentVotesHistory?.winner ?? [])
											.every((w, i) => votes[i] === w) ? "rounded font-bold" : ""}>
								  {votes?.join(" - ")}
							  </div>))}
								</div>
					  ))}
						</>}
			  </div>
			  <hr />
			  <div className={classes.missionButtons}>
				  {gameStore.missions.map((_, index) => (
							<div onClick={() => (void gameStore.setVotesHistoryIndex(index))}
					     className={gameStore.votesHistoryIndex === index ? classes.missionButtonChosen : classes.missionButton}>
								{index + 1}
							</div>))
				  }
			  </div>
		  </div>
	  } />
	);
});

export default VotesHistory;