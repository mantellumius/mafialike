import { observer } from "mobx-react-lite";
import * as React from "react";
import Mission from "./Mission";
import classes from "./Missions.module.css";
import { useRootStore } from "@stores/RootStore";

const Missions = observer(() => {
	const {gameStore} = useRootStore();
	return (
	  <div className={classes.missions}>
		  {gameStore.missions.length > 0 && gameStore.missions.map((mission, index) => (
		  	<Mission key={index} mission={mission} />
		  ))}
	  </div>
	);
});

export default Missions;