import * as React from "react";
import classes from "./Missions.module.css";
import classNames from "@lib/classNames";

const Mission: React.FC<Props> = ({ mission }) => {
	return (
	  <div className={classNames(classes.mission, {
			[classes.failed]: mission.isFailed, 
			[classes.succeeded]: mission.isSucceeded})
		}>
		  {mission.reqMembers}
		  {mission.fails > 0 && <div className={classes.failsCounter}>{mission.fails}</div>}
	  </div>
	);
};

interface Props {
	mission: MissionDto
}

export default Mission;