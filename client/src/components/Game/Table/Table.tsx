import { observer } from "mobx-react-lite";
import * as React from "react";
import Players from "./Players/Players";
import classes from "./Table.module.css";
import TableActions from "./TableActions/TableActions";
import Missions from "./Missions/Missions";
import { useRootStore } from "@stores/RootStore";

const Table = observer(() => {
	const {gameStore} = useRootStore();
	return (
	  <div className={classes.table} style={{ "--n": gameStore.players.length } as React.CSSProperties}>
		  <Players />
		  {gameStore.isStarted && <Missions />}
		  <div className={classes.tableActions}>
			  <TableActions />
		  </div>
	  </div>
	);
});
export default Table;