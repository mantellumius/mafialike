import { observer } from "mobx-react-lite";
import * as React from "react";
import Button from "@UI/Button/Button";
import classes from "./Player.module.css";
import PlayerAvatar from "../PlayerAvatar/PlayerAvatar";
import { useRootStore } from "@stores/RootStore";
import { Jail } from "../PlayerAvatarModicitators/Jail/Jail";
import classNames from "@lib/classNames";

const Player: React.FC<Props> = observer(({
	player,
	style,
	id,
}) => {
	const {gameStore, roomStore} = useRootStore();

	return (
		<div className={classes.avatar} style={style}>
			{gameStore.crownedUserId === id && <div className={classes.crown} />}
			<div className={classNames(classes.name, {[classes.me]: player.user.id === gameStore.me?.user.id})}>{player.user.nickname}</div>
			<PlayerAvatar 
				onClick={gameStore.onPlayerClick.bind(gameStore, id)}
				isInMission={gameStore.checkIsInMission(player.user.id)}
				chosen={gameStore.checkIsChosen(player.user.id)}
				canBeChosen={gameStore.checkCanBeChosen(player)}
				revealed={gameStore.revealed}
				image={player.role?.image}
			>
				{player.settings?.canDoMissions === false && <Jail />}
			</PlayerAvatar>
			{
				!gameStore.isStarted &&
				roomStore.owner.id === gameStore.me?.user.id &&
				gameStore.me?.user.id !== id &&
				<Button text={"Выгнать"} background={"var(--danger-600)"}
					className={classes.kickButton} width={"100px"}
					onClick={() => gameStore.kick(id)} />
			}
		</div>
	);
});
type Props = {
	id: number,
	style?: React.CSSProperties,
	canBeChosen?: boolean,
	player: PlayerDto,
}
export default Player;