import { observer } from "mobx-react-lite";
import * as React from "react";
import classes from "./PlayerAvatar.module.css";
import classNames from "@lib/classNames";

const PlayerAvatar: React.FC<Props> = observer(({
	canBeChosen = false,
	isInMission = false,
	revealed = true,
	chosen = false,
	children,
	onClick,
	image,
}) => {
	return (
	  <div className={classes.avatarImageContainer} onClick={onClick}>
		  <div style={{ backgroundImage: `url(${(image && revealed) ? image : ""})` }}
		       className={classNames(classes.avatarImage,
					{
						[classes.canBeChosen]: canBeChosen && !chosen,
						[classes.inMission]: isInMission,
						[classes.chosen]: chosen
					}
				)}>
		  </div>
		  {children}
	  </div>
	);
});

interface Props {
	image?: string,
	style?: React.CSSProperties,
	revealed?: boolean,
	canBeChosen?: boolean,
	chosen?: boolean,
	isInMission?: boolean,
	onClick?: () => void,
	isJailed?: boolean,
	children?: React.ReactNode,
}
export default PlayerAvatar;