import { observer } from "mobx-react-lite";
import * as React from "react";
import Player from "./Player/Player";
import { useRootStore } from "@stores/RootStore";

const Players = observer(() => {
	const {gameStore} = useRootStore();

	return (
	  <>
		  {gameStore.players.map((player, index) => {
			  return <Player 
					player={player}
					style={{ "--i": index } as React.CSSProperties}
					id={player.user.id}
					key={player.user.id}
				/>;
		  })
		  }
	  </>
	);
});


export default Players;