import { observer } from "mobx-react-lite";
import * as React from "react";
import Ability from "./Ability/Ability";
import MissionActions from "./MissionActions/MissionActions";
import SitAndStand from "./SitAndStand/SitAndStand";
import Suggestion from "./Suggestion/Suggestion";
import { useRootStore } from "@stores/RootStore";
import { Vote } from "./Vote/Vote";
import { StartSuggestions } from "./StartSuggestions/StartSuggestions";

const TableActions = observer(() => {
	const {gameStore} = useRootStore();
	switch (gameStore.stage) {
		case "PreGame":
			return <SitAndStand />;
		case "PreSuggestions":
			return <StartSuggestions/>;
		case "Suggestions":
			return <Suggestion />;
		case "Voting":
			return <Vote/>;
		case "MissionActive":
			return <MissionActions />;
		case "Ability":
			return <Ability />;
		case "Mission":
			return <></>;
		case "Wait":
			return <></>;
		default:
			return <></>;
	}
});

export default TableActions;