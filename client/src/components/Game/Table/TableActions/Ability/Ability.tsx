import * as React from "react";
import Button from "@UI/Button/Button";
import { useRootStore } from "@stores/RootStore";

const Ability = () => {
	const {gameStore} = useRootStore();
	return (
	  <div className="flex gap-[30px]">
		  <Button text={"Применить"} width={200} onClick={() => {
			  gameStore.useAbility();
		  }} background={"var(--success-600)"} />
		  <Button text={"Пропустить"} width={200} onClick={() => {
			  gameStore.cancelAbility();
		  }}/>
	  </div>
	);
};

export default Ability;