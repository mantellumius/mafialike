import * as React from "react";
import Button from "@UI/Button/Button";
import classes from "./MissionActions.module.css";
import { useRootStore } from "@stores/RootStore";

const MissionActions = () => {
	const {gameStore} = useRootStore();
	return (
	  <div className={classes.missionActions}>
		  <Button text={"Успех"} width={200} background={"var(--success-600)"} onClick={() => gameStore.sendSuccess()}
		          disabled={gameStore.me?.settings?.canSuccess === false} />
		  <Button text={"Провал"} width={200} background={"var(--danger-600)"} onClick={() => gameStore.sendFail()}
		          disabled={gameStore.me?.settings?.canFail === false} />
	  </div>
	);
};

export default MissionActions;