import { FC } from "react";
import { useRootStore } from "@stores/RootStore";
import Button from "@UI/Button/Button";
import { observer } from "mobx-react";

const StartSuggestions: FC = observer(() => {
	const {gameStore, roomStore} = useRootStore();
	return (
		<Button text={"Начать стадию предложений"} 
			width={300}  
			onClick={() => gameStore.startSuggestions()} 
			disabled={!roomStore.isOwner} />
	);
});

export { StartSuggestions };