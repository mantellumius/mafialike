import { FC } from "react";
import Button from "@UI/Button/Button";
import { useRootStore } from "@stores/RootStore";
import { observer } from "mobx-react";

const Vote: FC = observer(() => {
	const {gameStore} = useRootStore();
	return (
		 <Button text={"Проголосовать"} 
			onClick={() => gameStore.vote()} 
		 	width={300}
			disabled={!gameStore.selectedGroupId} />
	);
});

export { Vote };