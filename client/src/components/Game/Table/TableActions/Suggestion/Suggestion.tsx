import { observer } from "mobx-react-lite";
import * as React from "react";
import Button from "@UI/Button/Button";
import classes from "./Suggestion.module.css";
import { useRootStore } from "@stores/RootStore";

const Suggestion = observer(() => {
	const {gameStore} = useRootStore();
	return (
	  <div className={classes.suggestion}>
		  <Button text={"Предложить группу"} onClick={gameStore.suggestGroup.bind(gameStore)} width={300} />
		  <div className={classes.counter}>{gameStore.currentGroupSuggestion.length} / {gameStore.currentMission.reqMembers}</div>
	  </div>
	);
});

export default Suggestion;