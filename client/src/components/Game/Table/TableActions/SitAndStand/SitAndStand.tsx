import { observer } from "mobx-react-lite";
import * as React from "react";
import Button from "@UI/Button/Button";
import classes from "./SitAndStand.module.css";
import { useRootStore } from "@stores/RootStore";

const SitAndStand = observer(() => {
	const {gameStore} = useRootStore();
	return (
	  <div className={classes.centerButtons}>
		  <Button text={"Сесть"} onClick={() => gameStore.sitDown()}
		          background={"var(--success-500)"} disabled={gameStore.locked} />
		  <Button text={"Встать"} onClick={() => gameStore.standUp()}
		          background={"var(--danger-500)"} disabled={gameStore.locked} />
	  </div>
	);
});

export default SitAndStand;