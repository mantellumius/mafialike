import * as React from "react";
import { PropsWithChildren } from "react";
import { Navigate } from "react-router-dom";
import { observer } from "mobx-react-lite";
import { useRootStore } from "@stores/RootStore";

const ProtectedRoute: React.FC<PropsWithChildren> = observer(({ children }) => {
	const {authStore} = useRootStore();
	if (authStore.fetching)
		return <></>;
	if (!authStore.authenticated)
		return <Navigate to={"/login"} />;
	return <>{children}</>;
});

export default ProtectedRoute;