import * as React from "react";
import { Route, Routes } from "react-router-dom";
import Login from "@pages/Auth/Login";
import Registration from "@pages/Auth/Registration";
import CreateRoom from "@pages/CreateRoom/CreateRoom";
import CreateSet from "@pages/CreateSet/CreateSet";
import EditSet from "@pages/EditSet/EditSet";
import GameSetPage from "@pages/GameSet/GameSetPage";
import GameSetsList from "@pages/GameSetsList/GameSetsList";
import Room from "@pages/Room";
import Rules from "@pages/Rules/Rules";
import ProtectedRoute from "./ProtectedRoute/ProtectedRoute";

const MainRouter = () => {
	return (
		<Routes>
			<Route path={"/login"} element={<Login />} />
			<Route path={"/registration"} element={<Registration />} />
			<Route path={"/room/:id"} element={<ProtectedRoute><Room /></ProtectedRoute>} />
			<Route path={"/create-room"} element={<ProtectedRoute><CreateRoom /></ProtectedRoute>} />
			<Route path={"/rules"} element={<Rules />} />
			<Route path={"/game-sets"} element={<ProtectedRoute><GameSetsList /></ProtectedRoute>} />
			<Route path={"/game-sets/:id"} element={<ProtectedRoute><GameSetPage /></ProtectedRoute>} />
			<Route path={"/game-sets/create"} element={<ProtectedRoute><CreateSet /></ProtectedRoute>} />
			<Route path={"/game-sets/edit"} element={<ProtectedRoute><EditSet /></ProtectedRoute>} />
		</Routes>
	);
};

export default MainRouter;