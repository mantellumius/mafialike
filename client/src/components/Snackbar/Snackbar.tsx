import { observer } from "mobx-react-lite";
import * as React from "react";
import classes from "./Snackbar.module.css";
import SnackbarMessage from "./SnackbarMessage";
import { useRootStore } from "@stores/RootStore";

const Snackbar: React.FC = observer(() => {
	const { snackbarStore } = useRootStore();
	return (
		<div className={classes.snackbar}>
			{snackbarStore.messages.map((message, index) => (<SnackbarMessage key={index} snackbarMessage={message} />))}
		</div>
	);
});
export default Snackbar;