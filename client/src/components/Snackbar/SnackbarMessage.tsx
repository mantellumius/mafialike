import {
	CheckCircleIcon,
	ExclamationCircleIcon,
	ExclamationTriangleIcon,
	InformationCircleIcon,
	XMarkIcon
} from "@heroicons/react/24/outline";
import { observer } from "mobx-react-lite";
import * as React from "react";
import { ReactElement, useEffect } from "react";
import classes from "./Snackbar.module.css";

const SnackbarMessage: React.FC<Props> = observer(({ snackbarMessage }) => {
	const icons: { [key: string]: ReactElement } = {
		"Error": <ExclamationCircleIcon stroke={"var(--danger-400)"} />,
		"Warning": <ExclamationTriangleIcon stroke={"var(--warning-400)"} />,
		"Info": <InformationCircleIcon stroke={"var(--info-400)"} />,
		"Success": <CheckCircleIcon stroke={"var(--success-400)"} />
	};
	useEffect(() => {
		const remove = () => {
			if (typeof snackbarMessage.remove === "function")
				snackbarMessage.remove();
		};
		setTimeout(() => remove(), snackbarMessage.duration);
	}, [snackbarMessage]);
	return (
	  <div className={classes.message} onClick={() => snackbarMessage.duration = 0}
	       style={{ "--message-duration": `${(snackbarMessage.duration ?? 0) / 1000 - 1}s` } as React.CSSProperties}>
		  <div className={classes.icon}>
			  {icons[snackbarMessage.message.type]}
		  </div>
		  <div className={classes.text}>{snackbarMessage.message.text}</div>
		  <XMarkIcon className={classes.closeIcon}/>
	  </div>
	);
});
type Props = {
	snackbarMessage: SnackbarMessage;
}
export default SnackbarMessage;