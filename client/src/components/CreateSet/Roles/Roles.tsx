import { observer } from "mobx-react-lite";
import * as React from "react";
import PlayerAvatar from "../../Game/Table/Players/PlayerAvatar/PlayerAvatar";
import classes from "./Roles.module.css";
import { useRootStore } from "@stores/RootStore";

const Roles = observer(() => {
	const {gameSetStore} = useRootStore();
	return (
	  <div className={classes.roles}>
		  {gameSetStore?.roles && gameSetStore.roles.map(((role, index) =>
				<PlayerAvatar key={index} image={role.image}
			              onClick={() => gameSetStore.chooseRole(index)}
			              chosen={gameSetStore?.chosenRole?.id === role.id} />))}
	  </div>
	);
});

export default Roles;