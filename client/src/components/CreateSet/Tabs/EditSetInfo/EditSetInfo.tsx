import { observer } from "mobx-react-lite";
import GameSetInfoForm from "../../SetInfoForm/GameSetInfoForm";
import classes from "./EditSetInfo.module.css";
import { useRootStore } from "@stores/RootStore";

const EditSetInfo = observer(() => {
	const {gameSetStore} = useRootStore();
	async function onSubmit(gameSet: GameSet) {
		await gameSetStore.update(gameSet);
	}

	return (
	  <div className={classes.container}>
		  <GameSetInfoForm onSubmit={async (gameSet) => await onSubmit(gameSet)} initState={gameSetStore.gameSet} submitButtonText={"Сохранить"} />
	  </div>
	);
});

export default EditSetInfo;