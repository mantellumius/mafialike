import * as React from "react";
import Button from "@UI/Button/Button";
import { useRootStore } from "@stores/RootStore";

const DeleteGameSet = () => {
	const {gameSetStore} = useRootStore();
	return (
	  <div>
		  <Button text={"Удалить набор (Это действие нельзя будет обратить)"}
		          onClick={() => void gameSetStore.deleteGameSet()} background={"var(--danger-600)"} />
	  </div>
	);
};

export default DeleteGameSet;