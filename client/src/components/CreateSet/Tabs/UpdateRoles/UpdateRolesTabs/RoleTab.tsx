import * as React from "react";
import { useEffect, useState } from "react";
import FormInput from "@UI/FormInput/FormInput";
import FormInputArea from "@UI/FormInput/FormInputArea";
import classes from "./Tab.module.css";
import { ButtonWithLoading } from "@UI/ButtonWithLoading/ButtonWithLoading";

const RoleTab: React.FC<Props> = ({ getInitialRole, submitButtonText, onSubmit }) => {
	const [role, setRole] = useState<Role>(getInitialRole);
	useEffect(() => setRole(getInitialRole()), [getInitialRole]);

	return (
	  <div className={classes.tab}>
		  <form>
			  <FormInput name={"name"} label={"Название роли"} value={role.name}
			             onChange={(e) => setRole({
				             ...role,
				             name: e.target.value
			             })} />
			  <FormInput name={"image"} label={"Аватар роли"} value={role.image}
			             onChange={(e) => setRole({
				             ...role,
				             image: e.target.value
			             })} />
			  <FormInputArea name={"description"} label={"Описание роли"} value={role.description}
			             onChange={(e) => setRole({
				             ...role,
				             description: e.target.value
			             })} maxLength={255} />
			  <ButtonWithLoading onClick={onSubmit.bind(this, role)} text={submitButtonText} type={"submit"} background={"var(--success-600)"}/>
		  </form>
	  </div>
	);
};
type Props = {
	getInitialRole: () => Role;
	submitButtonText: string;
	onSubmit: (role: Role) => Promise<void>;
}
export default RoleTab;