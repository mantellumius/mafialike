import { observer } from "mobx-react-lite";
import * as React from "react";
import Checkbox from "@UI/Checkbox/Checkbox";
import FormInput from "@UI/FormInput/FormInput";
import Selector from "@UI/Selector/Selector";
import classes from "./Tab.module.css";
import { useRootStore } from "@stores/RootStore";
import removeByValue from "@lib/removeByValue";
import { ButtonWithLoading } from "@UI/ButtonWithLoading/ButtonWithLoading";

const RoleSettingsTab = observer(() => {
	const {gameSetStore} = useRootStore();

	async function onSubmit() {
		const resultRoleSettings = await gameSetStore.saveRoleSettings();
		const resultSpecialAbilitySettings = await gameSetStore.saveSpecialAbilitySettings();
		if (resultRoleSettings && resultSpecialAbilitySettings) {
			gameSetStore.successfullySaved();
		}
	}

	return (
	  <div className={classes.tab}>
		  <form>
			  <div>
				  <strong>Общие настройки:</strong>
				  <div className={classes.section}>
					  <Checkbox name={"canSuccess"} title={"Может ли роль выполнить миссию"}
					            checked={gameSetStore.chosenRoleSettings.canSuccess}
					            onChange={(e) => gameSetStore.chosenRoleSettings.canSuccess = e.target.checked} />
					  <Checkbox name={"canFail"} title={"Может ли роль провалить миссию"}
					            checked={gameSetStore.chosenRoleSettings.canFail}
					            onChange={(e) => gameSetStore.chosenRoleSettings.canFail = e.target.checked} />
					  <Selector options={[{ value: "Max", label: "Максимум" }, { value: "ExactlyOne", label: "Один" }]}
					            name={"amount"} label={"Количество роли"}
					            value={gameSetStore.chosenRoleSettings.amount}
					            onChange={(e) => gameSetStore.chosenRoleSettings.amount = e.target.value as RoleAmount} />
					  <Selector options={[{ value: "Red", label: "Красный" }, { value: "Blue", label: "Синий" }]}
					            name={"side"} label={"Сторона роли"} value={gameSetStore.chosenRoleSettings.side}
					            onChange={(e) => gameSetStore.chosenRoleSettings.side = e.target.value as Side} />
				  </div>
			  </div>
			  <div>
				  <strong>Кого может видеть:</strong>
				  <div className={classes.section}>
					  {gameSetStore.roles.filter(role => role.id !== gameSetStore.chosenRole?.id).map(role => (
							<Checkbox key={role.name} name={role.name} title={`${role.name}`}
						          onChange={(e) => e.target.checked ?
						            gameSetStore.chosenRoleSettings.canSee.push(role.id) :
						            removeByValue(gameSetStore.chosenRoleSettings.canSee, role.id)
						          }
						          checked={gameSetStore.chosenRoleSettings.canSee.includes(role.id)} />))}
				  </div>
			  </div>
			  <div>
				  <strong>Способность: </strong>
				  <div className={classes.section}>
					  <Selector options={[
							{
								value: "DisableFail",
								label: "Отключить у игрока возможность проваливать миссию"
							}, {
								value: "ForceSuccess",
								label: "Принуждает роль успешно выполнить миссию"
							}, {
								value: "RevealPlayerActionInLastMission",
								label: "Раскрыть действие игрока в последней миссии"
							}, {
								value: "Jail",
								label: "Посадить игрока в тюрьму(запрещает этому игроку быть выбранным в группу и отключает его способность)."
							}, {
								value: "JailAndReveal",
								label: "Посадить игрока в тюрьму и раскрыть его роль(запрещает этому игроку быть выбранным в группу и отключает его способность)."
							}, {
								value: "NoAbility",
								label: "Нет способности"
							}
						] as { value: SpecialAbilityType, label: string }[]}
					            name={"type"} label={"Тип способности"}
					            value={gameSetStore.chosenRoleAbilitySettings.type}
					            onChange={(e) => gameSetStore.chosenRoleAbilitySettings.type = e.target.value as SpecialAbilityType} />
					  {gameSetStore.chosenRoleAbilitySettings.type !== "NoAbility" &&
												<>
													<FormInput name={"amount"}
														           label={"Количество использований"}
														           value={gameSetStore.chosenRoleAbilitySettings.amount}
														           type={"number"}
														           onChange={(e) => gameSetStore.chosenRoleAbilitySettings.amount = parseInt(e.target.value)} />
													<div>На каких миссиях разрешено использовать способность:</div>
													<div className="flex gap-5">
														{[0, ...gameSetStore.missionsSettings.map(m => m.number)].map(number => (<Checkbox key={number}
								                                                         onChange={(e) => e.target.checked ?
								                                                           gameSetStore.chosenRoleAbilitySettings.allowedMissions.push(number) :
								                                                           gameSetStore.chosenRoleAbilitySettings.allowedMissions = gameSetStore.chosenRoleAbilitySettings.allowedMissions.filter(n => n !== number)}
								                                                         checked={gameSetStore.chosenRoleAbilitySettings.allowedMissions.includes(number)}
								                                                         title={number.toString()} />))}
													</div>
													<div>На какие роли эта работает эта способность:</div>
													<div className="flex gap-5">
														{gameSetStore.roles.map(role => (<Checkbox key={role.id}
								                                           onChange={(e) => e.target.checked ?
								                                             gameSetStore.chosenRoleAbilitySettings.allowedRoleIds.push(role.id) :
								                                             gameSetStore.chosenRoleAbilitySettings.allowedRoleIds = gameSetStore.chosenRoleAbilitySettings.allowedRoleIds.filter(id => id !== role.id)}
								                                           checked={gameSetStore.chosenRoleAbilitySettings.allowedRoleIds.includes(role.id)}
								                                           title={role.name.toString()} />))}
													</div>
												</>
					  }
				  </div>
			  </div>
			  <ButtonWithLoading onClick={onSubmit} text={"Сохранить"} type={"submit"} color="var(--neutral-50)" background={"var(--success-600)"} />
		  </form>
	  </div>
	);
});
export default RoleSettingsTab;