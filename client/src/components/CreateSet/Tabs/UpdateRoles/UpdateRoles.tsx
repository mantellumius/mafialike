import { observer } from "mobx-react-lite";
import * as React from "react";
import Button from "@UI/Button/Button";
import PlayerAvatar from "../../../Game/Table/Players/PlayerAvatar/PlayerAvatar";
import classes from "./UpdateRole.module.css";
import { useRootStore } from "@stores/RootStore";

const UpdateRoles = observer(() => {
	const {gameSetStore} = useRootStore();
	return (
	  <div className={classes.container}>
		  <div className={classes.chosenRole}>
			  <div className="w-[200px] h-[200px] rounded-full">
				  <PlayerAvatar image={gameSetStore?.chosenRole?.image ?? ""} />
			  </div>
			  <div className={classes.roleName}>
				  {gameSetStore?.chosenRole?.name}
			  </div>
			  <div className={classes.roleDescription}>
				  {gameSetStore?.chosenRole?.description}
			  </div>
		  </div>
		  <div className={classes.buttons}>
			  <Button text={"Создать роль"} background={"var(--success-600)"}
			          onClick={() => gameSetStore.setCreateRoleTab()} />
			  <Button text={"Обновить роль"} onClick={() => gameSetStore.setUpdateRoleTab()}
			          disabled={gameSetStore.chosenRole === undefined} />
			  <Button text={"Обновить настройки роли"} onClick={() => void gameSetStore.setUpdateRoleSettingsTab()}
			          disabled={gameSetStore.chosenRole === undefined} />
			  <Button text={"Удалить роль"} background={"var(--danger-600)"}
			          onClick={() => void gameSetStore.deleteRole()} disabled={gameSetStore.chosenRole === undefined} />
		  </div>
	  </div>
	);
});

export default UpdateRoles;