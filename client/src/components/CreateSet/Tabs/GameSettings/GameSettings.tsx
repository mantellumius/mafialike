import { TrashIcon } from "@heroicons/react/24/outline";
import { observer } from "mobx-react-lite";
import * as React from "react";
import Button from "@UI/Button/Button";
import FormInput from "@UI/FormInput/FormInput";
import classes from "./GameSettings.module.css";
import { useRootStore } from "@stores/RootStore";
import { ButtonWithLoading } from "@UI/ButtonWithLoading/ButtonWithLoading";

const GameSettings = observer(() => {
	const {gameSetStore} = useRootStore();
	return (
	  <div className={classes.container}>
		  <strong>Миссии: </strong>
		  {gameSetStore.missionsSettings && gameSetStore.missionsSettings.map(mission =>
				<div className="flex gap-4" key={mission.number}>
					<FormInput value={mission.requiredMembers}
				           onChange={(e) => mission.requiredMembers = (parseInt(e.target.value) || 0) <= 0 ? 1 : parseInt(e.target.value)}
				           label={"Количество участников"} type={"number"} />
					<FormInput value={mission.requiredFails}
				           onChange={(e) => mission.requiredFails = (parseInt(e.target.value) || 0) <= 0 ? 1 : parseInt(e.target.value)}
				           label={"Количество провалов"} type={"number"} />
					<Button
				  onClick={() => gameSetStore.deleteMissionSettings(mission.number)}
				  background={"transparent"} hoverBackground={"var(--neutral-400)"} width={"100px"} height={"50px"}>
						<TrashIcon stroke="var(--danger-600)"/>
					</Button>
				</div>
		  )}
		  <Button text={"Добавить миссию"} onClick={() => gameSetStore.addMissionSettings()} />
		  <ButtonWithLoading text={"Сохранить"} onClick={gameSetStore.saveMissions.bind(gameSetStore)} background={"var(--success-600)"} />
	  </div>
	);
});

export default GameSettings;