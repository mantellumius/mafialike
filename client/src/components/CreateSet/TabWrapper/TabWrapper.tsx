import * as React from "react";
import { PropsWithChildren } from "react";
import classes from "./TabWrapper.module.css";

const TabWrapper: React.FC<PropsWithChildren> = ({ children }) => {
	return (
	  <div className={classes.tabWrapper}>
		  {children}
	  </div>
	);
};
export default TabWrapper;