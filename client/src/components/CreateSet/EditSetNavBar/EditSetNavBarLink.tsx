import * as React from "react";
import classes from "./EditSetNavBar.module.css";

const EditSetNavBarLink: React.FC<Props> = ({ text, onClick }) => {
	return (
	  <div className={classes.link} onClick={onClick}>
		  <span className={classes.text}>{text}</span>
		  <div className={classes.underline}></div>
	  </div>
	);
};
type Props = {
	text: string;
	onClick: () => void;
}
export default EditSetNavBarLink;