import * as React from "react";
import { Tab } from "@stores/GameSetStore";
import classes from "./EditSetNavBar.module.css";
import EditSetNavBarLink from "./EditSetNavBarLink";

const EditSetNavBar: React.FC<Props> = ({ tabs, setTab, activeTabIndex }) => {
	return (
	  <div className={classes.navBar}
	       style={{ "--underline-margin-left": `calc(var(--create-set-link-width) * ${activeTabIndex ?? 0})` } as React.CSSProperties}>
		  {tabs.filter(tab => !!tab.text).map((tab: Tab, index: number) => {
			  return <EditSetNavBarLink key={index} text={tab.text} onClick={() => setTab(tab.text)} />;
		  })}
		  <hr />
	  </div>
	);
};
type Props = {
	tabs: Tab[],
	setTab: (text: string) => void,
	activeTabIndex?: number
}

export default EditSetNavBar;