import * as React from "react";
import { useEffect, useState } from "react";
import FormInput from "@UI/FormInput/FormInput";
import FormInputArea from "@UI/FormInput/FormInputArea";
import classes from "./GameSetInfoForm.module.css";
import { ButtonWithLoading } from "@UI/ButtonWithLoading/ButtonWithLoading";

const GameSetInfoForm: React.FC<Props> = ({ initState, onSubmit, submitButtonText }) => {
	const [gameSet, setGameSet] = useState<GameSet>({
		name: "",
		image: "",
		backgroundImage: "",
		description: ""
	});
	useEffect(() => initState && setGameSet(initState), [initState]);
	
	return (
	  <form className={classes.form}>
		  <FormInput name={"name"} label={"Название"} value={gameSet.name}
		             onChange={(e) => setGameSet({ ...gameSet, name: e.target.value })} />
		  <FormInput name={"image"} label={"Картинка карточки(URL)"} value={gameSet.image}
		             onChange={(e) => setGameSet({ ...gameSet, image: e.target.value })} required={false}/>
		  <FormInput name={"backgroundImage"} label={"Картинка фона(URL)"} value={gameSet.backgroundImage}
		             onChange={(e) => setGameSet({ ...gameSet, backgroundImage: e.target.value })} required={false}/>
		  <FormInputArea name={"description"} label={"Полное описание"} value={gameSet.description}
		             onChange={(e) => setGameSet({ ...gameSet, description: e.target.value })} maxLength={3000}/>
		  <ButtonWithLoading text={submitButtonText} type={"submit"} background={"var(--success-600)"} onClick={onSubmit.bind(this, gameSet)}/>
	  </form>
	);
};
type Props = {
	initState?: GameSet | WithId<GameSet>;
	onSubmit: (gameSet: GameSet) => Promise<void>;
	submitButtonText: string;
}
export default GameSetInfoForm;