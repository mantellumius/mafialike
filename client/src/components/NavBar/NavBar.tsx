import { ArrowRightOnRectangleIcon } from "@heroicons/react/24/outline";
import { observer } from "mobx-react-lite";
import * as React from "react";
import Button from "@UI/Button/Button";
import classes from "./NavBar.module.css";
import NavBarLink from "./NavBarLink/NavBarLink";
import { useRootStore } from "@stores/RootStore";
import classNames from "@lib/classNames";


const NavBar: React.FC = observer(() => {
	const { authStore } = useRootStore();

	return (
		<header className={classes.navBar}>
			<div className={classes.side}>
				<NavBarLink text={"Правила"} to={"/rules"} />
				<NavBarLink text={"Создать комнату"} to={"/create-room"} />
				<NavBarLink text={"Создать набор"} to={"/game-sets/create"} />
				<NavBarLink text={"Список наборов"} to={"/game-sets"} />
			</div>
			<div className={classes.side}>
				{authStore.authenticated ?
					<>
						<div className={classes.nickname}>{authStore?.user?.nickname}</div>
						<Button onClick={() => void authStore.logout()} className={classes.logout} width={50}
							background={"var(--primary-800)"} hoverBackground={"var(--primary-900)"}>
							<ArrowRightOnRectangleIcon stroke={"var(--neutral-200)"} />
						</Button>
					</> :
					<>
						<NavBarLink text={"Войти"} to={"/login"}
							className={classNames(classes.auth, {}, [classes.login])} enableUnderline={false} />
						<NavBarLink text={"Создать аккаунт"} to={"/registration"}
							className={classNames(classes.auth, {}, [classes.register])} enableUnderline={false} />
					</>
				}
			</div>
		</header>
	);
});
export default NavBar;