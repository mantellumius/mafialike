import * as React from "react";
import { Link } from "react-router-dom";
import classes from "./NavBarLink.module.css";
import classNames from "@lib/classNames";

const NavBarLink = ({ to, text, className, enableUnderline = true}: Params) => {
	return (
	  <Link to={to} className={classNames(classes.link, {}, [className])}>
		  {enableUnderline && <div className={classes.active}></div>}
		  {text}
	  </Link>
	);
};
type Params = {
	to: string;
	text: string;
	className?: string;
	enableUnderline?: boolean;
}
export default NavBarLink;