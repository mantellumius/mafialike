import * as React from "react";
import { useState } from "react";
import classes from "./Like.module.css";
import classNames from "@lib/classNames";

const Like: React.FC<Props> = ({ isLikedInit, onLike }) => {
	const [isLiked, setIsLiked] = useState(isLikedInit);
	return (
	  <div className={classNames(classes.like, {[classes.liked]: isLiked})} onClick={
		  () => {
			  onLike(!isLiked);
			  setIsLiked(!isLiked);
		  }
	  }>
		  <svg viewBox="0 0 25 20" fill="none" xmlns="http://www.w3.org/2000/svg">
			  <path
					d="M11.9649 3.12832C8.29171 -2.5454 0.857422 0.545461 0.857422 6.72603C0.857422 11.3672 11.0494 18.6272 11.9649 19.5712C12.8866 18.6272 22.5717 11.3672 22.5717 6.72603C22.5717 0.592318 15.6449 -2.5454 11.9649 3.12832Z"
					fill="#3E4373" />
		  </svg>
	  </div>
	);
};
type Props = {
	isLikedInit: boolean,
	onLike: (state: boolean) => void
}
export default Like;