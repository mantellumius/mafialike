export default function removeByValue<T>(arr: Array<T>, value: T) {
	arr.splice(arr.indexOf(value), 1);
}