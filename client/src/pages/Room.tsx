import * as React from "react";
import { useEffect } from "react";
import { useParams } from "react-router-dom";
import GameToolBar from "@components/Game/GameToolBar/GameToolBar";
import Table from "@components/Game/Table/Table";
import GameSetWindow from "@components/Game/Windows/GameSetWindow/GameSetWindow";
import GroupsWindow from "@components/Game/Windows/GroupsWindow/GroupsWindow";
import VotesHistory from "@components/Game/Windows/VotesHistory/VotesHistory";
import { useRootStore } from "@stores/RootStore";

const Room: React.FC = () => {
	const { roomStore: roomStore, gameStore, themeStore } = useRootStore();
	const { id } = useParams() as { id: string };
	useEffect(() => {
		themeStore.resetBackgroundImage();
		roomStore?.join(id);
		gameStore.changeGameSet(parseInt(localStorage.getItem("lastGameSetId") ?? "-1"));
	}, [gameStore, id, roomStore, themeStore]);
	return (
			  <div className="h-full flex justify-center items-center mb-[75px]">
				  <Table />
				  <GameToolBar />
				  <GroupsWindow />
				  <GameSetWindow />
				  <VotesHistory />
			  </div>
	);
};

export default Room;