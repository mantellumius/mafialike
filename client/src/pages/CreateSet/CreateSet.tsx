import * as React from "react";
import GameSetInfoForm from "@components/CreateSet/SetInfoForm/GameSetInfoForm";
import classes from "./CreateSet.module.css";
import useSetBackgroundImage from "@hooks/useSetBackgroundImage";
import { useRootStore } from "@stores/RootStore";

const CreateSet: React.FC = () => {
	const { gameSetStore, themeStore } = useRootStore();
	useSetBackgroundImage(themeStore);

	async function onSubmit(gameSet: GameSet) {
		await gameSetStore.create(gameSet);
	}

	return (
		<div className={classes.container}>
			<GameSetInfoForm onSubmit={onSubmit} submitButtonText={"Создать набор"} />
		</div>
	);
};

export default CreateSet;