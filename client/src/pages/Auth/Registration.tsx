import * as React from "react";
import { useState } from "react";
import { useNavigate } from "react-router-dom";
import login_image from "@assets/login_image.png";
import Button from "@UI/Button/Button";
import FormInput from "@UI/FormInput/FormInput";
import classes from "./AuthForm.module.css";
import { useRootStore } from "@stores/RootStore";

const Registration: React.FC= () => {
	const {authStore} = useRootStore();
	const navigate = useNavigate();
	const [credentials, setCredentials] = useState<{ login: string, email: string, password: string }>({
		login: "",
		email: "",
		password: ""
	});
	const [error, setError] = useState<string>("");
	const [emailError, setEmailError] = useState<string>("");

	async function onSubmit(e: React.FormEvent<HTMLFormElement>) {
		e.preventDefault();
		const re = /^[\w-.]+@([\w-]+.)+[\w-]{2,4}$/;
		if (!re.test(credentials.email.toLowerCase()))
			return setEmailError("Указана некорректная почта.");
		const response = await authStore.register(credentials.login, credentials.email, credentials.password);
		if (!response) return;
		if (response.status === 200)
			navigate("/login");
		else if (response.status === 400)
			setError("Пользователь с такими логином или почтой уже существует.");
	}

	return (
	  <form className={classes.form} onSubmit={(e) => {
		  void onSubmit(e);
	  }}>
		  <img src={login_image} className={classes.image} alt={"image"} />
		  <FormInput label={"Логин"} name={"login"} value={credentials.login} error={error}
		             onChange={(e) => {
			             setCredentials({ ...credentials, login: e.target.value });
			             setError("");
		             }} />
		  <FormInput label={"Почта"} name={"email"} value={credentials.email} onChange={(e) => {
			  setCredentials({ ...credentials, email: e.target.value });
			  setEmailError("");
			  setError("");
		  }} error={error || emailError} />
		  <FormInput label={"Пароль"} type={"password"} name={"password"} value={credentials.password}
		             onChange={(e) => {
			             setCredentials({ ...credentials, password: e.target.value });
		             }} />
		  <Button text={"Создать аккаунт"} type={"submit"} background={"var(--primary-700)"}/>
	  </form>
	);
};

export default Registration;