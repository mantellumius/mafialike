import * as React from "react";
import { useState } from "react";
import { useNavigate } from "react-router-dom";
import login_image from "@assets/login_image.png";
import Button from "@UI/Button/Button";
import FormInput from "@UI/FormInput/FormInput";
import classes from "./AuthForm.module.css";
import { useRootStore } from "@stores/RootStore";

const Login: React.FC = () => {
	const { authStore } = useRootStore();
	const navigate = useNavigate();
	const [credentials, setCredentials] = useState<{ login: string, password: string }>({ login: "", password: "" });
	const [error, setError] = useState<string>("");

	async function onSubmit(e: React.FormEvent<HTMLFormElement>) {
		e.preventDefault();
		const response = await authStore.login(credentials.login, credentials.password);
		if (!response) return;
		if (response.status === 200)
			navigate("/");
		else if (response.status === 400)
			setError("Неверный логин или пароль.\n");
	}

	return (
		<form className={classes.form} onSubmit={(e) => {
			void onSubmit(e);
		}}>
			<img src={login_image} className={classes.image} alt={"image"} />
			<FormInput label={"Логин или Почта"} name={"login"} value={credentials.login}
				onChange={(e) => {
					setCredentials({ ...credentials, login: e.target.value });
					setError("");
				}} />
			<FormInput label={"Пароль"} type={"password"} name={"password"} value={credentials.password}
				onChange={(e) => {
					setCredentials({ ...credentials, password: e.target.value });
					setError("");
				}} error={error} />
			<Button text={"Войти"} type={"submit"} background={"var(--primary-700)"} />
		</form>
	);
};
export default Login;