import * as React from "react";
import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import PlayerAvatar from "@components/Game/Table/Players/PlayerAvatar/PlayerAvatar";
import Paper from "@UI/Paper/Paper";
import classes from "./GameSetPage.module.css";
import useSetBackgroundImage from "@hooks/useSetBackgroundImage";
import useApi from "@hooks/useApi";
import { useRootStore } from "@stores/RootStore";

const GameSetPage: React.FC = () => {
	const {themeStore} = useRootStore();
	const { id } = useParams();
	const [gameSet, setGameSet] = useState<GameSet & { login: string } | null>(null);
	const [roles, setRoles] = useState<RoleDto[]>([]);
	useSetBackgroundImage(themeStore, gameSet?.backgroundImage);
	const { response: gameSetResponse } = useApi({
		url: `/game-sets/${id ?? ""}/?withAuthor=true`,
		method: "get"
	});
	const { response: rolesResponse } = useApi({
		url: `/game-sets/${id ?? ""}/roles`,
		method: "get"
	});
	useEffect(() => {
		if (gameSetResponse && rolesResponse) {
			setGameSet(gameSetResponse as GameSet & { login: string });
			setRoles(rolesResponse as RoleDto[]);
		}
	}, [gameSetResponse, rolesResponse]);
	
	return (
		<Paper>
			<div className="flex flex-col gap-10">
				{gameSet &&
					<div className="flex flex-col gap-6">
						<div className="flex justify-between w-100 items-center">
							<div className={classes.setName}>{gameSet.name}</div>
							<div className={classes.setAuthor}>Автор: {gameSet.login}</div>
						</div>
						<div className={classes.setDescription}>{gameSet.description}</div>
					</div>}
				{roles.length > 0 &&
					<div className={classes.roles}>
						{roles.map((role, index) => (
							<div key={index}>
								<div className="w-[150px] h-[150px]">
									<PlayerAvatar image={role.image} />
								</div>
								<div className={classes.roleName}>
									{role.name}
								</div>
								<div className={classes.roleDescription}>
									{role.description}
								</div>
							</div>))}
					</div>
				}
			</div>
		</Paper>
	);
};

export default GameSetPage;