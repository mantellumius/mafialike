import { ClipboardIcon } from "@heroicons/react/24/outline";
import { observer } from "mobx-react-lite";
import * as React from "react";
import { useEffect } from "react";
import Button from "@UI/Button/Button";
import classes from "./CreateRoom.module.css";
import { useRootStore } from "@stores/RootStore";
import { ButtonWithLoading } from "@UI/ButtonWithLoading/ButtonWithLoading";

const CreateRoom: React.FC = observer(() => {
	const { roomStore, snackbarStore, themeStore } = useRootStore();
	useEffect(() => themeStore.resetBackgroundImage(),[themeStore]);
	const url = `${window.location.origin}/room/${roomStore.createdRoomId}`;
	return (
	  <div className={classes.createRoom}>
		  <div className={classes.urlContainer}>
			  <div className={classes.url}>
				  {url}
			  </div>
			  <Button onClick={() => {
				  void navigator.clipboard.writeText(url);
				  snackbarStore.addMessage({ message: { type: "Success", text: "Ссылка скопирована." } });
			  }} className={classes.copy}>
				  <ClipboardIcon stroke={"var(--neutral-100)"}/>
			  </Button>
		  </div>
		  <ButtonWithLoading onClick={roomStore.create.bind(roomStore)} text={"Создать комнату"} />
	  </div>
	);
});

export default CreateRoom;