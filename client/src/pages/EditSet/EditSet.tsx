import { observer } from "mobx-react-lite";
import * as React from "react";
import { useEffect } from "react";
import Roles from "@components/CreateSet/Roles/Roles";
import TabWrapper from "@components/CreateSet/TabWrapper/TabWrapper";
import classes from "./EditSet.module.css";
import { useRootStore } from "@stores/RootStore";
import EditSetNavBar from "@components/CreateSet/EditSetNavBar/EditSetNavBar";

const EditSet: React.FC = observer(() => {
	const { gameSetStore } = useRootStore();
	useEffect(() => {
		gameSetStore.changeSetId(parseInt(localStorage.getItem("gameSetId") ?? "-1"));
		void gameSetStore.fetch();
	}, [gameSetStore]);
	function setTab(text: string) {
		const chosenTab = gameSetStore.tabs.find(tab => tab.text === text);
		if (chosenTab)
			gameSetStore.currentTab = chosenTab;
	}

	return (
		<div className={classes.container}>
			<EditSetNavBar tabs={gameSetStore.tabs} setTab={setTab} activeTabIndex={gameSetStore.currentTab.index} />
			<div className={classes.tabContainer}>
				<TabWrapper>
					{gameSetStore?.currentTab?.component}
				</TabWrapper>
				<Roles />
			</div>
		</div>
	);
});

export default EditSet;