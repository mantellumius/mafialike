import { ArrowRightIcon, PencilSquareIcon } from "@heroicons/react/24/outline";
import { useDebouncedValue } from "@mantine/hooks";
import * as React from "react";
import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import Button from "@UI/Button/Button";
import Card from "@UI/Card/Card";
import FormInput from "@UI/FormInput/FormInput";
import classes from "./GameSetsList.module.css";
import useSetBackgroundImage from "@hooks/useSetBackgroundImage";
import useApi from "@hooks/useApi";
import { useRootStore } from "@stores/RootStore";

const GameSetsList: React.FC = () => {
	const { gameSetStore, authStore, themeStore } = useRootStore();
	const [gameSets, setGameSets] = useState<WithIdAndAuthorId<GameSet & { login: string }>[]>([]);
	const [name, setName] = useState("");
	const [debouncedName] = useDebouncedValue(name, 300);
	const navigate = useNavigate();
	useSetBackgroundImage(themeStore);
	const { response } = useApi({
		url: "/game-sets/search",
		method: "get",
		params: {
			name: debouncedName ?? "",
		}
	});
	useEffect(() => setGameSets(response as WithIdAndAuthorId<GameSet & { login: string }>[]), [response]);

	return (
		<div className={classes.container}>
			<FormInput value={name} onChange={(e) => setName(e.target.value)} placeholder={"Поиск по названию"} className={classes.search} width={"80%"} />
			<div className={classes.cardsList}>
				{gameSets && gameSets.map(gameSet =>
					<Card key={gameSet.id} image={gameSet.image} title={gameSet.name.slice(0, 50)}
						description={gameSet.description} author={gameSet.login}>
						{(authStore.user?.id === gameSet.authorId || authStore.user?.role === "Admin") &&
							<Button onClick={() => {
								gameSetStore.changeSetId(gameSet.id);
								navigate("/game-sets/edit");
							}} text="Редактировать"><PencilSquareIcon className="w-6 h-6" /></Button>
						}
						<Button text={"Подробнее"} background={"var(--primary-700)"}
							onClick={() => navigate(`/game-sets/${gameSet.id}`)}>
							<ArrowRightIcon className="w-6 h-6" />
						</Button>
					</Card>
				)}
			</div>
		</div>
	);
};

export default GameSetsList;