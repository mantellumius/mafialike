module.exports = {
	env: { browser: true, es2020: true },
	settings: {
		"import/resolver":
		{
			node: { moduleDirectory: ["node_modules", "src/"] },
		}
	},
	extends: [
		"eslint:recommended",
		"plugin:@typescript-eslint/recommended",
		"plugin:react-hooks/recommended",
		"plugin:@typescript-eslint/recommended-requiring-type-checking",
		"plugin:import/typescript"
	],
	parser: "@typescript-eslint/parser",
	parserOptions: {
		project: true,
		tsconfigRootDir: __dirname,
		ecmaVersion: "latest",
		sourceType: "module"
	},
	ignorePatterns: ["*.js", "*.cjs", "vite.config*"],
	plugins: ["react-refresh", "@typescript-eslint", "import"],
	rules: {
		"lines-between-class-members": [
			"error",
			"always",
			{
				"exceptAfterSingleLine": true
			}
		],
		"@typescript-eslint/no-unsafe-assignment": "off",
		"react-refresh/only-export-components": "off",
		"no-mixed-spaces-and-tabs": "off",
		"@typescript-eslint/no-empty-function": "off",
		"@typescript-eslint/no-extra-semi": "off",
		"indent": [
			"error",
			"tab",
			{ "SwitchCase": 1 }
		],
		"linebreak-style": [
			"error",
			"unix"
		],
		"quotes": [
			"error",
			"double"
		],
		"semi": [
			"error",
			"always"
		]
	}
};
