module.exports = {
	"env": {
		"es2021": true,
		"node": true
	},
	"parser": "@typescript-eslint/parser",
	"extends": [
		"eslint:recommended",
		"plugin:@typescript-eslint/eslint-recommended",
		"plugin:@typescript-eslint/recommended"
	],
	"overrides": [],
	"plugins": [
		"@typescript-eslint"
	],
	"ignorePatterns": ["*.js", "*.cjs"],
	"parserOptions": {
		"ecmaVersion": "latest",
		"sourceType": "module"
	},
	"rules": {
		"lines-between-class-members": [
			"error",
			"always",
			{
				"exceptAfterSingleLine": true
			}
		],
		"@typescript-eslint/ban-ts-comment": "off",
		"no-mixed-spaces-and-tabs": "off",
		"@typescript-eslint/no-extra-semi": "off",
		"@typescript-eslint/no-empty-function": "off",
		"indent": [
			"error",
			"tab",
			{ "SwitchCase": 1 }
		],
		"linebreak-style": [
			"error",
			"unix"
		],
		"quotes": [
			"error",
			"double"
		],
		"semi": [
			"error",
			"always"
		]
	}
};
