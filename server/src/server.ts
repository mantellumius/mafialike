import { ClientToServerEvents, InterServerEvents, ServerToClientEvents } from "@shared";
import cors from "cors";
import { config } from "dotenv";
import express from "express";
import * as fs from "fs";
import http from "http";
import https from "https";
import logger from "morgan";
import { Server } from "socket.io";
import db from "./db/index.js";
import { createRedis } from "./db/redis.js";
import useGameHandler from "./handlers/useGameHadler.js";
import useRoomHandler from "./handlers/useRoomHandler.js";
import AuthMiddleware from "./middlewares/AuthMiddleware.js";
import IOMiddleware from "./middlewares/IOMiddleware.js";
import { createSessionMiddleware } from "./middlewares/createSessionMiddleware.js";
import { accounts, auth, gameSets, missions, roles, rooms } from "./routes/index.js";
import SocketData from "./types/SocketData";

config();
const privateKey = fs.existsSync(`${process.env.SSL_DIR}privkey.pem`) ? fs.readFileSync(`${process.env.SSL_DIR}privkey.pem`) : undefined;
const certificate = fs.existsSync(`${process.env.SSL_DIR}fullchain.pem`) ? fs.readFileSync(`${process.env.SSL_DIR}fullchain.pem`) : undefined;
const credentials = { key: privateKey, cert: certificate };
const port = process.env.PORT || "3000";
const corsOptions = {
	credentials: true,
	origin: [(process.env.FRONTEND_URL ?? "").toString(), "http://localhost", "http://localhost:4173"]
};
const app = express();
const server = (credentials.cert && credentials.key) ? https.createServer(credentials, app) : http.createServer(app);
export const io = new Server<ClientToServerEvents, ServerToClientEvents, InterServerEvents, SocketData>(server, {
	cors: corsOptions,
});
export const redis = await createRedis();
const sessionMiddleware = createSessionMiddleware(redis);

app.use(cors(corsOptions));
app.use(sessionMiddleware);
app.use(IOMiddleware);
app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use("/auth", auth);
app.use("/api", AuthMiddleware);
app.use("/api", accounts);
app.use("/api", rooms);
app.use("/api", gameSets);
app.use("/api", roles);
app.use("/api", missions);

io.engine.use(sessionMiddleware);
io.engine.use(AuthMiddleware);
io.on("connection", (socket) => {
	const { user } = socket.request.session;
	if (!user) return;
	io.to(user.id.toString()).disconnectSockets();
	socket.join(user.id.toString());
	console.log(`${user.nickname} connected`);
	useRoomHandler(io, socket);
	useGameHandler(io, socket);
});

server.listen(port, () => console.log("Server started on port " + port));

const startGracefulShutdown = async () => {
	console.log("Starting shutdown of express...");
	server.close(() => {
		console.log("Express shut down.");
	});
	await db.destroy();
	await redis.quit();
};
process.on("SIGTERM", startGracefulShutdown);
process.on("SIGINT", startGracefulShutdown);
