import { config as dotenvConfig } from "dotenv";
import type { Knex } from "knex";
import path from "path";

const pathToEnv = path.resolve(process.cwd()).includes("dist") ? "../.env" : ".env";
dotenvConfig({ path: pathToEnv });
const config: { [key: string]: Knex.Config } = {
	development: {
		client: "postgresql",
		connection: process.env.DB_URL_DEV,
		pool: {
			min: 1,
			max: 3
		},
		migrations: {
			directory: path.join("./db/migrations"),
			tableName: "knex_migrations"
		},
		useNullAsDefault: true
	},

	staging: {
		client: "postgresql",
		connection: {
			database: "my_db",
			user: "username",
			password: "password"
		},
		pool: {
			min: 2,
			max: 5
		},
		migrations: {
			tableName: "knex_migrations"
		}
	},

	production: {
		client: "postgresql",
		connection: process.env.DB_URL_PROD,
		pool: {
			min: 1,
			max: 5
		},
		migrations: {
			directory: path.join("./db/migrations"),
			tableName: "knex_migrations"
		},
		useNullAsDefault: true
	}

};

export default config;
