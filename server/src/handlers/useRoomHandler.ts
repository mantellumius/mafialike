import { ClientToServerEvents, InterServerEvents, ServerToClientEvents } from "@shared";
import { Server, Socket } from "socket.io";
import GameSetsCacheService from "../services/GameSetsCacheService.js";
import GamesService from "../services/GamesService.js";
import RoomsService from "../services/RoomsService.js";
import SocketData from "../types/SocketData";

export default function useRoomHandler(io: Server<ClientToServerEvents, ServerToClientEvents, InterServerEvents, SocketData>, socket: Socket<ClientToServerEvents, ServerToClientEvents, never, SocketData>) {
	const { user } = socket.request.session;
	if (!user) return;
	socket.on("disconnecting", async () => {
		const { roomId } = socket.request.session;
		const room = await RoomsService.getAsync(roomId ?? "");
		if (!room) return;
		room.leave(user.id);
	});
	socket.on("ShufflePlayers", async () => {
		const { roomId } = socket.request.session;
		if (!roomId) return;
		const room = await RoomsService.getAsync(roomId);
		if (!room || !room.isOwner(user) || GamesService.has(roomId)) return;
		room.shuffle();
		room.emit("RoomStateSent", room.players);
	});
	socket.on("SitDown", async () => {
		const { roomId } = socket.request.session;
		const room = await RoomsService.getAsync(roomId ?? "");
		if (!room || room.isLocked) return;
		if (room.sit(user.id))
			room.emit("RoomStateSent", room.players);
	});
	socket.on("GetUp", async () => {
		const { roomId } = socket.request.session;
		const room = await RoomsService.getAsync(roomId ?? "");
		if (!room || room.isLocked) return;
		if (room.stand(user.id))
			room.emit("RoomStateSent", room.players);
	});
	socket.on("Kick", async (userId) => {
		const { roomId } = socket.request.session;
		if (!roomId) return;
		const room = await RoomsService.getAsync(roomId);
		if (!room || !room.isOwner(user) || GamesService.has(roomId)) return;
		room.kick(userId);
		room.emit("GotUp", userId);
	});
	socket.on("Join", async (roomId, callback) => {
		const room = await RoomsService.getAsync(roomId);
		if (!room) return socket.emit("JoinFailed");
		socket.request.session.roomId = roomId;
		socket.request.session.save();
		socket.request.session.reload(err => err && console.error("reload error:    ", err));
		room.join(user);
		return callback(room.owner);
	});
	socket.on("ChangeLockState", async (state) => {
		const { roomId } = socket.request.session;
		const room = await RoomsService.getAsync(roomId ?? "");
		if (!room || !room.isOwner(user) || GamesService.has(roomId ?? "")) return;
		room.isLocked = state;
		room.emit("LockStateChanged", state);
	});
	socket.on("ChangeGameSet", async (gameSetId) => {
		const room = await RoomsService.getAsync(socket.request.session.roomId ?? "");
		if (!room || !room.isOwner(user) || GamesService.has(room.id)) return;
		const gameSet = await GameSetsCacheService.getAsync(gameSetId);
		if (!gameSet) return;
		room.gameSetId = gameSetId;
		room.emit("GameSetChanged", gameSet);
	});
}