import { ClientToServerEvents, InterServerEvents, ServerToClientEvents } from "@shared";
import { Server, Socket } from "socket.io";
import Game from "../classes/Game.js";
import RoleBuilderFactory from "../classes/RoleBuilderFactory.js";
import MissionsSettings from "../classes/Settings/MissionsSettings.js";
import RolesSettings from "../classes/Settings/RolesSettings.js";
import createDeepProxy from "../helpers/createDeepProxy.js";
import DtoService from "../services/DtoService.js";
import GameSetsCacheService from "../services/GameSetsCacheService.js";
import GameSetsService from "../services/GameSetsService.js";
import GamesService from "../services/GamesService.js";
import PlayerStageService from "../services/PlayerStageService.js";
import RoomsService from "../services/RoomsService.js";
import VotesHistoryService from "../services/VotesHistoryService.js";
import Player from "../types/Player";
import Settings from "../types/Settings";
import SocketData from "../types/SocketData";

export default function useGameHandler(
	io: Server<ClientToServerEvents, ServerToClientEvents, InterServerEvents, SocketData>,
	socket: Socket<ClientToServerEvents, ServerToClientEvents, never, SocketData>
) {
	async function updateStage(userId: number | undefined, roomId: string, stage: PlayerStage) {
		if (!userId) return;
		PlayerStageService.updateStage(userId, roomId, stage);
		io.to(userId.toString()).emit("StageUpdated", stage);
	}

	async function updateStageForPlayers(roomId: string, stage: PlayerStage) {
		const room = await RoomsService.getAsync(roomId);
		if (!room) return;
		room.players.forEach((user) => PlayerStageService.updateStage(user?.id, roomId, stage));
		room.emitToPlayers("StageUpdated", stage);
	}

	async function updateStageForObservers(roomId: string, stage: PlayerStage) {
		const room = await RoomsService.getAsync(roomId);
		if (!room) return;
		room.observers.forEach((user) => PlayerStageService.updateStage(user?.id, roomId, stage));
		room.emitToObservers("StageUpdated", stage);
	}

	async function onSuggestionsFinished(roomId: string) {
		const groupsDtos = ((await GamesService.getAsync(roomId))?.getLastGroups?.distinctSuggestedGroups ?? []).map(DtoService.toDtoGroup);
		(await RoomsService.getAsync(roomId))?.emitToPlayers("VotingStarted", groupsDtos);
		await updateStageForPlayers(roomId, "Voting");
	}

	async function onVotingFinished(roomId: string) {
		const game = await GamesService.getAsync(roomId);
		const room = await RoomsService.getAsync(roomId);
		if (!game || !room) return;
		if (game.reVotingRequired) {
			game.startReVoting();
			room.emitToPlayers("VotingStarted", game.getLastGroups?.distinctSuggestedGroups.map(DtoService.toDtoGroup) ?? []);
			return updateStageForPlayers(roomId, "Voting");
		}
		if (!game.getLastGroups) return;
		const winner = game.getLastGroups.getWinner;
		await VotesHistoryService.setWinner(roomId, game.currentMissionIndex, winner.users.map(u => game.getNumberFromCrown(u.id)).sort());
		await updateVotesHistoryRoom(roomId, game.currentMissionIndex);
		await updateStageForPlayers(roomId, "Mission");
		game.currentMission.setMembers(winner.users);
		room.emit("MissionStarted", game.missions.map(DtoService.toDtoMission));
		updateAbilityCanUse(game);
		if (game.unusedAbilities.length <= 0)
			for (const member of winner.users)
				await updateStage(member.id, roomId, "MissionActive");
	}

	async function onMissionFinished(roomId: string) {
		const game = await GamesService.getAsync(roomId);
		const room = await RoomsService.getAsync(roomId);
		if (!game || !room) return;
		const missions = game.missions.map(DtoService.toDtoMission);
		room.emit("MissionFinished", missions);
		if (game.isBlueWin || game.isRedWin) {
			room.emit("GameOver", game.players.map(DtoService.toDtoPlayer));
			await updateStageForPlayers(roomId, "Finished");
		} else {
			await updateStageForPlayers(roomId, "PreSuggestions");
			updateAbilityCanUse(game);
		}

	}

	async function abilityUsageDecided(game: Game, roomId: string, user: UserDto) {
		game.unusedAbilities = game.unusedAbilities.filter(id => id !== user.id);
		if (game.unusedAbilities.length <= 0 && game.gameStage === "Mission") {
			if (!game.getLastGroups) return;
			const winner = game.getLastGroups.getWinner;
			for (const member of winner.users)
				await updateStage(member.id, roomId, "MissionActive");
		}
	}

	async function updateVotesHistory(roomId: string, missionIndex: number, userId: number) {
		const history = await VotesHistoryService.getVotesHistoryForMission(roomId, missionIndex);
		if (!history) return;
		io.to(userId.toString()).emit("VotesHistoryUpdated", missionIndex, history);
	}

	async function updateVotesHistoryRoom(roomId: string, missionIndex: number) {
		const room = await RoomsService.getAsync(roomId);
		if (!room) return;
		const history = await VotesHistoryService.getVotesHistoryForMission(roomId, missionIndex);
		if (!history) return;
		room.emit("VotesHistoryUpdated", missionIndex, history);
	}

	function updatePlayers(game: Game, player: Player, socket?: Socket<ClientToServerEvents, ServerToClientEvents, never, SocketData>) {
		const visiblePlayers = game.getVisionFor(player);
		if (socket) socket.emit("PlayersUpdated", visiblePlayers.map(DtoService.toDtoPlayer));
		else io.to(player.user.id.toString()).emit("PlayersUpdated", visiblePlayers.map(DtoService.toDtoPlayer));
	}

	function updateAbilityCanUse(game: Game) {
		game.unusedAbilities = game.players.filter(p => p.ability?.canUse(game, p)).map(p => p.user.id);
	}

	const { user } = socket.request.session;
	if (!user) return;
	socket.on("Join", async (roomId) => {
		const room = await RoomsService.getAsync(roomId);
		if (!room) return;
		socket.emit("LockStateChanged", room.isLocked);
		const gameSet = await GameSetsCacheService.getAsync(room.gameSetId);
		if (gameSet) socket.emit("GameSetChanged", gameSet);
		if (await GamesService.getAsync(roomId)) {
			const game = await GamesService.getAsync(roomId);
			if (!game) return;
			const stage = (await PlayerStageService.getStage(user.id, roomId)) ?? "Wait";
			const player = game.players.find(player => player.user.id === user.id);
			const players = game.players.map(player => DtoService.toDtoPlayer({
				user: player.user,
				settings: { canDoMissions: player.settings.canDoMissions }
			}));
			const missions = game.missions.map(DtoService.toDtoMission);
			socket.emit("GameStateSent", players, missions, game.currentMissionIndex, game.crownedUserId);
			if (player) updatePlayers(game, player, socket);
			socket.emit("StageUpdated", stage);
			game.missions.forEach((_, index) => updateVotesHistory(roomId, index, user.id));
			if (game.isBlueWin || game.isRedWin) return room.emit("GameOver", game.players.map(DtoService.toDtoPlayer));
			if (game.getLastGroups)
				switch (stage) {
					case "Voting":
						return socket.emit("VotingStarted", game.getLastGroups.distinctSuggestedGroups.map(DtoService.toDtoGroup));
					case "Mission":
					case "MissionActive":
						return socket.emit("MissionStarted", game.missions.map(DtoService.toDtoMission));
				}
		} else {
			socket.emit("RoomStateSent", room.players);
			await updateStage(user.id, roomId, "PreGame");
		}
	});
	socket.on("StartGame", async () => {
		const { roomId } = socket.request.session;
		const room = await RoomsService.getAsync(roomId ?? "");
		if (!roomId || !room || !room.isOwner(user) || await GamesService.getAsync(roomId ?? "")) return;
		if (room.players.length < 5)
			return socket.emit("StartFailed", "Недостаточно игроков");
		if (!room.gameSetId) return;
		const fullRolesSettings = await GameSetsService.getSetFullRoles(room.gameSetId);
		const fullRoleAndBuilder = fullRolesSettings.map(({ role, settings, abilitySettings }) => {
			const builder = RoleBuilderFactory.create(role.id, role.name, role.image, role.description);
			return { builder, settings, abilitySettings };
		});
		const missionsSettings = await GameSetsService.getMissionsSettings(room.gameSetId);
		const settings: Settings = {
			missionsSettings: new MissionsSettings(missionsSettings),
			rolesSettings: new RolesSettings(fullRoleAndBuilder)
		};
		const game = GamesService.create(roomId, settings, room.players);
		if (!game)
			return socket.emit("StartFailed", "Некорректный набор.\n Выберите другой или исправьте текущий.");
		GamesService.createGame(roomId, game);
		const missions = game.missions.map(DtoService.toDtoMission);
		const players = game.players.map(player => DtoService.toDtoPlayer({
			user: player.user,
			settings: { canDoMissions: player.settings.canDoMissions }
		}));
		room.isLocked = true;
		room.emit("LockStateChanged", true);
		room.emit("GameStarted", players, missions, game.crownedUserId);
		await updateStageForObservers(roomId, "Wait");
		await updateStageForPlayers(roomId, "PreSuggestions");
		game.players.forEach(player => updatePlayers(game, player));
	});
	socket.on("StartSuggestions", async () => {
		const { roomId } = socket.request.session;
		const room = await RoomsService.getAsync(roomId ?? "");
		if (!roomId || !room || !room.isOwner(user)) return;
		const game = await GamesService.getAsync(roomId);
		if (!game || (!game.currentMission.isFinished && game.currentMissionIndex !== -1)) return;
		room.emitToPlayers("SuggestionsStarted");
		game.nextMission();
		room.emitToPlayers("NextMission", game.currentMissionIndex);
		await updateStageForPlayers(roomId, "Suggestions");
		await VotesHistoryService.createVotesHistory(roomId,
			game.currentMissionIndex,
			game.players.map(p => ({
				nickname: p.user.nickname,
				numberFromCrown: game.getNumberFromCrown(p.user.id)
			})));
	});
	socket.on("SuggestGroup", async (group) => {
		const { roomId } = socket.request.session;
		if (!roomId) return;
		const game = await GamesService.getAsync(roomId);
		const room = await RoomsService.getAsync(roomId);
		if (!game || !room || !game.players.find(p => p.user.id === user.id)) return;
		const lastGroups = game.getLastGroups;
		if (!lastGroups) return;
		while (group.length < lastGroups.groupSize) {
			const newPlayer = game.players.find(p => p.settings.canDoMissions && !group.map(m => m.id).includes(p.user.id))?.user;
			if (!newPlayer) return;
			group.push(DtoService.toDtoUser(newPlayer));
		}
		lastGroups.suggestGroup(group, user);
		await updateStage(user.id, roomId, "Wait");
		if (lastGroups.isSuggestionsFinished) {
			await onSuggestionsFinished(roomId);
			const groups = lastGroups.suggestedGroups
				.map(group => ({
					group: group.users.map(u => game.getNumberFromCrown(u.id)).sort(),
					number: game.getNumberFromCrown(group.authors[0].id)
				}))
				.sort((a, b) => a.number - b.number)
				.map(g => g.group);
			await VotesHistoryService.saveGroups(roomId, game.currentMissionIndex, groups);
			await updateVotesHistoryRoom(roomId, game.currentMissionIndex);
		}
	});
	socket.on("Vote", async (groupId) => {
		const { roomId } = socket.request.session;
		if (!roomId) return;
		const game = await GamesService.getAsync(roomId);
		const room = await RoomsService.getAsync(roomId);
		if (!game || !room || !game.players.find(p => p.user.id === user.id)) return;
		const lastGroups = game.getLastGroups;
		if (!lastGroups) return;
		lastGroups.vote(groupId, user.id);
		await updateStage(user.id, roomId, "Wait");
		if (game.getLastGroups?.isVotingFinished) {
			const groups = game.players
				.map(p => ({
					number: p.numberFromCrown,
					group: lastGroups.suggestedGroups
						.find(g => g.votedUsersId.includes(p.user.id))
						?.users.map(u => game.getNumberFromCrown(u.id)).sort() ?? []
				}))
				.sort((a, b) => a.number - b.number)
				.map(g => g.group);
			await VotesHistoryService.saveVoting(roomId, game.currentMissionIndex, groups);
			await updateVotesHistoryRoom(roomId, game.currentMissionIndex);
			await onVotingFinished(roomId);
		}
	});
	socket.on("PerformMissionAction", async (action) => {
		const { roomId } = socket.request.session;
		if (!roomId) return;
		const game = await GamesService.getAsync(roomId);
		if (!game || !game.players.find(p => p.user.id === user.id)) return;
		if ((action === "Success" && game.succeedCurrentMission(user.id)) || (action === "Fail" && game.failCurrentMission(user.id))) {
			await updateStage(user.id, roomId, "Mission");
			if (game.currentMission.isFinished)
				await onMissionFinished(roomId);
		}
	});
	socket.on("UseSpecialAbility", async (targetId) => {
		const { roomId } = socket.request.session;
		const room = await RoomsService.getAsync(roomId ?? "");
		if (!roomId || !room) return;
		const game = await GamesService.getAsync(roomId);
		if (!game) return;
		const player = game.players.find(p => p.user.id === user.id);
		if (!player) return;
		if (!player.ability.canUse(game, player)) return;
		player.ability.use(game, targetId);
		if ((player.ability.settings.type === "Jail" || player.ability.settings.type === "JailAndReveal") && player.ability.hit === true) {
			game.players.forEach(player => updatePlayers(game, player));
			room.emit("AbilityResultReceived", player.ability?.logMessage);
		} else {
			socket.emit("UpdateAbilityInfo", DtoService.toDtoAbility(player.ability));
			socket.emit("AbilityResultReceived", player.ability?.logMessage);
		}
		await abilityUsageDecided(game, roomId, user);
	});
	socket.on("CancelUseSpecialAbility", async () => {
		const { roomId } = socket.request.session;
		const room = await RoomsService.getAsync(roomId ?? "");
		if (!roomId || !room) return;
		const game = await GamesService.getAsync(roomId);
		if (!game) return;
		await abilityUsageDecided(game, roomId, user);
	});
}
