import RedisStore from "connect-redis";
import session from "express-session";
import { RedisClientType } from "redis";

export const createSessionMiddleware = (redis: RedisClientType) => {
	const store = new RedisStore({
		client: redis,
		prefix: "session:"
	});

	return session({
		// eslint-disable-next-line @typescript-eslint/no-non-null-assertion
		secret: process.env.SESSION_SECRET!,
		resave: false,
		saveUninitialized: false,
		cookie: {
			signed: true,
			sameSite: "strict",
			secure: false,
			maxAge: 1000 * 60 * 60 * 24 * 7
		},
		store
	});
};