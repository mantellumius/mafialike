import { NextFunction, Request, Response } from "express";
import { redis } from "../server.js";

export default function buildRateLimiterMiddleware(limit?: number, expireInSeconds?: number, prefix?: string): (req: Request, res: Response, next: NextFunction) => Promise<void | Response> {
	return async (req: Request, res: Response, next: NextFunction) => {
		const ip = req.headers["x-forwarded-for"] as string || req.socket.remoteAddress;
		if (ip) {
			const amount = parseInt(await redis.get(`rate-limit:${ip}:${prefix ?? ""}`) ?? "0");
			if (amount >= (limit ?? 10))
				return res.status(429).json({ message:"Too many requests", timeout: expireInSeconds ?? 60});
			await redis.multi().incr(`rate-limit:${ip}:${prefix ?? ""}`).expire(`rate-limit:${ip}:${prefix ?? ""}`, expireInSeconds ?? 60).exec();
			next();
		}
	};
}