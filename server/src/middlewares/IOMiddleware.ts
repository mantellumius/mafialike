import { NextFunction, Request, Response } from "express";
import { io } from "../server.js";

export default function (req: Request, res: Response, next: NextFunction) {
	req.io = io;
	next();
}