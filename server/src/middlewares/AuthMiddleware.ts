import { NextFunction, Request, Response } from "express";

export default function authMiddleware(req: Request, res: Response, next: NextFunction) {
	if (req.session.authenticated)
		next();
	else if (typeof res.sendStatus === "function")
		res.sendStatus(401);
	else next(new Error("Unauthorized"));
}