import ISpecialAbility from "./ISpecialAbility";

type Player = {
	user: User;
	role: WithId<Role>;
	settings: RoleSettings;
	ability: ISpecialAbility;
	numberFromCrown: number;
}

export default Player;
