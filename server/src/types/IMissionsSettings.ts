import IMission from "./IMission";

interface IMissionsSettings {
	generateMissions(): IMission[];
	get amount(): number;
}

export default IMissionsSettings;