import FullRole from "./FullRole";
import FullRoleInfo from "./FullRoleInfo";

interface IRolesSettings {
	reds: FullRoleInfo[];
	blues: FullRoleInfo[];
	generateRandomPool: (playersCount: number) => FullRole[];
}

export default IRolesSettings;