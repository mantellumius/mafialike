import IMissionsSettings from "./IMissionsSettings";
import IRolesSettings from "./IRolesSettings";

interface Settings {
	missionsSettings: IMissionsSettings;
	rolesSettings: IRolesSettings;
}

export default Settings;
