type Member = {
	id: number;
	isFail: boolean;
	isSuccess: boolean;
	forced: boolean;
	acted: boolean;
}
export default Member;
