interface IRoom {
	join(user: UserDto): void;

	leave(userId: number): void;

	sit(userId: number): void;

	stand(userId: number): void;
}

export default IRoom;