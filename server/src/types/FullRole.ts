import ISpecialAbility from "./ISpecialAbility";

type FullRole = {
	role: Role,
	settings: RoleSettings
	ability: ISpecialAbility;
}
export default FullRole;
