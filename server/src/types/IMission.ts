import Member from "./Member";

interface IMission {
	readonly reqFails: number;
	readonly reqMembers: number;
	members: Member[] | null;

	setMembers(members: UserDto[]): void;

	get fails(): number;

	get successes(): number;

	get isFailed(): boolean;

	get isSucceeded(): boolean;

	get isFinished(): boolean;

	getMember(id: number): Member | undefined;

	fail(id: number): void;

	succeed(id: number): void;

	forceMemberSuccess(id: number): boolean;
}

export default IMission;