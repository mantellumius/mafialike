type FullRoleInfo = {
	builder: () => WithId<Role>;
	settings: RoleSettings;
	abilitySettings: SpecialAbilitySettings;
}
export default FullRoleInfo;