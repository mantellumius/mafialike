export type Account =  {
	id: number;
	login: string;
	email: string;
	password: string;
	created_at: string;
	role: UserRole;
}