import { ClientToServerEvents, InterServerEvents, ServerToClientEvents } from "@shared";
import { Server } from "socket.io";
import SocketData from "./SocketData";

declare global {
	namespace Express {
		interface Request {
			io: Server<ClientToServerEvents, ServerToClientEvents, InterServerEvents, SocketData>;
		}
	}

	namespace SOCKET.IO {

	}
}