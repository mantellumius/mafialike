import session, { Session } from "express-session";

declare module "http" {

	interface IncomingMessage {
		session: Session & Partial<session.SessionData>;
		sessionID: string;
	}
}