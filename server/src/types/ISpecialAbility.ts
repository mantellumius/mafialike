import Game from "../classes/Game";
import Player from "./Player";

interface ISpecialAbility {
	settings: SpecialAbilitySettings;
	logMessage: Message;
	hit: boolean | null;

	canUse(game: Game, player: Player): boolean;

	use(game: Game, targetId: number): void;
}

export default ISpecialAbility;