import { Knex } from "knex";
import { Account } from "./Account";

declare module "knex/types/tables" {
	interface Tables {
		accounts: Knex.CompositeTableType<
		  Account,
		  Pick<Account, "login" | "email" | "password">,
		  Partial<Omit<Account, "id" & "email" & "name">>
		>;
		gameSets: Knex.CompositeTableType<
		  WithIdAndAuthorId<GameSet>,
		  Pick<WithIdAndAuthorId<GameSet>, "name" | "description" | "backgroundImage" | "authorId" | "image">,
		  Partial<Omit<WithIdAndAuthorId<GameSet>, "id" | "authorId">>
		>;
		roles: Knex.CompositeTableType<
		  WithIdAndAuthorId<Role>,
		  Pick<WithIdAndAuthorId<Role>, "name" | "description" | "image" | "authorId">,
		  Partial<Omit<WithIdAndAuthorId<Role>, "authorId" | "id">>
		>;
		roleSettings: Knex.CompositeTableType<
		  RoleSettings,
		  RoleSettings,
		  Partial<Omit<RoleSettings, "roleId">>
		>;
		missionSettings: Knex.CompositeTableType<
		  WithId<MissionSettings>,
		  Pick<WithId<MissionSettings>, "requiredFails" | "requiredMembers">,
		  Partial<Omit<WithId<MissionSettings>, "id">>
		>;
		abilitySettings: Knex.CompositeTableType<
		  SpecialAbilitySettings,
		  SpecialAbilitySettings,
		  Partial<Omit<SpecialAbilitySettings, "roleId">>
		>;
		setRoles: Knex.CompositeTableType<
		  { setId: number, roleId: number },
		  { setId: number, roleId: number },
		  { setId: number, roleId: number }
		>;
		setMissionsSettings: Knex.CompositeTableType<
		  { setId: number, missionSettingsId: number },
		  { setId: number, missionSettingsId: number },
		  { setId: number, missionSettingsId: number }
		>;
		gameSetLikes: Knex.CompositeTableType<
		  { gameSetId: number, userId: number },
		  { gameSetId: number, userId: number },
		  { gameSetId: number, userId: number }>;
	}
}