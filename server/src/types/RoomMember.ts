type RoomMember = {
	user: UserDto;
	roomRole: RoomRole;
}
export default RoomMember;