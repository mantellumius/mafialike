import Room from "../classes/Room.js";
import createDeepProxy from "../helpers/createDeepProxy.js";
import { redis } from "../server.js";
import IdService from "./IdService.js";

class RoomsService {
	rooms: Map<string, Room>;

	constructor() {
		this.rooms = new Map();
	}

	public async create(owner: UserDto): Promise<string> {
		const id = IdService.generateId();
		const newRoom = new Room(id, owner);
		await this.saveToDb(id, new Room(id, owner));
		const proxy = createDeepProxy(newRoom, () => this.saveToDb(id, newRoom));
		this.rooms.set(id, proxy);
		return id;
	}

	private async saveToDb(id: string, room: Room) {
		await redis.multi()
			.set(`rooms:${id}`, JSON.stringify(room))
			.expire(`rooms:${id}`, 60 * 60 * 24).exec();
	}

	public async getAsync(id: string): Promise<Room | undefined> {
		if (!id) return;
		if (!this.rooms.has(id)) {
			const fromDb = await this.getFromDb(id);
			if (!fromDb) return;
			const room = Room.createFromJSON(fromDb);
			const proxy = createDeepProxy(room, () => this.saveToDb(id, room));
			this.rooms.set(id, proxy);
		}
		return this.get(id);
	}

	public async getFromDb(id: string) {
		const fromDb = await redis.get(`rooms:${id}`);
		if (!fromDb) return;
		return JSON.parse(fromDb);
	}

	private get(id: string): Room | undefined {
		return this.rooms.get(id);
	}

	private async has(id: string): Promise<boolean> {
		return await redis.exists(`rooms:${id}`) > 0;
	}
}

export default new RoomsService();