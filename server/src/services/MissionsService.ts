import db from "../db/index.js";

class MissionsService {
	async upsertMission(id: number, mission: MissionSettings) {
		let result;
		if (Number.isNaN(id))
			result = await db.table("missionSettings").insert(mission, ["id"]);
		else
			result = await db.table("missionSettings").update(mission, ["id"]);
		if (result.length > 0)
			return result[0].id;
		return -1;
	}
}

export default new MissionsService();