import Game from "../classes/Game.js";
import createDeepProxy from "../helpers/createDeepProxy.js";
import { redis } from "../server.js";
import Settings from "../types/Settings.js";

class GamesService {
	private games: Map<string, Game>;
	private readonly GameStorageDuration = 60 * 60 * 24;
	constructor() {
		this.games = new Map();
	}

	public create(roomId: string, settings: Settings, players: UserDto[]): Game | null {
		const game = Game.create(settings, players);
		if (!game)
			return null;
		return createDeepProxy(game, () => this.saveToDb(roomId, game));
	}

	public async getAsync(id: string): Promise<Game | undefined> {
		if (!id) return;
		if (!this.games.has(id)) {
			const fromDb = await this.getFromDb(id);
			if (!fromDb) return;
			const game = Game.createFromJSON(fromDb.gameJSON);
			const proxy = createDeepProxy(game, () => this.saveToDb(id, game));
			this.games.set(id, proxy);
		}
		return this.games.get(id);
	}

	public has(roomId: string): boolean {
		return this.games.has(roomId ?? "");
	}

	public createGame(roomId: string, game: Game) {
		if (!roomId) return;
		this.games.set(roomId, game);
		void this.saveToDb(roomId, game);
	}

	public async saveToDb(roomId: string, game: Game) {
		await redis.multi()
			.set(`gameWithRoomId:${roomId}`, JSON.stringify({
				roomId,
				gameJSON: game
			}))
			.expire(`gameWithRoomId:${roomId}`, this.GameStorageDuration)
			.exec();
	}

	private async getFromDb(id: string): Promise<{ roomId: string, gameJSON: Game; } | undefined> {
		const fromDb = await redis.get(`gameWithRoomId:${id}`);
		if (!fromDb) return;
		return JSON.parse(fromDb);
	}
}

export default new GamesService();