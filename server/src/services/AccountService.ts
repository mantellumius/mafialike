import bcrypt from "bcrypt";
import db from "../db/index.js";

class AccountService {
	async login(loginOrEmail: string, password: string) {
		if (!loginOrEmail || !password) return false;
		const account = await db("accounts").where({ login: loginOrEmail }).orWhere({ email: loginOrEmail }).first();
		if (!account) return false;
		const isPassEquals = await bcrypt.compare(password, account.password);
		if (!isPassEquals) return false;
		return account;
	}

	async register(email: string, login: string, password: string): Promise<boolean> {
		try {
			const account = await db("accounts").where({ login }).orWhere({ email }).first();
			if (account) return false;
			const hashedPassword = await bcrypt.hash(password, 10);
			await db("accounts").insert({
				email,
				login,
				password: hashedPassword
			});
			return true;
		} catch (e) {
			return false;
		}
	}

	async getById(id: number) {
		return db("accounts").where({ id }).first();
	}
}

export default new AccountService();