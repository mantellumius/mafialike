import { redis } from "../server.js";

class PlayerStageService {
	updateStage(userId: number, roomId: string, stage: PlayerStage) {
		redis.multi()
			.set(`player-stage:${roomId}:${userId}`, stage)
			.expire(`player-stage:${roomId}:${userId}`, 60 * 60 * 6)
			.exec();
	}

	getStage(userId: number, roomId: string): Promise<PlayerStage | null> {
		if (!userId || !roomId) return Promise.resolve(null);
		return redis.get(`player-stage:${roomId}:${userId}`) as Promise<PlayerStage | null>;
	}
}

export default new PlayerStageService();