import db from "../db/index.js";
import RolesService from "./RolesService.js";

class GameSetsService {
	async getAll() {
		return db.table("gameSets")
			.join("accounts", "gameSets.authorId", "accounts.id")
			.select("gameSets.*", "accounts.login");
	}

	async getById(id: number): Promise<WithIdAndAuthorId<GameSet> | undefined> {
		return db.table("gameSets").where("gameSets.id", id).first();
	}

	async getByIdWithAuthor(id: number): Promise<WithIdAndAuthorId<GameSet> | undefined> {
		return db.table("gameSets")
			.where("gameSets.id", id)
			.join("accounts", "gameSets.authorId", "accounts.id")
			.select("gameSets.*", "accounts.login").first();
	}

	async searchByName(query: string): Promise<WithIdAndAuthorId<GameSet>[]> {
		if (!query) return [];
		return db.table("gameSets")
			.where("gameSets.name", "like", `%${query}%`)
			.join("accounts", "gameSets.authorId", "accounts.id")
			.select("gameSets.*", "accounts.login");
	}

	async getSetRoles(id: number): Promise<WithId<Role>[]> {
		return db
			.table("setRoles")
			.select("id", "name", "description", "image")
			.where({ setId: id })
			.join("roles", "setRoles.roleId", "roles.id");
	}

	async getSetFullRoles(id: number): Promise<{ role: WithId<Role>, settings: RoleSettings, abilitySettings: SpecialAbilitySettings; }[]> {
		const result: { role: WithId<Role>, settings: RoleSettings, abilitySettings: SpecialAbilitySettings; }[] = [];
		const setRoles = await this.getSetRoles(id);
		const rolesSettings = await RolesService.getRolesSettings(setRoles.map(role => role.id));
		const abilitiesSettings = await RolesService.getRolesAbilitySettings(setRoles.map(role => role.id));

		for (const role of setRoles) {
			const settings = rolesSettings.find(settings => settings.roleId === role.id);
			const abilitySettings = abilitiesSettings.find(settings => settings.roleId === role.id);
			if (!settings || !abilitySettings) continue;
			result.push({ role, settings, abilitySettings });
		}
		return result;
	}

	async create(gameSet: WithAuthorId<GameSet>) {
		return db.table("gameSets").insert(gameSet, "id").onConflict().ignore();
	}

	async update(gameSet: WithAuthorId<GameSet>, id: number): Promise<WithIdAndAuthorId<GameSet>[]> {
		return db.table("gameSets").where({ id, authorId: gameSet.authorId }).update(gameSet, "*");
	}

	async delete(id: number) {
		return db.table("gameSets").where({ id }).delete();
	}

	async addRole(setId: number, roleId: number) {
		return db.table("setRoles").insert({ roleId, setId });
	}

	async getRolesAmount(setId: number) {
		return parseInt((await db.table("setRoles").where({ setId }).count())[0].count as string);
	}

	async updateMissionsSettings(setId: number, missionsSettings: MissionSettings[]) {
		await db.transaction(async trx => {
			await trx.raw(`
DELETE FROM "missionSettings"
WHERE id IN (
	SELECT "missionSettingsId"
	FROM "setMissionsSettings"
	WHERE "setId" = ?
)
			`, setId);
			if (missionsSettings.length === 0) return;
			const newIds = (await trx("missionSettings").insert(missionsSettings, "id")).map(m => m.id);
			await trx("setMissionsSettings").insert(newIds.map(id => ({ setId, missionSettingsId: id })));
		});
	}

	async getMissionsSettings(setId: number): Promise<MissionSettings[]> {
		return db
			.table("setMissionsSettings")
			.where({ setId })
			.join("missionSettings", "setMissionsSettings.missionSettingsId", "missionSettings.id").select(["number", "requiredMembers", "requiredFails"]);
	}

	async createLike(setId: number, userId: number) {
		return db.table("gameSetLikes").insert({ gameSetId: setId, userId });
	}

	async deleteLike(setId: number, userId: number) {
		return db.table("gameSetLikes").where({ gameSetId: setId, userId }).delete();
	}

	async getLikes(setId: number) {
		return db.table("gameSetLikes").where({ gameSetId: setId }).count();
	}
}

export default new GameSetsService();