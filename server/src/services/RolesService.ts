import db from "../db/index.js";

class RolesService {
	async getById(id: number): Promise<Role | undefined> {
		const result = await db.table("roles").where({ id }).first();
		return result;
	}

	async createRole(role: WithAuthorId<Role>): Promise<WithIdAndAuthorId<Role> | null> {
		const result = await db.table("roles").insert(role, ["*"]);
		return result[0] ?? null;
	}

	async updateRole(role: Role, id: number, authorId: number): Promise<Role | null> {
		const result = await db.table("roles").where({ id, authorId }).update(role, ["*"]);
		return result[0] ?? null;
	}

	async deleteRole(id: number, authorId: number): Promise<boolean> {
		const result = await db.table("roles").where({ id, authorId }).delete();
		return result > 0;
	}

	async upsertRoleSettings(roleSettings: RoleSettings): Promise<RoleSettings[]> {
		return db.table("roleSettings").insert(roleSettings, "*").onConflict("roleId").merge();
	}

	async upsertRoleAbilitySettings(abilitySettings: SpecialAbilitySettings): Promise<SpecialAbilitySettings[]> {
		return db.table("abilitySettings").insert(abilitySettings, "*").onConflict("roleId").merge();
	}

	async getRoleSettings(id: number): Promise<RoleSettings | undefined> {
		return db.table("roleSettings").select("*").where({ roleId: id }).first();
	}

	async getRolesSettings(ids: number[]): Promise<(RoleSettings)[]> {
		return db.table("roleSettings").select("*").whereIn("roleId", ids);
	}

	async getRoleAbilitySettings(id: number): Promise<SpecialAbilitySettings | undefined> {
		return db.table("abilitySettings").select("*").where({ roleId: id }).first();
	}

	async getRolesAbilitySettings(ids: number[]): Promise<(SpecialAbilitySettings)[]> {
		return db.table("abilitySettings").select("*").whereIn("roleId", ids);
	}
}

export default new RolesService();