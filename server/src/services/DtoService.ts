import Group from "../classes/Group.js";
import IMission from "../types/IMission";
import ISpecialAbility from "../types/ISpecialAbility";
import Player from "../types/Player";

class DtoService implements DtoService {
	toDtoUser = (user: User) => ({ id: user.id, nickname: user.nickname } as UserDto);
	toDtoRole = (role: Role) => ({ name: role.name, image: role.image, description: role.description } as RoleDto);
	toDtoMission = (mission: IMission) => ({
		reqFails: mission.reqFails,
		reqMembers: mission.reqMembers,
		isFailed: mission.isFailed,
		isSucceeded: mission.isSucceeded,
		successes: mission.isFinished ? mission.successes : 0,
		fails: mission.isFinished ? mission.fails : 0,
		members: mission.members ? mission.members.map(m => m.id) : [],
	} as MissionDto);

	toDtoGroup = (group: Group) => ({ id: group.id, members: group.users, authors: group.authors } as GroupDto);
	toDtoGroupArray = (groups: Group[]) => groups.map(this.toDtoGroup);
	toDtoRoleSettings = (settings: Partial<RoleSettings>) => ({
		canFail: settings.canFail,
		canSuccess: settings.canSuccess,
		canDoMissions: settings.canDoMissions,
		isRevealed: settings.isRevealed,
	} as RoleSettingsDto);

	toDtoAbility = (ability: Partial<ISpecialAbility>) => ({
		amount: ability?.settings?.amount ?? 0,
		isDisabled: ability?.settings?.isDisabled ?? false,
		targetOnlyMissionMembers: ability.settings?.targetOnlyMissionMembers,
		allowedGameStages: ability.settings?.allowedGameStages,
		allowedMissions: ability.settings?.allowedMissions,
	} as AbilityDto);

	toDtoPlayer = (player: { user: Player["user"], role?: Player["role"], settings?: Partial<Player["settings"]>, ability?: Partial<Player["ability"]>, numberFromCrown?: number }) => ({
		user: player.user ? this.toDtoUser(player.user) : undefined,
		role: player.role ? this.toDtoRole(player.role) : undefined,
		settings: player.settings ? this.toDtoRoleSettings(player.settings) : undefined,
		ability: player.ability ? this.toDtoAbility(player.ability) : undefined,
		numberFromCrown: player.numberFromCrown,
	} as PlayerDto);
}

export default new DtoService();