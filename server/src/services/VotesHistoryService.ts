import { redis } from "../server.js";

class VotesHistoryService {
	async createVotesHistory(roomId: string, missionNumber: number, nicknamesWithNumber: { nickname: string, numberFromCrown: number }[]) {
		nicknamesWithNumber = nicknamesWithNumber.sort((a, b) => a.numberFromCrown - b.numberFromCrown);
		const defaultVotesHistory: VotesHistory = {
			players: nicknamesWithNumber,
			winner: null,
			history: {
				groups: [],
				votings: []
			}
		};
		await redis.multi().json.set(`votes-history:${roomId}:${missionNumber}`, ".", defaultVotesHistory).expire(`votes-history:${roomId}:${missionNumber}`, 60 * 60 * 6).exec();
	}

	async saveGroups(roomId: string, missionIndex: number, groups: number[][]) {
		await redis.json.set(`votes-history:${roomId}:${missionIndex}`, ".history.groups", groups);
	}

	async saveVoting(roomId: string, missionIndex: number, voting: number[][]) {
		await redis.json.ARRAPPEND(`votes-history:${roomId}:${missionIndex}`, ".history.votings", voting);
	}

	async setWinner(roomId: string, missionIndex: number, winner: number[]) {
		await redis.json.set(`votes-history:${roomId}:${missionIndex}`, ".winner", winner);
	}

	async getVotesHistoryForMission(roomId: string, missionIndex: number) {
		return await redis.json.get(`votes-history:${roomId}:${missionIndex}`) as VotesHistory | null;
	}
}

export default new VotesHistoryService();