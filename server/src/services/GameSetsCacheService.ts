import { redis } from "../server.js";
import GameSetsService from "./GameSetsService.js";

class GameSetsCacheService {
	cacheDuration = 60 * 60;

	async getAsync(id: number | undefined): Promise<WithIdAndAuthorId<GameSet> | null> {
		try {
			if (!id) return null;
			const [cachedGameSet, success] = await redis.multi()
				.get(`game-sets:${id}`)
				.expire(`game-sets:${id}`, this.cacheDuration)
				.exec();
			if (!cachedGameSet || !success) {
				const gameSet = await GameSetsService.getById(id);
				if (!gameSet) return null;
				await this.setAsync(gameSet);
				return gameSet;
			}
			return JSON.parse(cachedGameSet as string);
		} catch (error) {
			console.log(error);
			return null;
		}
	}

	async setAsync(gameSet: WithIdAndAuthorId<GameSet>): Promise<void> {
		try {
			await redis.multi()
			  .set(`game-sets:${gameSet.id}`, JSON.stringify(gameSet))
			  .expire(`game-sets:${gameSet.id}`, this.cacheDuration)
			  .exec();
		} catch (error) {
			console.log(error);
		}
	}
}

export default new GameSetsCacheService();