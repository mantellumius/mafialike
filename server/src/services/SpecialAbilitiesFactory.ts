import DisableFail from "../classes/SpecialAbilities/DisableFail.js";
import ForceSuccess from "../classes/SpecialAbilities/ForceSuccess.js";
import Jail from "../classes/SpecialAbilities/Jail.js";
import JailAndReveal from "../classes/SpecialAbilities/JailAndReveal.js";
import NoAbility from "../classes/SpecialAbilities/NoAbility.js";
import RevealPlayerActionInLastMission from "../classes/SpecialAbilities/RevealPlayerActionInLastMission.js";
import ISpecialAbility from "../types/ISpecialAbility";

class SpecialAbilitiesFactory {
	public static getAbility(type: SpecialAbilityType, settings: SpecialAbilitySettings): ISpecialAbility {
		switch (type) {
			case "DisableFail":
				return new DisableFail(settings);
			case "ForceSuccess":
				return new ForceSuccess(settings);
			case "RevealPlayerActionInLastMission":
				return new RevealPlayerActionInLastMission(settings);
			case "Jail":
				return new Jail(settings);
			case "JailAndReveal":
				return new JailAndReveal(settings);
			case "NoAbility":
				return new NoAbility(settings);
			default:
				throw new Error("Unknown ability type");
		}
	}

	public static createFromJSON(json: ISpecialAbility): ISpecialAbility {
		const ability = SpecialAbilitiesFactory.getAbility(json.settings.type, json.settings);
		Object.assign(ability, json);
		return ability;
	}
}

export default SpecialAbilitiesFactory;