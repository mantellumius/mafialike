import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
	await knex.schema.alterTable("abilitySettings", (table) => {
		table.boolean("targetOnlyMissionMembers").notNullable().defaultTo(true);
	});
}


export async function down(knex: Knex): Promise<void> {
	await knex.schema.alterTable("abilitySettings", (table) => {
		table.dropColumn("targetOnlyMissionMembers");
	});
}

