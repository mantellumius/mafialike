import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
	await knex.schema.alterTable("accounts", (table) => {
		table.enum("role", ["User",  "Admin"])
		  .notNullable()
		  .defaultTo("User");
	});
}


export async function down(knex: Knex): Promise<void> {
	await knex.schema.alterTable("accounts", (table) => {
		table.dropColumn("role");
	});
}

