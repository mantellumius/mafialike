import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
	await knex.schema.createTable("gameSets", (table) => {
		table.increments("id").primary();
		table.integer("authorId").references("id").inTable("accounts").notNullable();
		table.string("name").notNullable();
		table.text("description").notNullable();
		table.string("shortDescription", 100).notNullable();
		table.string("image").notNullable();
	});
	await knex.schema.createTable("setRoles", (table) => {
		table.integer("setId").references("id").inTable("gameSets").notNullable().onDelete("CASCADE");
		table.integer("roleId").references("id").inTable("roles").notNullable().onDelete("CASCADE");
	});
	await knex.schema.createTable("setMissionsSettings", (table) => {
		table.integer("setId").references("id").inTable("gameSets").notNullable().onDelete("CASCADE");
		table.integer("missionSettingsId").references("id").inTable("missionSettings").notNullable().onDelete("CASCADE");
	});
}

export async function down(knex: Knex): Promise<void> {
	await knex.schema.dropTable("setRoles");
	await knex.schema.dropTable("setMissionsSettings");
	await knex.schema.dropTable("gameSets");
}
