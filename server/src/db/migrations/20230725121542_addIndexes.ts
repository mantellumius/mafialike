import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
	await knex.schema.alterTable("setRoles", table => {
		table.integer("setId").index().notNullable().alter();
	});
	await knex.schema.alterTable("setMissionsSettings", table => {
		table.integer("setId").index().notNullable().alter();
	});
	await knex.schema.alterTable("gameSetLikes", table => {
		table.integer("gameSetId").index().notNullable().alter();
	});
}

export async function down(): Promise<void> {
	return;
}

