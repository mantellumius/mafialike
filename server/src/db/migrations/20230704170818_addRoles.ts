import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
	await knex.schema.createTable("roles", table => {
		table.increments("id").primary().unique();
		table.integer("authorId").references("id").inTable("accounts").notNullable();
		table.string("name").notNullable();
		table.string("description").notNullable();
		table.string("image").notNullable();
		table.timestamp("created_at").defaultTo(knex.fn.now());
		table.timestamp("updated_at").defaultTo(knex.fn.now());
	});
	await knex.schema.createTable("roleSettings", table => {
		table.integer("roleId").primary().references("id").inTable("roles").onDelete("CASCADE");
		table.boolean("canFail").notNullable();
		table.boolean("canSuccess").notNullable();
		table.string("amount").notNullable();
		table.string("side").notNullable();
	});
}


export async function down(knex: Knex): Promise<void> {
	await knex.schema.dropTable("roleSettings");
	await knex.schema.dropTable("roles");
}

