import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
	return knex.schema.alterTable("roleSettings", (table) => {
		table.boolean("canDoMissions").notNullable().defaultTo(true);
	});
}

export async function down(knex: Knex): Promise<void> {
	return knex.schema.alterTable("roleSettings", (table) => {
		table.dropColumn("canDoMissions");
	});
}
