import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
	await knex.schema.createTable("abilitySettings", table => {
		table.integer("roleId").references("id").inTable("roles").notNullable().primary().onDelete("CASCADE");
		table.integer("amount").notNullable();
		table.string("type").notNullable();
		table.specificType("allowedGameStages", "varchar[]").notNullable();
		table.specificType("allowedMissions", "integer[]").notNullable().defaultTo("{}");
	});
}

export async function down(knex: Knex): Promise<void> {
	await knex.schema.dropTable("abilitySettings");
}

