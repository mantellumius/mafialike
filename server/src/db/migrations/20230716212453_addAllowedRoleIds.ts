import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
	await knex.schema.alterTable("abilitySettings", table => {
		table.specificType("allowedRoleIds", "integer[]").notNullable().defaultTo("{}");
	});
}


export async function down(knex: Knex): Promise<void> {
	await knex.schema.alterTable("abilitySettings", table => {
		table.dropColumn("allowedRoleIds");
	});
}

