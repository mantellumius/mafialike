import { Knex } from "knex";

export async function up(knex: Knex): Promise<void> {
	await knex.schema.alterTable("missionSettings", (table) => {
		table.integer("number").notNullable().defaultTo(1);
	});
}

export async function down(knex: Knex): Promise<void> {
	await knex.schema.alterTable("missionSettings", (table) => {
		table.dropColumn("number");
	});
}

