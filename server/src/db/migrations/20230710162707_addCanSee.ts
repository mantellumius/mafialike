import { Knex } from "knex";

export async function up(knex: Knex): Promise<void> {
	return knex.schema.alterTable("roleSettings", (table) => {
		table.specificType("canSee", "integer[]").notNullable().defaultTo("{}");
	});
}


export async function down(knex: Knex): Promise<void> {
	return knex.schema.alterTable("roleSettings", (table) => {
		table.dropColumn("canSee");
	});
}

