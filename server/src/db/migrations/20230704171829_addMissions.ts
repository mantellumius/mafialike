import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
	await knex.schema.createTable("missionSettings",  (table) => {
		table.increments("id").primary();
		table.integer("requiredFails").notNullable();
		table.integer("requiredMembers").notNullable();
	});
}

export async function down(knex: Knex): Promise<void> {
	await knex.schema.dropTable("missionSettings");
}

