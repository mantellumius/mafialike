import { Knex } from "knex";

export async function up(knex: Knex): Promise<void> {
	await knex.schema.alterTable("abilitySettings", table => {
		table.boolean("isDisabled").notNullable().defaultTo(false);
	});
}


export async function down(knex: Knex): Promise<void> {
	await knex.schema.alterTable("abilitySettings", table => {
		table.dropColumn("isDisabled");
	});
}

