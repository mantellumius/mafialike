import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
	await knex.schema.createTable("gameSetLikes", (table) => {
		table.integer("gameSetId").references("id").inTable("gameSets").onDelete("CASCADE");
		table.integer("userId").references("id").inTable("accounts").notNullable().onDelete("CASCADE");
	});
}


export async function down(knex: Knex): Promise<void> {
	await knex.schema.dropTable("gameSetLikes");
}

