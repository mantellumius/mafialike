import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
	await knex.schema.alterTable("roleSettings", (table) => {
		table.boolean("isRevealed").notNullable().defaultTo(false);
	});
}


export async function down(knex: Knex): Promise<void> {
	await knex.schema.alterTable("roleSettings", (table) => {
		table.dropColumn("isRevealed");
	});
}

