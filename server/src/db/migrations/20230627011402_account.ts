import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
	await knex.schema.createTable("accounts", table => {
		table.increments("id").primary().unique();
		table.string("login").unique().notNullable();
		table.string("email").unique().notNullable();
		table.string("password").notNullable();
		table.timestamp("createdAt", {useTz: false}).defaultTo(knex.fn.now()).notNullable();
	});
}

export async function down(knex: Knex): Promise<void> {
	await knex.schema.dropTable("accounts");
}

