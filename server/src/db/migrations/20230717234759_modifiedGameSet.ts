import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
	await knex.schema.alterTable("gameSets", table => {
		table.dropColumn("shortDescription");
		table.string("backgroundImage").defaultTo("").notNullable();
	});
}


export async function down(knex: Knex): Promise<void> {
	await knex.schema.alterTable("gameSets", table => {
		table.string("shortDescription").defaultTo("").notNullable();
		table.dropColumn("backgroundImage");
	});
}

