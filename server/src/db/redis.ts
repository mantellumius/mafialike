import { config } from "dotenv";
import { createClient, RedisClientType } from "redis";

config();

export async function createRedis() {
	let client: RedisClientType;
	if (process.env.NODE_ENV === "production")
		client = createClient({
			password: process.env.REDIS_PASSWORD,
			socket: {
				host: process.env.REDIS_HOST,
				port: parseInt(process.env.REDIS_PORT!)
			}
		});
	else
		client = createClient({
			password: process.env.REDIS_PASSWORD_DEV,
			socket: {
				host: process.env.REDIS_HOST_DEV,
				port: parseInt(process.env.REDIS_PORT_DEV!)
			}
		});
	await client.connect();
	return client;
}

