import { Router } from "express";
import MissionsController from "../controllers/MissionsController.js";
import BuildRateLimiterMiddleware from "../middlewares/BuildRateLimiterMiddleware.js";

const router = Router()
	.put("/missions", BuildRateLimiterMiddleware(15, 60, "missions"), MissionsController.upsertMissions);

export default router;