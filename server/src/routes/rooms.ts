import { Router } from "express";
import RoomsController from "../controllers/RoomsController.js";
import BuildRateLimiterMiddleware from "../middlewares/BuildRateLimiterMiddleware.js";

const router = Router()
	.post("/rooms", BuildRateLimiterMiddleware(10, 60, "rooms"), RoomsController.create);

export default router;