import { Router } from "express";
import AccountsController from "../controllers/AccountsController.js";

const router = Router()
	.post("/accounts/:id", AccountsController.getById);


export default router;