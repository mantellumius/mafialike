import { Router } from "express";
import GameSetsController from "../controllers/GameSetsController.js";
import BuildRateLimiterMiddleware from "../middlewares/BuildRateLimiterMiddleware.js";

const router = Router()
	.get("/game-sets/search", GameSetsController.search)
	.get("/game-sets/:id/", GameSetsController.getById)
	.get("/game-sets/:id/roles", GameSetsController.getRoles)
	.get("/game-sets", GameSetsController.getAll)
	.post("/game-sets", GameSetsController.create)
	.put("/game-sets/:id", BuildRateLimiterMiddleware(15, 60, "gameSets"), GameSetsController.update)
	.delete("/game-sets/:id", GameSetsController.delete)
	.post("/game-sets/roles", GameSetsController.addRole)
	.get("/game-sets/:id/missions", GameSetsController.getMissionsSettings)
	.put("/game-sets/:id/missions", BuildRateLimiterMiddleware(15, 60, "missions"), GameSetsController.updateMissionsSettings);

export default router;