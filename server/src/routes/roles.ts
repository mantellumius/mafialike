import { Router } from "express";
import RolesController from "../controllers/RolesController.js";
import BuildRateLimiterMiddleware from "../middlewares/BuildRateLimiterMiddleware.js";

const router = Router()
	.get("/roles/:id", RolesController.getById)
	.post("/roles", RolesController.createRole)
	.patch("/roles/:id", BuildRateLimiterMiddleware(15, 60, "role"), RolesController.updateRole)
	.delete("/roles/:id", RolesController.deleteRole)
	.get("/roles/:id/settings", RolesController.getRoleSettings)
	.put("/roles/:id/settings", BuildRateLimiterMiddleware(15, 60, "role:settings"), RolesController.upsertRoleSettings)
	.get("/roles/:id/abilities", RolesController.getRoleAbilitySettings)
	.put("/roles/:id/abilities", BuildRateLimiterMiddleware(15, 60, "role:abilities"), RolesController.upsertRoleAbilitySettings);

export default router;