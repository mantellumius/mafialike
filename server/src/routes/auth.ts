import { Router } from "express";
import AuthController from "../controllers/AuthController.js";
import BuildRateLimiterMiddleware from "../middlewares/BuildRateLimiterMiddleware.js";

const router = Router()
	.post("/login", BuildRateLimiterMiddleware(10, 60, "auth"), AuthController.login)
	.post("/register", BuildRateLimiterMiddleware(10,  60, "auth"), AuthController.register)
	.get("/logout", AuthController.logout)
	.get("/is-authenticated", AuthController.isAuthenticated);

export default router;