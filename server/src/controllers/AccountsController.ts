import { Request, Response } from "express";
import AccountService from "../services/AccountService.js";

class AccountsController {
	public getById(req: Request, res: Response) {
		try {
			const id = Number(req.params.id);
			const account = AccountService.getById(id);
			if (!account) return res.status(404).json({ message: "Account not found" });
			res.json(account);
		} catch (e) {
			res.sendStatus(400);
			console.log(e);
		}
	}
}

export default new AccountsController();