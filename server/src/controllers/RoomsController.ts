import { Request, Response } from "express";
import RoomsService from "../services/RoomsService.js";

class RoomsController {
	async create(req: Request, res: Response) {
		try {
			const owner = req.session.user;
			if (!owner) return res.status(401).json({ message: "Unauthorized" });
			const room = await RoomsService.create(owner);
			return res.json(room);
		}
		catch (err) {
			console.log(err);
			res.sendStatus(400);
		}
	}
}

export default new RoomsController();