import { Request, Response } from "express";
import db from "../db/index.js";
import GameSetsCacheService from "../services/GameSetsCacheService.js";
import GameSetsService from "../services/GameSetsService.js";

class GameSetsController {
	async getAll(req: Request, res: Response) {
		try {
			const result = await GameSetsService.getAll();
			res.json(result);
		} catch (e) {
			res.sendStatus(400);
			console.error(e);
		}
	}

	async getById(req: Request, res: Response) {
		try {
			const withAuthor = req.query.withAuthor === "true";
			const setId = parseInt(req.params.id);
			const result = withAuthor ? await GameSetsService.getByIdWithAuthor(setId) : await GameSetsService.getById(setId);
			if (!result) return res.sendStatus(404);
			res.json(result);
		} catch (e) {
			res.sendStatus(400);
			console.error(e);
		}
	}

	async search(req: Request, res: Response) {
		try {
			const query = req.query as {name:string};
			if (!query.name) return res.json(await GameSetsService.getAll());
			const result = await GameSetsService.searchByName(query.name);
			res.json(result);
		} catch (e) {
			res.sendStatus(400);
			console.error(e);
		}
	}

	async getRoles(req: Request, res: Response) {
		try {
			const setId = parseInt(req.params.id);
			if (!setId) return res.sendStatus(400);
			const result = await GameSetsService.getSetRoles(setId);
			res.json(result);
		} catch (e) {
			res.sendStatus(400);
			console.error(e);
		}
	}

	async create(req: Request, res: Response) {
		try {
			req.body.authorId = req.session.user?.id;
			const result = await GameSetsService.create(req.body);
			res.json(result[0]);
		} catch (e) {
			res.sendStatus(400);
			console.error(e);
		}
	}

	async update(req: Request, res: Response) {
		try {
			const setId = parseInt(req.params.id);
			req.body.authorId = req.session.user?.id;
			const result = await GameSetsService.update(req.body, setId);
			await GameSetsCacheService.setAsync(result[0]);
			res.json(result[0]);
		} catch (e) {
			res.sendStatus(400);
			console.error(e);
		}
	}

	async delete(req: Request, res: Response) {
		try {
			const setId = parseInt(req.params.id);
			const set = await GameSetsService.getById(setId);
			const { id, role } = req.session.user as User;
			if (set && (set.authorId === id || role === "Admin")) {
				const result = await GameSetsService.delete(setId);
				if (result)
					res.json(200);
			} else {
				res.send(403);
			}
		} catch (e) {
			res.sendStatus(400);
			console.error(e);
		}
	}

	async addRole(req: Request, res: Response) {
		try {
			const { setId, roleId } = req.body as { setId: number, roleId: number };
			const authorId = req.session.user?.id;
			if (!setId || !roleId) return res.sendStatus(400);
			const set = await db.table("gameSets").where({ id: setId, authorId });
			if (!set.length) return res.sendStatus(400);
			if ((await GameSetsService.getRolesAmount(setId)) >= 20)
				return res.status(400).send({reason: "Слишком много ролей"});
			await GameSetsService.addRole(setId, roleId);
			res.sendStatus(200);
		} catch (e) {
			res.sendStatus(400);
			console.error(e);
		}
	}

	async getMissionsSettings(req: Request, res: Response) {
		try {
			const { id } = req.params;
			const setId = parseInt(id);
			if (!setId) return res.sendStatus(400);
			const set = await db.table("gameSets").where({ id: setId }).first();
			if (!set) return res.sendStatus(400);
			const missionSettings = await GameSetsService.getMissionsSettings(setId);
			res.json(missionSettings);
		} catch (e) {
			res.sendStatus(400);
			console.error(e);
		}
	}

	async updateMissionsSettings(req: Request, res: Response) {
		try {
			const { id } = req.params;
			const authorId = req.session.user?.id;
			const setId = parseInt(id);
			if (!setId) return res.sendStatus(400);
			const set = await db.table("gameSets").where({ id: setId, authorId }).first();
			if (!set) return res.sendStatus(400);
			if (req.body.length > 10) return res.status(400).send({reason: "Слишком много миссий"});
			await GameSetsService.updateMissionsSettings(setId, req.body);
			res.sendStatus(200);
		} catch (e) {
			res.sendStatus(400);
			console.error(e);
		}
	}

	async createLike(req: Request, res: Response) {
		try {
			const authorId = req.session.user?.id;
			if (!authorId) return res.sendStatus(401);
			const setId = parseInt(req.params.id);
			if (!setId) return res.sendStatus(400);
			await GameSetsService.createLike(setId, authorId);
			res.sendStatus(200);
		} catch (e) {
			res.sendStatus(400);
			console.error(e);
		}
	}

	async deleteLike(req: Request, res: Response) {
		try {
			const authorId = req.session.user?.id;
			if (!authorId) return res.sendStatus(401);
			const setId = parseInt(req.params.id);
			if (!setId) return res.sendStatus(400);
			await GameSetsService.deleteLike(setId, authorId);
			res.sendStatus(200);
		} catch (e) {
			res.sendStatus(400);
			console.error(e);
		}
	}

	async getLikes(req: Request, res: Response) {
		try {
			const setId = parseInt(req.params.id);
			if (!setId) return res.sendStatus(400);
			const amount = await GameSetsService.getLikes(setId);
			res.send(amount);
		} catch (e) {
			res.sendStatus(400);
			console.error(e);
		}
	}
}

export default new GameSetsController();