import { Request, Response } from "express";
import MissionsService from "../services/MissionsService.js";

class MissionSettingsController {
	async upsertMissions(req: Request, res: Response) {
		try {
			const missionId = await MissionsService.upsertMission(parseInt(req.body.id), req.body);
			if (missionId > 0)
				res.sendStatus(200);
			else
				res.sendStatus(400);
		} catch (e) {
			res.sendStatus(400);
			console.error(e);
		}
	}
}

export default new MissionSettingsController();