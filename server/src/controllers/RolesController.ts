import { Request, Response } from "express";
import db from "../db/index.js";
import RolesService from "../services/RolesService.js";

class RolesController {
	async getById(req: Request, res: Response) {
		try {
			const id = parseInt(req.params.id);
			const role = await RolesService.getById(id);
			if (role)
				return res.json(role);
			return res.sendStatus(404);
		} catch (e) {
			res.sendStatus(400);
			console.error(e);
		}
	}

	async createRole(req: Request, res: Response) {
		try {
			req.body.authorId = req.session.user?.id;
			const role = await RolesService.createRole(req.body);
			if (role) {
				await RolesService.upsertRoleSettings({
					roleId: role.id,
					canDoMissions: true,
					canFail: false,
					canSuccess: true,
					side: "Blue",
					amount: "ExactlyOne",
					isRevealed: false,
					canSee: []
				});
				await RolesService.upsertRoleAbilitySettings({
					roleId: role.id,
					type: "NoAbility",
					isDisabled: false,
					amount: 0,
					targetOnlyMissionMembers: false,
					allowedGameStages: [],
					allowedMissions: [],
					allowedRoleIds: [],
				});
				return res.json({ role });
			}
			return res.sendStatus(400);
		} catch (e) {
			res.sendStatus(400);
			console.error(e);
		}
	}

	async updateRole(req: Request, res: Response) {
		try {
			const authorId = req.session.user?.id;
			const id = parseInt(req.params.id);
			if (!authorId) return res.send(401);
			const roleId = await RolesService.updateRole(req.body, id, authorId);
			if (roleId)
				return res.json({ roleId });
			return res.sendStatus(400);
		} catch (e) {
			res.sendStatus(400);
			console.error(e);
		}
	}

	async deleteRole(req: Request, res: Response) {
		try {
			const authorId = req.session.user?.id;
			const id = parseInt(req.params.id);
			if (!authorId) return res.send(401);
			const role = await RolesService.deleteRole(id, authorId);
			if (role)
				return res.json(role);
			return res.sendStatus(400);
		} catch (e) {
			res.sendStatus(400);
			console.error(e);
		}
	}

	async getRoleSettings(req: Request, res: Response) {
		try {
			const roleId = parseInt(req.params.id);
			const settings = await RolesService.getRoleSettings(roleId);
			if (settings)
				return res.json(settings);
			return res.sendStatus(400);
		} catch (e) {
			res.sendStatus(400);
			console.error(e);
		}
	}

	async getRoleAbilitySettings(req: Request, res: Response) {
		try {
			const roleId = parseInt(req.params.id);
			const abilitySettings = await RolesService.getRoleAbilitySettings(roleId);
			if (abilitySettings)
				return res.json(abilitySettings);
			return res.sendStatus(400);
		} catch (e) {
			res.sendStatus(400);
			console.error(e);
		}
	}

	async upsertRoleSettings(req: Request, res: Response) {
		try {
			const authorId = req.session.user?.id;
			req.body.roleId = parseInt(req.params.id);
			const role = await db.table("roles").where({ id: req.body.roleId, authorId }).first();
			if (!role) return;
			const roleSettings = await RolesService.upsertRoleSettings(req.body);
			if (roleSettings.length > 0)
				return res.json(roleSettings[0]);
			return res.sendStatus(400);
		} catch (e) {
			res.sendStatus(400);
			console.error(e);
		}
	}

	async upsertRoleAbilitySettings(req: Request, res: Response) {
		try {
			const authorId = req.session.user?.id;
			req.body.roleId = parseInt(req.params.id);
			const role = await db.table("roles").where({ id: req.body.roleId, authorId }).first();
			if (!role) return;
			const abilitySettings = await RolesService.upsertRoleAbilitySettings(req.body);
			if (abilitySettings.length > 0)
				return res.json(abilitySettings[0]);
			return res.sendStatus(400);
		} catch (e) {
			res.sendStatus(400);
			console.error(e);
		}
	}
}

export default new RolesController();