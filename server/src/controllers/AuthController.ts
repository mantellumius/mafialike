import { Request, Response } from "express";
import AccountService from "../services/AccountService.js";

class AuthController {
	async login(req: Request, res: Response) {
		try {
			const { login, password } = req.body;
			const account = await AccountService.login(login, password);
			if (account) {
				req.session.authenticated = true;
				req.session.user = { id: account.id, nickname: account.login, role: account.role };
				return res.json({ authenticated: req.session.authenticated, user: req.session.user });
			}
			return res.sendStatus(400);
		} catch (err) {
			console.error(err);
			return res.sendStatus(400);
		}
	}

	async register(req: Request, res: Response) {
		try {
			const { email, login, password } = req.body;
			if (await AccountService.register(email, login, password))
				return res.sendStatus(200);
			return res.sendStatus(400);
		} catch (err) {
			console.error(err);
			return res.sendStatus(400);
		}
	}

	async logout(req: Request, res: Response) {
		try {
			req.session.destroy((err) => err && console.error(err));
			return res.sendStatus(200);
		} catch (err) {
			console.error(err);
			return res.sendStatus(400);
		}
	}

	async isAuthenticated(req: Request, res: Response) {
		try {
			return res.json({ authenticated: !!req.session.authenticated, user: req.session.user });
		} catch (e) {
			return res.sendStatus(401);
		}
	}
}

export default new AuthController();