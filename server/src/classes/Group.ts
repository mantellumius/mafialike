import ArraySet from "./DataStructures/ArraySet.js";

export default class Group {
	public users: UserDto[];
	private votes: ArraySet<number>;
	public readonly authors: UserDto[];
	public readonly id: string;


	constructor(users: UserDto[], id: string, authors: UserDto[] = []) {
		this.users = users;
		this.id = id;
		this.authors = authors;
		this.votes = new ArraySet<number>();
	}

	public static createFromJSON(obj: Group): Group {
		const copy = new Group(obj.users, obj.id, obj.authors);
		copy.votes = ArraySet.createFromJSON(obj.votes);
		return copy;
	}

	public static createFromGroup(group: Group) {
		return new Group(group.users, group.id, group.authors);
	}

	get authorsId() {
		return this.authors.map(author => author.id);
	}


	get getVotes() {
		return this.votes.size;
	}

	get votedUsersId() {
		return this.votes.toArray();
	}

	public vote(userId: number) {
		this.votes.add(userId);
	}
}