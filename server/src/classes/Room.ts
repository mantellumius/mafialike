import { ServerToClientEvents } from "@shared";
import { Socket } from "socket.io";
import shuffleArray from "../helpers/shuffleArray.js";
import {io} from "../server.js";
import IRoom from "../types/IRoom.js";
import RoomMember from "../types/RoomMember.js";

class Room implements IRoom {
	readonly owner: UserDto;
	private members: RoomMember[];
	public id: string;
	public isLocked = false;
	public gameSetId: number | undefined;

	constructor(id: string, owner: UserDto) {
		this.owner = owner;
		this.id = id;
		this.members = [];
	}

	join(user: UserDto) {
		const memberIndex = this.members.findIndex(u => u.user.id === user.id);
		if (memberIndex === -1) {
			this.members.push({ user, roomRole: "Observer" });
		}
		else if (this.members[memberIndex].roomRole === "PlayerDisconnected")
			this.members[memberIndex].roomRole = "Player";
	}

	leave(userId: number): void {
		const memberIndex = this.members.findIndex(u => u.user.id === userId);
		if (memberIndex === -1) return;
		if (this.members[memberIndex].roomRole === "Player")
			this.members[memberIndex].roomRole = "PlayerDisconnected";
		if (this.members[memberIndex].roomRole === "Observer")
			this.members.splice(memberIndex, 1);
	}

	sit(userId: number): boolean {
		if (this.isLocked) return false;
		const member = this.members.find(member => member.user.id === userId);
		if (!member) return false;
		member.roomRole = "Player";
		return true;
	}

	stand(userId: number): boolean {
		if (this.isLocked) return false;
		const member = this.members.find(member => member.user.id === userId);
		if (!member) return false;
		member.roomRole = "Observer";
		return true;
	}

	shuffle() {
		this.members = shuffleArray(this.members);
	}

	kick(userId: number) {
		const member = this.members.find(member => member.user.id === userId);
		if (!member) return;
		member.roomRole = "Observer";
	}

	emit<Event extends keyof ServerToClientEvents>(event: Event, ...args: Parameters<ServerToClientEvents[Event]>) {
		this.emitTo(this.connected, event, ...args);
	}

	emitToPlayers<Event extends keyof ServerToClientEvents>(event: Event, ...args: Parameters<ServerToClientEvents[Event]>) {
		this.emitTo(this.connectedPlayers, event, ...args);
	}

	emitToObservers<Event extends keyof ServerToClientEvents>(event: Event, ...args: Parameters<ServerToClientEvents[Event]>) {
		this.emitTo(this.observers, event, ...args);
	}

	private getSockets(users: UserDto[]): Socket[] {
		const rooms = io.sockets.adapter.rooms;
		return users.map(user => {
			const [socketId] = rooms.get(user.id.toString())?.values() ?? [];
			return io.sockets.sockets.get(socketId);
		}).filter(socket => socket !== undefined) as Socket[];
	}

	get sockets(): Socket[] {
		return this.getSockets(this.users);
	}

	get playersSockets(): Socket[] {
		return this.getSockets(this.connectedPlayers) ?? [];
	}

	get observersSockets(): Socket[] {
		return this.getSockets(this.observers);
	}

	get users() {
		return this.members.map(member => member.user);
	}

	get players() {
		return this.members.filter(member => member.roomRole === "Player" || member.roomRole === "PlayerDisconnected").map(member => member.user);
	}

	get connectedPlayers() {
		return this.members.filter(member => member.roomRole === "Player").map(member => member.user);
	}

	get connected() {
		return this.members.filter(member => member.roomRole === "Player" || member.roomRole === "Observer").map(member => member.user);
	}

	get observers() {
		return this.members.filter(member => member.roomRole === "Observer").map(member => member.user);
	}

	isOwner(user: UserDto) {
		return this.owner.id === user.id;
	}

	private emitTo<Event extends keyof ServerToClientEvents>(users: UserDto[], event: Event, ...args: Parameters<ServerToClientEvents[Event]>) {
		users.forEach(user => io.to(user.id.toString()).emit(event, ...args));
	}

	static createFromJSON(obj: Room): Room {
		const room = new Room(obj.id, obj.owner);
		Object.assign(room, obj);
		return room;
	}
}

export default Room;