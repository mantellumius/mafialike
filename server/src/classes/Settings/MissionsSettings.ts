import IMission from "../../types/IMission";
import IMissionsSettings from "../../types/IMissionsSettings";
import Mission from "../Mission.js";


class MissionsSettings implements IMissionsSettings {
	missionsSettings: MissionSettings[];

	constructor(missionsSettings: MissionSettings[]) {
		this.missionsSettings = missionsSettings;
	}

	public generateMissions(): IMission[] {
		return this.missionsSettings.map(missionSettings => new Mission(missionSettings.requiredFails, missionSettings.requiredMembers));
	}

	get amount(): number {
		return this.missionsSettings.length;
	}
}

export default MissionsSettings;