import getRandom from "../../helpers/getRandom.js";
import shuffleArray from "../../helpers/shuffleArray.js";
import SpecialAbilitiesFactory from "../../services/SpecialAbilitiesFactory.js";
import FullRole from "../../types/FullRole";
import FullRoleInfo from "../../types/FullRoleInfo";
import IRolesSettings from "../../types/IRolesSettings";

export default class RolesSettings implements IRolesSettings {
	reds: FullRoleInfo[];
	blues: FullRoleInfo[];

	constructor(rolesSettingsAndBuilders: FullRoleInfo[]) {
		this.blues = rolesSettingsAndBuilders.filter(blue => blue.settings.side === "Blue");
		this.reds = rolesSettingsAndBuilders.filter(red => red.settings.side === "Red");
	}

	public generateRandomPool(playersCount: number): FullRole[] {
		const result: FullRole[] = this.generateFor(Math.ceil(playersCount / 2) - 1, this.reds)
		  .concat(this.generateFor(Math.floor(playersCount / 2) + 1, this.blues));
		return shuffleArray(result);
	}

	private generateFor(amount: number, rolesSettingsAndBuilders: FullRoleInfo[]): FullRole[] {
		const result: FullRole[] = [];
		const ones = rolesSettingsAndBuilders.filter(red => red.settings.amount === "ExactlyOne");
		ones.forEach(one => result.push(this.generateRole(one)));
		const maxes = rolesSettingsAndBuilders.filter(red => red.settings.amount === "Max");
		if (maxes.length > 0)
			while (result.length < amount)
				result.push(this.generateRole(getRandom(maxes)));
		return result.slice(0, amount);
	}

	private generateRole(role: FullRoleInfo): FullRole {
		return {
			role: role.builder(),
			settings: { ...role.settings },
			ability: SpecialAbilitiesFactory.getAbility(role.abilitySettings.type, role.abilitySettings)
		};
	}
}