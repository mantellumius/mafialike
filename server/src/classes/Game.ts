import getDistanceBetweenIndexes from "../helpers/getDistanceBetweenIndexes.js";
import getRandom from "../helpers/getRandom.js";
import SpecialAbilitiesFactory from "../services/SpecialAbilitiesFactory.js";
import IMission from "../types/IMission";
import Player from "../types/Player";
import Settings from "../types/Settings";
import Group from "./Group.js";
import Groups from "./Groups.js";
import Mission from "./Mission.js";

class Game {
	public currentMissionIndex: number;
	public players: Player[];
	public missions: IMission[];
	public unusedAbilities: number[] = [];
	public crownedUserId: number;
	private groups: Groups[];
	private missionsToWin: number;

	public static create(settings: Settings, users: UserDto[]): Game | null {
		const game = new Game();
		game.currentMissionIndex = -1;
		game.missionsToWin = Math.ceil(settings.missionsSettings.amount / 2);
		game.missions = settings.missionsSettings.generateMissions();
		const rolesPool = settings.rolesSettings.generateRandomPool(users.length);
		if (settings.missionsSettings.amount === 0 || rolesPool.length < users.length) return null;
		game.crownedUserId = getRandom(users.map(user => user.id));
		game.players = users.map((user, i) => ({
			user: user,
			role: rolesPool[i].role,
			settings: rolesPool[i].settings,
			ability: rolesPool[i].ability,
			numberFromCrown: getDistanceBetweenIndexes(
			  users.findIndex(u => u.id === game.crownedUserId),
			  users.findIndex(u => u.id === user.id),
			  users.length) + 1,
		} as Player));
		game.groups = [new Groups(game.players.length, game.currentMission.reqMembers)];
		return game;
	}

	public static createFromJSON(gameJSON: Game): Game {
		const game = new Game();
		Object.assign(game, gameJSON);
		game.players = gameJSON.players.map(player => ({
			...player,
			ability: SpecialAbilitiesFactory.createFromJSON(player.ability)
		}));
		game.missions = gameJSON.missions.map(mission => Mission.createFromJSON(mission));
		game.groups = gameJSON.groups.map(groups => Groups.createFromJSON(groups));
		return game;
	}

	public getNumberFromCrown(userId: number) {
		const player = this.players.find(p => p.user.id === userId);
		if (player && player.numberFromCrown)
			return player.numberFromCrown;
		return 0;
	}

	public get gameStage(): GameStage {
		if (this.isRedWin || this.isBlueWin) return "Finished";
		if (this.currentMission.isFinished) return "PreSuggestions";
		if ((this.currentMission?.members?.length ?? 0) > 0) return "Mission";
		if (!this.getLastGroups?.isSuggestionsFinished && this.getLastGroups?.distinctSuggestedGroups.length === 0) return "PreSuggestions";
		if (!this.getLastGroups?.isSuggestionsFinished) return "Suggestions";
		if (!this.getLastGroups?.isVotingFinished) return "Voting";
		return "PreSuggestions";
	}

	public get currentMission(): IMission {
		return this.missions[Math.max(this.currentMissionIndex, 0)];
	}

	public succeedCurrentMission(userId: number): boolean {
		const player = this.players.find(player => player.user.id === userId);
		if (!player || !player.settings.canSuccess) return false;
		this.currentMission.succeed(userId);
		return true;
	}

	public failCurrentMission(userId: number): boolean {
		const player = this.players.find(player => player.user.id === userId);
		if (!player || !player.settings.canFail) return false;
		this.currentMission.fail(userId);
		return true;
	}

	public get getLastGroups(): Groups | undefined {
		return this.groups[this.groups.length - 1];
	}

	public get getPreviousGroups(): Groups | undefined {
		return this.groups[this.groups.length - 2];
	}

	get reVotingRequired(): boolean {
		if (!this.getLastGroups) throw new Error("Invalid call. last group doesn't exist");
		const currentWinners = this.getLastGroups.getWinners;
		const previousWinners = this.getPreviousGroups?.getWinners;
		if (previousWinners)
			return previousWinners && currentWinners.length > 1 && currentWinners.length !== previousWinners.length;
		return currentWinners.length !== 1;
	}

	public getVisionFor(player: Player): Player[] {
		return this.players.map(
		  p => p.user.id === player.user.id ||
		  player.settings.canSee.includes(p.role.id) ||
		  p.settings.isRevealed ? p : {
			  user: p.user,
			  settings: { canDoMissions: p.settings.canDoMissions},
			  numberFromCrown: p.numberFromCrown
		  } as Player);
	}

	public startReVoting() {
		if (!this.getLastGroups) throw new Error("Invalid call. last group doesn't exist");
		const newWinners = this.getLastGroups.getWinners.map(group => Group.createFromGroup(group));
		this.groups.push(new Groups(this.players.length, this.currentMission.reqMembers, newWinners));
	}

	public nextMission() {
		this.currentMissionIndex++;
		this.groups = [new Groups(this.players.length, this.currentMission.reqMembers)];
	}

	public get isBlueWin(): boolean {
		return this.missions.reduce((acc, mission) => mission.isSucceeded ? acc + 1 : acc, 0) >= this.missionsToWin;
	}

	public get isRedWin(): boolean {
		return this.missions.reduce((acc, mission) => mission.isFailed ? acc + 1 : acc, 0) >= this.missionsToWin;
	}


}

export default Game;