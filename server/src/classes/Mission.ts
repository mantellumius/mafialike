import IMission from "../types/IMission";
import Member from "../types/Member";

class Mission implements IMission {
	readonly reqFails: number;
	readonly reqMembers: number;
	members: Member[] | null;

	constructor(reqFails: number, reqMembers: number) {
		this.reqFails = reqFails;
		this.reqMembers = reqMembers;
		this.members = null;
	}

	public static createFromJSON(obj: IMission): IMission {
		const copy = new Mission(obj.reqFails, obj.reqMembers);
		return Object.assign(copy, obj);
	}

	setMembers(members: UserDto[]) {
		this.members = members.map(member => ({
			id: member.id,
			isFail: false,
			isSuccess: false,
			forced: false,
			acted: false
		}));
	}

	get fails(): number {
		return (this.members ?? []).reduce((acc, member) => member.isFail ? acc + 1 : acc, 0);
	}

	get successes(): number {
		return (this.members ?? []).reduce((acc, member) => member.isSuccess ? acc + 1 : acc, 0);
	}

	get isFailed(): boolean {
		return this.isFinished && this.fails >= this.reqFails;
	}

	get isSucceeded(): boolean {
		return this.isFinished && this.fails < this.reqFails;
	}

	get isFinished(): boolean {
		if (!this.members) return false;
		return this.members.every(member => member.acted);
	}

	getMember(id: number): Member | undefined {
		return this.members?.find(member => member.id === id);
	}

	fail(id: number): void {
		const member = this.getMember(id);
		if (member) {
			member.isFail = !member.forced;
			member.acted = true;
		}
	}

	succeed(id: number): void {
		const member = this.getMember(id);
		if (member) {
			member.isSuccess = !member.forced;
			member.acted = true;
		}
	}

	forceMemberSuccess(id: number): boolean {
		const member = this.getMember(id);
		if (!member) return false;
		member.isSuccess = true;
		member.isFail = false;
		member.forced = true;
		return true;
	}
}

export default Mission;