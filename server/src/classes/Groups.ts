import arraysEqual from "../helpers/arraysEqual.js";
import getRandom from "../helpers/getRandom.js";
import Group from "./Group.js";

export default class Groups {
	public distinctSuggestedGroups: Group[] = [];
	public suggestedGroups: Group[] = [];
	private winnerId: string | undefined;
	private readonly maxGroups: number;
	public readonly groupSize: number;

	constructor(maxGroups: number, groupSize: number, groups?: Group[]) {
		this.maxGroups = maxGroups;
		this.groupSize = groupSize;
		this.suggestedGroups = groups ?? [];
		this.distinctSuggestedGroups = groups ?? [];
	}

	public static createFromJSON(obj: Groups): Groups {
		const copy = new Groups(obj.maxGroups, obj.groupSize, obj.suggestedGroups.map(group => Group.createFromJSON(group)));
		copy.distinctSuggestedGroups = obj.distinctSuggestedGroups.map(group => Group.createFromJSON(group));
		return copy;
	}

	public suggestGroup(suggestedGroup: UserDto[], author: UserDto): void {
		if (this.distinctSuggestedGroups.find(group => group.authorsId.includes(author.id)))
			return;
		suggestedGroup = suggestedGroup.slice(0, this.groupSize);
		const newGroup = new Group(suggestedGroup, `group-${this.suggestedGroups.length}`, [author]);
		const repeatedGroup = this.suggestedGroups.find(group => arraysEqual(group.users, suggestedGroup, (a, b) => a.id - b.id));
		if (repeatedGroup)
			repeatedGroup.authors.push(author);
		else
			this.distinctSuggestedGroups.push(newGroup);
		this.suggestedGroups.push(newGroup);
	}

	public get isSuggestionsFinished(): boolean {
		return this.suggestedGroups.length === this.maxGroups;
	}

	public get isVotingFinished(): boolean {
		return this.totalVotes === this.maxGroups;
	}

	public get getWinners(): Group[] {
		const maxVotes = this.suggestedGroups.map(group => group.getVotes).reduce((prev, curr) => Math.max(prev, curr), 0);
		return this.suggestedGroups.filter(group => group.getVotes === maxVotes);
	}

	public get getWinner(): Group {
		this.winnerId ??= getRandom(this.getWinners).id;
		return this.getWinners.find(group => group.id === this.winnerId)!;
	}

	public vote(groupId: string, voteAuthorId: number): void {
		const group = this.suggestedGroups.find(group => group.id === groupId);
		if (!group) return;
		group.vote(voteAuthorId);
	}

	private get totalVotes(): number {
		return this.suggestedGroups.map(group => group.getVotes).reduce((prev, curr) => prev + curr, 0);
	}
}