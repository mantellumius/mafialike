/* eslint-disable @typescript-eslint/no-explicit-any */
type T = Record<string | symbol, any>;

class DeepProxyHandler implements ProxyHandler<T> {
	proxyCache: WeakMap<any, T>;
	onChange: () => void;

	constructor(onChange: () => void) {
		this.proxyCache = new WeakMap();
		this.onChange = onChange;
	}

	get(target: T, property: string | symbol): any {
		const item = target[property];
		if (item && typeof item === "object") {
			if (!this.proxyCache.has(item))
				this.proxyCache.set(item, new Proxy(item, new DeepProxyHandler(this.onChange)));
			return this.proxyCache.get(item);
		}
		return item;
	}

	set(target: T, property: string | symbol, newValue: any): boolean {
		target[property] = newValue;
		if (typeof this.onChange === "function")
			this.onChange();
		return true;
	}
}

export default DeepProxyHandler;