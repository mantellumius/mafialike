import Player from "../../types/Player";
import Game from "../Game.js";
import SpecialAbilityBase from "./SpecialAbilityBase.js";

class RevealPlayerActionInLastMission extends SpecialAbilityBase {
	private action: MissionAction | undefined;

	constructor(settings: SpecialAbilitySettings) {
		super(settings);
		this.settings.allowedGameStages = ["PreSuggestions"];
		this.settings.targetOnlyMissionMembers = true;
	}

	useAbility(game: Game, target: Player): void {
		const member = game.missions.find(m => m.isFinished)?.getMember(target.user.id);
		if (!member) return;
		this.action = member.isSuccess ? "Success" : "Fail";
	}

	buildLogMessage(target: Player): Message {
		let result = "";
		if (this.action === "Success") {
			result = "выполнил";
		}
		if (this.action === "Fail") {
			result = "провалил";
		}
		return {
			type: "Info",
			text: `${target.user.nickname} ${result} последнюю миссию`
		};
	}
}

export default RevealPlayerActionInLastMission;