import Player from "../../types/Player";
import Game from "../Game.js";
import SpecialAbilityBase from "./SpecialAbilityBase.js";

class DisableFail extends SpecialAbilityBase {
	constructor(settings: SpecialAbilitySettings) {
		super(settings);
		this.settings.allowedGameStages = ["Mission"];
		this.settings.targetOnlyMissionMembers = true;
	}

	useAbility(game: Game, target: Player): void {
		target.settings.canFail = false;
		this.logMessage = this.buildLogMessage(target);
	}

	buildLogMessage(target: Player): Message {
		return {
			type: "Info", text: `${target.user.nickname} запрещено проваливать миссию.`
		};
	}
}

export default DisableFail;