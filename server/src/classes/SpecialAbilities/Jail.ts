import Player from "../../types/Player.js";
import Game from "../Game.js";
import SpecialAbilityBase from "./SpecialAbilityBase.js";

class Jail extends SpecialAbilityBase {
	constructor(settings: SpecialAbilitySettings) {
		super(settings);
		this.settings.allowedGameStages = ["PreSuggestions"];
	}

	useAbility(game: Game, target: Player): void {
		target.settings.canDoMissions = false;
		if (target.ability) target.ability.settings.isDisabled = true;
	}

	buildLogMessage(target: Player): Message {
		return {
			type: "Info", text: `${target.user.nickname} попал в тюрьму`
		};
	}
}

export default Jail;