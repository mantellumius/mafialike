import SpecialAbilityBase from "./SpecialAbilityBase.js";

class NoAbility extends SpecialAbilityBase {
	constructor(settings: SpecialAbilitySettings) {
		super(settings);
	}

	canUse(): boolean {
		return false;
	}

	useAbility(): void {
		return;
	}

	buildLogMessage(): Message {
		return { type: "Error", text: "Нет способности" };
	}
}

export default NoAbility;