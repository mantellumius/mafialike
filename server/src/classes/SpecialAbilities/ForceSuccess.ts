import Player from "../../types/Player";
import Game from "../Game.js";
import SpecialAbilityBase from "./SpecialAbilityBase.js";

class ForceSuccess extends SpecialAbilityBase {
	constructor(settings: SpecialAbilitySettings) {
		super(settings);
		this.settings.allowedGameStages = ["Mission"];
		this.settings.targetOnlyMissionMembers = true;
	}

	useAbility(game: Game, target: Player): void {
		if (!game.currentMission.forceMemberSuccess(target.user.id)) return;
	}

	buildLogMessage(target: Player): Message {
		return {
			type: "Info", text: `${target.user.nickname} был принуждён выполнить миссию`
		};
	}
}

export default ForceSuccess;