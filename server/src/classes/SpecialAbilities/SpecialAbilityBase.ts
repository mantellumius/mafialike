import ISpecialAbility from "../../types/ISpecialAbility";
import Player from "../../types/Player";
import Game from "../Game.js";

abstract class SpecialAbilityBase implements ISpecialAbility {
	settings: SpecialAbilitySettings;
	logMessage: Message;
	hit: boolean | null;

	protected constructor(settings: SpecialAbilitySettings) {
		this.settings = settings;
		this.logMessage = { type: "Error", text: "Нет сообщения" };
		this.hit = null;
	}

	canUse(game: Game, player: Player): boolean {
		if (player.ability?.settings.isDisabled) return false;
		return (this.settings.allowedMissions.length === 0 || this.settings.allowedMissions.includes(game.currentMissionIndex + 1)) &&
		  this.settings.allowedGameStages.includes(game.gameStage) &&
		  this.settings.amount > 0;
	}

	use(game: Game, targetId: number): void {
		const target = game.players.find(p => p.user.id === targetId);
		if (!target) return;
		if (this.settings.allowedRoleIds.length === 0 || this.settings.allowedRoleIds.includes(target.role.id)) {
			this.useAbility(game, target);
			this.logMessage = this.buildLogMessage(target);
			this.settings.allowedMissions = this.settings.allowedMissions.filter(m => m !== game.currentMissionIndex + 1);
			this.hit = true;
		} else {
			this.logMessage = this.buildNotHitMessage();
			this.hit = false;
		}
		this.settings.amount--;
	}

	buildNotHitMessage(): Message {
		return { type: "Info", text: "Вы не попали в нужную роль. Способность потрачена." };
	}

	abstract buildLogMessage(player: Player): Message;

	abstract useAbility(game: Game, target: Player): void ;
}

export default SpecialAbilityBase;