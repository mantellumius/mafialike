class RoleBuilderFactory {
	static create(id: number, name: string, image: string, description?: string): () => WithId<Role> {
		return () => ({ id, name, image, description: description ?? "" });
	}
}

export default RoleBuilderFactory;