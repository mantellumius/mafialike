class ArraySet<T> {
	private readonly values: T[];

	constructor(values?: T[]) {
		this.values = values ?? [];
	}

	public static createFromJSON<T>(arraySet: ArraySet<T>): ArraySet<T> {
		return new ArraySet(arraySet.values);
	}

	add(value: T): void {
		if (!this.has(value)) {
			this.values.push(value);
		}
	}

	has(value: T): boolean {
		return this.values.includes(value);
	}

	clear(): void {
		this.values.length = 0;
	}

	get size(): number {
		return this.values.length;
	}

	toArray(): T[] {
		return [...this.values];
	}


}

export default ArraySet;