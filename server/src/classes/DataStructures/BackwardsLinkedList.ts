class BackwardsLinkedList<T> {
	tail: BackwardsLinkedListNode<T> | null;
	length: number;

	constructor(...items: T[]) {
		this.length = 0;
		this.tail = null;
		items.forEach(item => this.add(item));
	}

	add(value: T) {
		const newNode = new BackwardsLinkedListNode(value);
		newNode.previous = this.tail;
		this.tail = newNode;
		this.length++;
	}
}

class BackwardsLinkedListNode<T> {
	value: T;
	previous: BackwardsLinkedListNode<T> | null;

	constructor(value: T) {
		this.value = value;
		this.previous = null;
	}
}

export { BackwardsLinkedListNode };
export default BackwardsLinkedList;