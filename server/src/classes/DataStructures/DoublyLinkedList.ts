export default class DoublyLinkedList<T> {
	tail: LinkedListNode<T> | null;
	head: LinkedListNode<T> | null;
	length: number;
	constructor() {
		this.length = 0;
		this.tail = null;
		this.head = null;
	}

	addToTheEnd(value: T) {
		const newNode = new LinkedListNode(value);
		newNode.previous = this.tail;
		if (this.tail) this.tail.next = newNode;
		this.tail = newNode;
		this.head ??= newNode;
		this.head.next ??= this.tail;
		this.length++;
	}

	addToTheStart(value:T) {
		const newNode = new LinkedListNode(value);
		newNode.next = this.head;
		if (this.head) this.head.previous = newNode;
		this.head = newNode;
		this.tail ??= newNode;
		this.tail.previous ??= this.head;
		this.length++;
	}
}

export class LinkedListNode<T> {
	value: T;
	next: LinkedListNode<T> | null;
	previous: LinkedListNode<T> | null;

	constructor(value: T) {
		this.value = value;
		this.next = null;
		this.previous = null;
	}
}