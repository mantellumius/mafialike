import DeepProxyHandler from "../classes/DeepProxyHandler.js";

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export default function createDeepProxy(target: any, onChange: () => void) {
	return new Proxy(target, new DeepProxyHandler(onChange));
}
