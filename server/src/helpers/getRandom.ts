import generateRandomIntFromInterval from "./generateRandomIntFromInterval.js";

export default function getRandom<T>(array: T[]): T {
	return array[generateRandomIntFromInterval(0, array.length - 1)];
}
