export default function getDistanceBetweenIndexes(startIndex: number, endIndex: number, length: number) {
	let diff = endIndex - startIndex;
	if (diff < 0) diff += length;
	return diff;
}