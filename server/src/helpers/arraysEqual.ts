export default function arraysEqual<T>(arr1: T[], arr2: T[], compare: (a: T, b: T) => number): boolean {
	if (arr1 === arr2) return true;
	if (arr1 === null || arr2 === null) return false;
	if (arr1.length !== arr2.length) return false;
	arr1 = arr1.sort(compare);
	arr2 = arr2.sort(compare);
	return arr1.every((_, index) => compare(arr1[index], arr2[index]) === 0);
}