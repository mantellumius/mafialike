### Mafia like game
## Веб сайт для игры в жанре "mafia-like" на базе настольной игры "Сопротивление". На сайте вы можете создать свой уникальный набор ролей и поиграть в него.

## Стек технологий
- Typescript
- React(Vite)
- MOBX
- Express
- PostgreSQL
- Redis
- WebSockets(socket.io)
- Docker

## Поиграть можно по ссылке https://owncloud3.serveftp.com/
