type WithAuthorId<T> = T & { authorId: number };
