type MissionDto = {
	reqFails: number;
	reqMembers: number;
	fails: number;
	successes: number;
	isFailed: boolean;
	isSucceeded: boolean;
	members: number[];
}
