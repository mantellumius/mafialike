type RoleAndSettings = {
	role: RoleDto;
	settings: RoleSettingsDto;
}