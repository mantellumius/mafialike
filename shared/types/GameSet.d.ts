type GameSet = {
	name: string;
	description: string;
	backgroundImage: string;
	image: string;
}
