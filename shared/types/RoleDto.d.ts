type RoleDto = {
	name: string;
	description: string;
	image: string;
}
