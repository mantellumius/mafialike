type PlayerDto = {
	user: UserDto;
	role?: RoleDto;
	settings?: RoleSettingsDto
	ability?: AbilityDto;
	numberFromCrown?: number;
}
