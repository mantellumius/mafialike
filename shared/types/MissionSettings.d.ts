type MissionSettings = {
	requiredMembers: number,
	requiredFails: number,
	number: number,
}