type RoleSettingsDto = {
	canFail: boolean,
	canSuccess: boolean,
	canDoMissions: boolean;
	isRevealed: boolean;
}
