type User = {
	id: number;
	nickname: string;
	role: UserRole;
}