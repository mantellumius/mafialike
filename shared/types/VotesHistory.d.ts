type VotesHistory = {
	players: { nickname: string, numberFromCrown: number; }[];
	winner: number[] | null;
	history: {
		groups: number[][];
		votings: number[][][];
	};
};