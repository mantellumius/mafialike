type AbilityDto = {
	amount: number,
	targetOnlyMissionMembers:  boolean,
	allowedGameStages: GameStage[],
	allowedMissions: number[],
	isDisabled: boolean,
}