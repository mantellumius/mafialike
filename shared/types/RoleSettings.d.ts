type RoleSettings = {
	roleId: number;
	side: Side;
	canFail: boolean,
	canSuccess: boolean,
	canDoMissions: boolean;
	isRevealed: boolean;
	canSee: number[];
	amount: RoleAmount;
}
