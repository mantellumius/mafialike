type WithIdAndAuthorId<T> = WithId<T> & WithAuthorId<T> & T;
