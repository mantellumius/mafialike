type GroupDto = {
	id: string;
	members: UserDto[];
	authors: UserDto[];
}
