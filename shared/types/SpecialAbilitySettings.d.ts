type SpecialAbilitySettings = {
	roleId: number
	type: SpecialAbilityType;
	amount: number;
	targetOnlyMissionMembers: boolean;
	allowedMissions: number[];
	allowedGameStages: GameStage[];
	allowedRoleIds: number[];
	isDisabled: boolean;
}