type SpecialAbilityType =
  "DisableFail"
  | "ForceSuccess"
  | "RevealPlayerActionInLastMission"
  | "Jail"
  | "JailAndReveal"
  | "NoAbility";
