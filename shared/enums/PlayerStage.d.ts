type PlayerStage = GameStage |
  "Wait"
  | "Ability"
  | "PreGame"
  | "MissionActive";
