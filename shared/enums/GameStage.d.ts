type GameStage =
  | "PreSuggestions"
  | "Suggestions"
  | "Voting"
  | "Mission"
  | "Finished"
