import ClientToServerEvents from "./interfaces/ClientToServerEvents";
import InterServerEvents from "./interfaces/InterServerEvents";
import ServerToClientEvents from "./interfaces/ServerToClientEvents";

export {
	ServerToClientEvents,
	ClientToServerEvents,
	InterServerEvents,
};

