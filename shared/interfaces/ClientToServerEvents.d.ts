interface ClientToServerEvents {
	Create: (callback: (roomId: string) => void) => void;
	Join: (roomId: string, callback: (owner: UserDto) => void) => void;
	ChangeGameSet: (gameSetId: number) => void;
	UseSpecialAbility: (targetId: number) => void;
	CancelUseSpecialAbility: () => void;
	StartGame: () => void;
	SitDown: () => void;
	GetUp: () => void;
	Kick: (userId: number) => void;
	StartSuggestions: () => void;
	SuggestGroup: (group: UserDto[]) => void;
	Vote: (groupId: string) => void;
	PerformMissionAction: (action: MissionAction) => void;
	ChangeLockState: (locked: boolean) => void;
	ShufflePlayers: () => void;
}

export default ClientToServerEvents;
