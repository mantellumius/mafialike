interface ServerToClientEvents {
	JoinFailed: () => void;
	GotUp: (userId: number) => void;
	SatDown: (user: UserDto) => void;
	LockStateChanged: (state: boolean) => void;
	GameSetChanged: (gameSet: WithIdAndAuthorId<GameSet>) => void;
	SuggestionsStarted: () => void;
	PlayersUpdated: (players: PlayerDto[]) => void;
	GameStarted: (players: PlayerDto[], mission: MissionDto[], crownedUserId: number) => void;
	StartFailed: (reason: string) => void;
	GameStateSent: (users: PlayerDto[], missions: MissionDto[], currentMissionIndex: number, crownedUserId: number) => void;
	RoomStateSent: (users: UserDto[]) => void;
	VotingStarted: (groups: GroupDto[]) => void;
	MissionStarted: (missions: MissionDto[]) => void;
	MissionFinished: (missions: MissionDto[]) => void;
	NextMission: (missionIndex: number) => void;
	GameOver: (players: PlayerDto[]) => void;
	StageUpdated: (stage: PlayerStage) => void;
	AbilityResultReceived: (message: Message) => void;
	UpdateAbilityInfo: (ability: AbilityDto) => void;
	VotesHistoryUpdated: (missionIndex: number, votesHistory: VotesHistory) => void;
}

export default ServerToClientEvents;